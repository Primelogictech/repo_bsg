<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title>BSG Garments</title>
	
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
	
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css">
	
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css">
	
	<!-- Feathericon CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/feathericon.min.css">
	
	<!-- Datatables CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/datatables/datatables.min.css">
	
	<!-- Main CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
	
	<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	
	<!-- FONT AWESOME CSS -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
		
</head>
<body>
	<!-- Main Wrapper -->
	<div class="main-wrapper">
		<!-- Header -->
		<div class="header">
			<!-- Logo -->
			<div class="header-left">
				<a href="<?php echo base_url('dashboard'); ?>" class="logo">
					<div class="logo-name">BSG Garments</div>
				</a>
				<a href="<?php echo base_url('dashboard'); ?>" class="logo logo-small">
					<!-- <img src="assets/img/logo-small.png" alt="Logo" width="30" height="30"> -->
					<div class="logo-name-small">BSG Garments</div>
				</a>
			</div>
			<!-- /Logo -->
			
			<a href="javascript:void(0);" id="toggle_btn">
				<i class="fe fe-text-align-left"></i>
			</a>
			
			<div class="top-nav-search">
				<form>
					<input type="text" class="form-control" placeholder="Search here">
					<button class="btn" type="submit"><i class="fa fa-search"></i></button>
				</form>
			</div>
			
			<!-- Mobile Menu Toggle -->
			<a class="mobile_btn" id="mobile_btn">
				<i class="fa fa-bars"></i>
			</a>
			<!-- /Mobile Menu Toggle -->
			
			<!-- Header Right Menu -->
			<ul class="nav user-menu">

				<!-- Notifications -->
				<li class="nav-item dropdown noti-dropdown">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
						<i class="fe fe-bell"></i> <span class="badge badge-pill">3</span>
					</a>
					<div class="dropdown-menu notifications">
						<div class="topnav-dropdown-header">
							<span class="notification-title">Notifications</span>
							<a href="javascript:void(0)" class="clear-noti"> Clear All </a>
						</div>
						<div class="noti-content">
							<ul class="notification-list">
								<li class="notification-message">
									<a href="#">
										<div class="media">
											<span class="avatar avatar-sm">
												<img class="avatar-img rounded-circle" alt="User Image" src="<?php echo base_url(); ?>assets/admin/user.png">
											</span>
											<div class="media-body">
												<p class="noti-details">Notifications appear here</p>
												<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
											</div>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="topnav-dropdown-footer">
							<a href="#">View all Notifications</a>
						</div>
					</div>
				</li>
				<!-- /Notifications -->
				
				<!-- User Menu -->
				<li class="nav-item dropdown has-arrow">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
						<span class="user-img"><img class="rounded-circle" src="<?php echo base_url(); ?>assets/admin/user.png" width="31" alt="Admin"></span>
					</a>
					<div class="dropdown-menu">
						<div class="user-header">
							<div class="avatar avatar-sm">
								<img src="<?php echo base_url(); ?>assets/admin/user.png" alt="User Image" class="avatar-img rounded-circle">
							</div>
							<div class="user-text">
								<h6><?php echo $this->session->userdata("name") ?></h6>
								<p class="text-muted mb-0">Role : <?php echo $this->session->userdata("role") ?></p>
							</div>
						</div>
						<a class="dropdown-item" href="">My Profile</a>
						<a class="dropdown-item" href="">Settings</a>
						<a class="dropdown-item" href="<?php echo base_url('logout'); ?>">Logout</a>
					</div>
				</li>
				<!-- /User Menu -->
			</ul>
			<!-- /Header Right Menu -->
		</div>
		<!-- /Header -->
		
		<!-- Sidebar -->
		<div class="sidebar" id="sidebar">
			<div class="sidebar-inner slimscroll">
				<div id="sidebar-menu" class="sidebar-menu">
					<ul>
						<?php if($this->session->userdata('role') == "Admin"){ ?>
						<li class="br-menu-item"> 
							<a href="<?php echo base_url('dashboard'); ?>"><i class="fe fe-home"></i> <span>Dashboard</span></a>
						</li>

						<li class="submenu">
							<a href="#"><i class="fe fe-document"></i> <span> Orders Management </span> <span class="menu-arrow"></span></a>
							<ul class="br-menu-show" style="display: none;">
								<li class="br-menu-item"><a href="<?php echo base_url('sales/orders'); ?>"> Orders </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('sales/shipments'); ?>"> Shipments </a></li>
							</ul>
						</li>

						<li class="submenu">
							<a href="#"><i class="fe fe-document"></i> <span> Catalog Management </span> <span class="menu-arrow"></span></a>
							<ul class="br-menu-show" style="display: none;">
								<li class="br-menu-item"><a href="<?php echo base_url('categories'); ?>"> View/Add Category </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('all_products'); ?>"> View/Add Product </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('create_sale_item'); ?>"> View/Create Sale Item List </a></li>
							</ul>
						</li>

						<li class="submenu">
							<a href="#"><i class="fe fe-user"></i> <span> Home Page </span> <span class="menu-arrow"></span></a>
							<ul class="br-menu-show" style="display: none;">
								<li class="br-menu-item"><a href="<?php echo base_url('home/top_banners'); ?>"> Top Banners </a></li>
							</ul>
						</li>

						<li class="submenu">
							<a href="#"><i class="fe fe-user"></i> <span> Masters </span> <span class="menu-arrow"></span></a>
							<ul class="br-menu-show" style="display: none;">
								<li class="br-menu-item"><a href="<?php echo base_url('masters/scrolling_message'); ?>"> Scrolling Message </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('masters/tags'); ?>"> Tags </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('masters/attributes'); ?>"> Add Attributes </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('masters/attribute_masters'); ?>"> Add Product Attributes </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('tax'); ?>"> Tax Percentages </a></li>
								<li class="br-menu-item"><a href="<?php echo base_url('masters/our_advantages'); ?>"> Our Advantages </a></li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<!-- /Sidebar -->