<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Admin - Login</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>/assets/admin/img/favicon.png">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">

    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper login-body">
            <div class="login-wrapper">
            	<div class="container">
                	<div class="loginbox">
                    	<div class="login-left">
							<!-- <img class="img-fluid" src="assets/img/logo-white.png" alt="Logo"> -->
							<div class="logo-name text-white">BSG Garments</div>
                        </div>
                        <div class="login-right">
							<div class="login-right-wrap">
								<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
								<h1>Login</h1>

								<!-- Form -->
								<form action="<?php echo base_url('verify_login'); ?>" method="post">
									<div class="form-group">
										<input class="form-control" type="email" name="email" placeholder="Email" required>
									</div>
									<div class="form-group">
										<input class="form-control" type="password" name="password" placeholder="Password" required>
									</div>
									<div class="form-group">
										<input class="form-control btn btn-primary btn-block" type="submit" name="submit" value="Login">
									</div>
								</form>
								<!-- /Form -->
								
								
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="<?php echo base_url(); ?>assets/admin/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="<?php echo base_url(); ?>assets/admin/js/script.js"></script>
		
    </body>
</html>