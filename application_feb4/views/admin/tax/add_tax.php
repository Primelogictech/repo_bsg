<?php $this->load->view('admin/header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-3">
                    <h4 class="page-title">Add Tax</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-3">
                    <div class="float-right">
                        <a href="<?php echo base_url('tax'); ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url('upload_tax') ?>" method="post" enctype="multipart/form-data">
                            <input type="text" name="id" value="<?php if (isset($q)) {    echo $q['id']; } ?>" hidden>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Tax Name<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="tax" max="250" value="<?php if(isset($q['name']) && !empty($q['name'])){ echo $q['name'];} ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Tax Percentage<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="percentage" max="250" value="<?php if(isset($q['percentage']) && !empty($q['percentage'])){ echo $q['percentage'];} ?>" required>
                                </div>
                            </div>
                            
                            <div id="appendAttr"></div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>