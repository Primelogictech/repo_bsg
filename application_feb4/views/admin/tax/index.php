<?php $this->load->view('admin/header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Tax Percentages</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('add_tax'); ?>" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="datatable table table-hover table-center mb-0">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Tax Name</th>
										<th>Tax Percentage</th>
										<th width="50px">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach ($tax as $value) { ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $value['name']; ?></td>
										<td><?php echo $value['percentage']; ?></td>
										<td class="text-right">
											<div class="actions">
												<a href="<?php echo base_url('edit_tax'); ?>/<?php echo base64_encode($value['id']); ?>" class="btn btn-sm bg-success-light mr-2">
													<i class="fe fe-pencil"></i> Edit
												</a>
                                                                                            <a onclick="return confirm('Are you sure?')" class="btn btn-sm bg-danger-light" href="<?php echo base_url('delete_tax'); ?>/<?php echo base64_encode($value['id']); ?>">
													<i class="fe fe-trash"></i> Delete
												</a>
											</div>
										</td>
									</tr>
									<?php $i++;} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>

	</div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>