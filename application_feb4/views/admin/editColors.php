<?php include('header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">

		<div class="row">
        <?php if(isset($product_attributes) && !empty($product_attributes)){ ?>
    <?php foreach ($product_attributes as $key) { ?>
    <form action="<?php echo base_url('updateColors'); ?>" method="post" enctype="multipart/form-data">
    <div class="form-group row remove_attributes_block">
        <label class="col-form-label col-md-3"></label>
        <div class="col-md-12 row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Size</label>
                    <select class="form-control" id="size" name="size">
                        <option value="">-- Select --</option>
                        <option value="S" <?php if($key['size']=='S'){ echo "selected"; } ?>>S</option>
                        <option value="M" <?php if($key['size']=='M'){ echo "selected"; } ?>>M</option>
                        <option value="L" <?php if($key['size']=='L'){ echo "selected"; } ?>>L</option>
                        <option value="XL" <?php if($key['size']=='XL'){ echo "selected"; } ?>>XL</option>
                        <option value="XXL" <?php if($key['size']=='XXL'){ echo "selected"; } ?>>XXL</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Color Name</label>
                    <input type="text" name="colorName" class="form-control" value="<?php echo $key['color_name']; ?>">
                    <input type="hidden" name="id" class="form-control" value="<?php echo $key['id']; ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Color</label>
                    <input type="file" name="color" class="form-control">
                    <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key['color']; ?>" alt="color" width="50px" height="50px">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Quantity</label>
                    <input type="number" name="quantity" value="<?php echo $key['quantity']; ?>" min="1" class="form-control" placeholder="Quantity">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Price</label>
                    <input type="number" value="<?php echo $key['price']; ?>" name="productPrice" min="1" class="form-control" placeholder="Price">
                </div>
            </div>
            <div class="col-md-3">
            <input id="rowCountinput" type="hidden" value="1">
                <div class="form-group">
                    <label>Images</label>
                    <input multiple class="form-control" type="file" accept="image/*" name="images[]" <?php if(isset($q) && !empty($q['id'])){}else{ echo "required"; } ?>>
                   
                </div>
            </div>
            <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-success">
            </div>
        </div>
    </div>
    </form>
    <?php } ?>
    <?php } ?>		
		</div>

	</div>			
</div>
