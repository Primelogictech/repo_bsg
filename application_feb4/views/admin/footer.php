		</div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="<?php echo base_url(); ?>assets/admin/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="<?php echo base_url(); ?>assets/admin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="<?php echo base_url(); ?>assets/admin/js/script.js"></script>

		<script type="text/javascript">
		$(function(){
				var url = window.location.href;
				$('body').find('a[href="'+url+'"]').parents('.br-menu-item').addClass('active');
				$('body').find('a[href="'+url+'"]').parents('.br-menu-show').show();
			});
		</script>
		
    </body>
</html>