<?php include(__DIR__."/../header.php"); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Products</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="javascript:history.go(-1)" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-reply"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="datatable table table-hover table-center mb-0">
								<thead>
									<tr>
										<th>Product Title</th>
										<th>Price Per Each item</th>
										<th>Category</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									if(isset($sale_products) && !empty($sale_products['products'])){ 
										foreach ($products as $value) { 
											if(in_array($value['id'], explode(',', $sale_products['products']))){ ?>
									<tr>
										<td><?php echo $value['product_title']; ?></td>
										<td><?php echo $value['product_price']; ?></td>
										<td><?php echo $value['category_name']; ?></td>
									</tr>
									<?php }}} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>

	</div>			
</div>
<!-- /Page Wrapper -->

<?php include(__DIR__."/../footer.php"); ?>