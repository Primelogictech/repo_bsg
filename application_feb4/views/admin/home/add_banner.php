<?php $this->load->view('admin/header.php'); ?>
			
<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title"><?php if(isset($heading)){ echo $heading; } ?></h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
					<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php if(isset($back)){ echo $back; } ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form action="<?php if(isset($action)){ echo $action; } ?>" method="post" enctype="multipart/form-data">
							<input type="text" name="error_redirect" value="<?php if(isset($error_redirect)){ echo $error_redirect; } ?>" hidden>
							<input type="text" name="upload_path" value="<?php if(isset($upload_path)){ echo $upload_path; } ?>" hidden>
							<input type="text" name="table" value="<?php if(isset($table)){ echo $table; } ?>" hidden>
							<input type="text" name="success_redirect" value="<?php if(isset($success_redirect)){ echo $success_redirect; } ?>" hidden>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Banner Image <span class="mandatory">(please upload an image with 1800x600 pixels)</span></label>
								<div class="col-md-9">
									<input class="form-control" type="file" name="banner_image" accept="image/png,image/jpg,image/jpeg" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Choose Main Category <span class="mandatory">*</span></label>
								<div class="col-md-9">
									<select class="form-control" name="main_category_id" required>
										<option value="">-- Select --</option>
										<?php foreach ($main_categories as $key) { ?>
										<option value="<?php echo $key['id']; ?>" <?php if(isset($q)){  if($q['main_category_id'] == $key['id']){ echo "selected"; }} ?>><?php echo $key['main_category_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Discount Percentage<span class="mandatory">*</span></label>
								<div class="col-md-9">
									<input type="number" name="discount" min="0" max="100" class="form-control" placeholder="Discount Percentage" required>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>