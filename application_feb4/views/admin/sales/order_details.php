<?php $this->load->view('admin/header.php'); ?>

<style type="text/css">
    .icon-container {
        width: 45px;
        position: absolute;
        left: 0;
        top: 11px;
        text-align: center;
    }
    i.fa-minus.toggle-icon.fa{
        position: absolute;
        right: 15px;
        top: 15px;
        font-size: 12px;
        cursor: pointer;
    }
    .panel-icon {
        font-size: 14px;
        padding-right: 0!important;
    }
    .panel-heading {
        padding: 10px 15px;
        border-bottom: 1px solid #f4f4f4;
        color: #444;
        padding-left: 45px;
    }
    .panel-body {
        padding: 15px;
        border: 1px solid #ddd;
        border-radius: 5px;
        margin-bottom: 5px;
    }
    .print-btn{
        border: 1px solid #ccc;
        background-color: #fff;
        border-color: #ddd;
        margin: 0px 10px 0px 0px;
        border-radius: .25rem;
        padding: 6.2px 10px;
    }
    .form-group {
        margin-bottom: 5px;
    }
    .label-wrapper {
        display: table;
        float: right;
    }
    .control-label {
        padding-top: 7px;
        margin-bottom: 0;
        text-align: right;
    }
    .form-text-row {
        padding-top: 6px;
    }
</style>

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-3">
                    <h4 class="page-title">Orders View</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-3">
                    <div class="float-right">
                        <button class="print-btn">Print Packaging Slip</button>
                        <a href="<?php echo base_url('sales/orders'); ?>" class="btn back-btn"><i class="fa fa-reply"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="panel-heading">
                        <div class="icon-container">
                            <i class="fa fa-info panel-icon"></i>
                        </div>
                        <span>Info</span>
                    </div>
                    <div><i class="fa-minus toggle-icon fa"></i></div>
                    <div class="card-body">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Order Id :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['payment_order_id'])){ echo $order_data['payment_order_id']; } ?></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Order Date :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['created_on'])){ echo $order_data['created_on']; } ?></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Customer name:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['name'])){ echo $order_data['name']; } ?></div>
                                    </div>
                                    <?php if($this->session->userdata('role') == "admin"){ ?>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Store Name :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['store_name'])){ echo $order_data['store_name']; } ?></div>
                                    </div>
                                    <?php  } ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <!-- <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Order Subtotal :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row">5000</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Order Shipping Charges :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row">200</div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Tax :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"></div>
                                    </div> -->
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Order Total :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['order_total'])){ echo $order_data['order_total']; } ?></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Payment Menthod :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['payment_method'])){ echo $order_data['payment_method']; } ?></div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="label-wrapper">
                                            <label class="control-label">Payment Status :</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-text-row"><?php if(isset($order_data) && !empty($order_data['payment_status'])){ echo $order_data['payment_status']; } ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="panel-heading">
                        <div class="icon-container">
                            <i class="fas fa-truck panel-icon"></i>
                        </div>
                        <span>Shipping & Billing</span>
                    </div>
                    <div><i class="fa-minus toggle-icon fa"></i></div>
                    <div class="card-body">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-hover table-bordered" style="max-width: 500px;">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <strong>Shipping address</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Full name</td>
                                                <td><?php if(isset($address)){ echo $address['full_name']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php if(isset($user_data)){ echo $user_data['email']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Mobile Number</td>
                                                <td><?php if(isset($address)){ echo $address['mobile']; } ?></td>
                                            </tr>

                                            <tr>
                                                <td>Address </td>
                                                <td><?php if(isset($address)){ echo $address['address_lane']; } ?></td>
                                            </tr>

                                            <tr>
                                                <td>City</td>
                                                <td><?php if(isset($address)){ echo $address['city']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>State</td>
                                                <td><?php if(isset($address)){ echo $address['state']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Zip</td>
                                                <td><?php if(isset($address)){ echo $address['zip']; } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-hover table-bordered" style="max-width: 500px;">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <strong>Billing address</strong>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                                <td>Full name</td>
                                                <td><?php if(isset($address)){ echo $address['full_name']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php if(isset($user_data)){ echo $user_data['email']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Mobile Number</td>
                                                <td><?php if(isset($address)){ echo $address['mobile']; } ?></td>
                                            </tr>

                                            <tr>
                                                <td>Address</td>
                                                <td><?php if(isset($address)){ echo $address['address_lane']; } ?></td>
                                            </tr>

                                            <tr>
                                                <td>City</td>
                                                <td><?php if(isset($address)){ echo $address['city']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>State</td>
                                                <td><?php if(isset($address)){ echo $address['state']; } ?></td>
                                            </tr>
                                            <tr>
                                                <td>Zip</td>
                                                <td><?php if(isset($address)){ echo $address['zip']; } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="panel-heading">
                        <span>Order Items / Products</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th style="width: 120px;">Product Name</th>
                                        <th>Product Image</th>
                                        <th>Quantity</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Tax</th>
                                        <th>Total</th>
                                        <th>Product Order Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($products) && !empty($products)){ foreach($products as $value){
                                        $product_data = json_decode($value['product_info'], TRUE);
                                    ?>
                                    <tr>
                                        <td>
                                            <div><?php if(isset($product_data) && !empty($product_data['product_title'])){ echo $product_data['product_title']; } ?></div>
                                            <div>Id: <?php if(isset($product_data) && !empty($product_data['id'])){ echo $product_data['id']; } ?></div>
                                        </td>
                                        <td><?php if(isset($product_data) && !empty($product_data['banner_image'])){ ?> <img width="50px" src='<?php echo base_url(); ?>/uploads/images/product_images/<?php echo $product_data['banner_image']; ?>'> <?php } ?></td>
                                        <td><?php echo $value['quantity']; ?></td>
                                        <td>
                                            <?php if(!empty($value['color'])){ ?> <img width="50px" src='<?php echo base_url(); ?>/uploads/images/product_images/<?php echo $value['color']; ?>'> <?php } ?>
                                        </td>
                                        <td><?php echo $value['size']; ?></td>
                                        <td><?php if(isset($product_data) && !empty($product_data['product_price'])){ echo "₹ ".$product_data['product_price']; } ?></td>
                                        <td><?php if(isset($product_data) && !empty($product_data['discount_percent'])){ echo $product_data['discount_percent']; } ?></td>
                                        <td><?php if(isset($product_data) && !empty($product_data['tax'])){ echo $product_data['tax']; } ?></td>
                                        <td><?php if(isset($product_data) && !empty($product_data['product_price'])){ echo "₹ ".$product_data['product_price']; } ?></td>
                                        <td>
                                            <?php if(isset($value['return_status'])){ 
                                                if($value['return_status']==1){
                                            ?>
                                            <p>Return Created by <?php if(isset($order_data) && !empty($order_data['name'])){ echo $order_data['name']; } ?> On <?php echo $value['created_at']; ?></p>
                                            <p><b>Reason </b>: <?php echo $value['comments']; ?> </p>
                                            <a href="<?php echo base_url('sales/return_confirm'); ?>/<?php echo $value['return_id'] ?>" class="btn-sm btn-danger btn">Confirm Return</a>

                                       <?php }else{ ?>
                                        <p>Return Created by <?php if(isset($order_data) && !empty($order_data['name'])){ echo $order_data['name']; } ?> On <?php echo $value['created_at']; ?></p>
                                            <p><b>Reason </b>: <?php echo $value['comments']; ?> </p>

                                         <a href="javascript:;" class="btn-sm btn-success btn">Return Completed</a>

                                       <?php  }
                                            }else{
                                             if($value['order_product_status']==Orders::$orderPlaced && $this->session->userdata('role') != "admin"){ ?>
                                            <button class="approve btn btn-sm bg-success-light mr-2" data-id="<?php echo $value['id'] ?>">Approve</button>
                                            <button class="reject btn btn-sm bg-warning-light" data-id="<?php echo $value['id'] ?>">Reject</button>
                                            <?php }elseif($value['order_product_status']==Orders::$vendorApproved){
                                                echo Orders::checkOrderStatus($value['order_product_status']); ?>
                                                <?php if($this->session->userdata('role') == 'vendor'){ ?><br><button class="handover_to_delivery btn btn-sm bg-primary-light" data-id="<?php echo $value['id'] ?>" data-orderid="<?php echo $value['actual_order_id'] ?>" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal">Handover to Delivery</button><?php } ?>
                                            <?php }else{ echo Orders::checkOrderStatus($value['order_product_status'], $this->session->userdata('role')); } } ?>
                                        </td>
                                    </tr>
                                    <?php } }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                <div class="panel-heading">
                        <span>Product Shipment Details</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Shipment ID</th>
                                        <th>Order ID</th>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>Product Image</th>
                                        <th>Tracking Number</th>
                                        <th>Couries Patner</th>
                                        <th>Date Shipped</th>
                                        <th>Current Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;foreach($shipments as $shipment){ ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo "SHP-".$shipment['id']; ?></td>
                                        <td><?php echo "ORDR-".$shipment['order_id']; ?></td>
                                        <td><?php echo $shipment['product_id']; ?></td>
                                        <td><?php echo $shipment['product_name']; ?></td>
                                        <td><img width="50px" src='<?php echo base_url(); ?>/uploads/images/product_images/<?php echo $shipment['product_image']; ?>'></td>
                                        <td><?php echo $shipment['tracking_id']; ?></td>
                                        <td><?php echo $shipment['delivery_partner']; ?></td>
                                        <td><?php echo date('d M Y',strtotime($shipment['created_on'])); ?></td>
                                        <td>
                                            <span class="px-2"><?php echo Orders::checkOrderStatus($shipment['shipment_status'],$this->session->userdata('role')); ?></span>
                                            <span class="actions">
                                                <a data-toggle="modal" href="orders_view.html" class="btn btn-sm bg-success-light mr-2">
                                                    <i class="far fa-eye"></i> View Details
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                               <?php $i++;} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>			
        </div>
    </div>
</div>
<!-- /Page Wrapper -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Handover to Delivery</h5>
                <button type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="<?php echo base_url('sales_controller/handover_to_delivery'); ?>" enctype="multipart/form-data">

                    <input type="number" name="order_product_id" id="order_product_id" hidden>
                    <input type="number" name="order_id" id="order_id" hidden>
                    
                    <div class="form-group">
                        <label> Tracking ID <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="tracking_id" id="tracking_id" required>
                    </div>

                    <div class="form-group">
                        <label> Delivery Partner <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="delivery_partner" id="delivery_partner" required>
                    </div>
        
                    <div class="form-group mb15"> 
                        <div class="col-sm-12 text-center">
                            <input type="submit" class="submit_btn" value="Submit">
                        </div>
                    </div>
        
                </form>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/footer.php'); ?>

<script type="text/javascript">     
    $('body').on('click', '.approve, .reject', function(){
        if (confirm("Are you sure?")){
        var status = $(this).text();
        var id = $(this).data('id');
        $.ajax({
            url: "<?php echo base_url('sales_controller/change_product_order_status'); ?>",
            data: { id: id, status: status },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
        }
    });
</script>

<script type="text/javascript">     
    $('body').on('click', '.handover_to_delivery', function(){
        var product_id = $(this).data('id');
        var order_id = $(this).data('orderid');
        $('#order_product_id').val(product_id);
        $('#order_id').val(order_id);
    });
</script>