<?php $this->load->view('admin/header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h4 class="page-title">Shipments</h4>
                </div>
            </div>
        </div>
        <!-- /Page Header -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Shipment ID</th>
                                        <th>Order ID</th>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>Product Image</th>
                                        <th>Tracking Number</th>
                                        <th>Couries Patner</th>
                                        <th>Date Shipped</th>
                                        <th>Current Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;foreach($shipments as $shipment){ ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo "SHP-".$shipment['id']; ?></td>
                                        <td><?php echo "ORDR-".$shipment['order_id']; ?></td>
                                        <td><?php echo $shipment['product_id']; ?></td>
                                        <td><?php echo $shipment['product_name']; ?></td>
                                        <td><img width="50px" src='<?php echo base_url(); ?>/uploads/images/product_images/<?php echo $shipment['product_image']; ?>'></td>
                                        <td><?php echo $shipment['tracking_id']; ?></td>
                                        <td><?php echo $shipment['delivery_partner']; ?></td>
                                        <td><?php echo date('d M Y',strtotime($shipment['created_on'])); ?></td>
                                        <td>
                                            <span class="px-2"><?php echo Orders::checkOrderStatus($shipment['shipment_status'],$this->session->userdata('role')); ?></span>
                                            <span class="actions">
                                                <a data-toggle="modal" href="orders_view.html" class="btn btn-sm bg-success-light mr-2">
                                                    <i class="far fa-eye"></i> View Details
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                               <?php $i++;} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>			
        </div>
        
    </div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>