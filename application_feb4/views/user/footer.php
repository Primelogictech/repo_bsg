</div>
<!-- Footer Section Begin -->

        <footer class="footer">
            <div class="bsg-footer">
                <div class="col-md-12 col-lg-12">
                    <div class="row">
                        <div class="col-md-3 my-2">
                            <h5 class="text-white font-weight-bold mb-4">BSG Garments</h5>
                            <div>1-59, Orchid Hospital Lane Prabhatnagar, Chaitanyapuri, Hyderabad, Telangana 500060</div>
                            <div class="social-media-icons-section">
                                    <div class="fb-block">
                                        <a href="https://www.facebook.com/BSGGarments">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </div>
                                    <div class="twitter-block">
                                        <a href="https://www.youtube.com/channel/UCqH_aloz9BVdePXmXSYnsIw">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </div>
                                    <div class="instagram-blok">
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-3 my-2">
                            <h5 class="text-white font-weight-bold">About Us</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">BSG Garments - Manufacturer of shirts, kids jeans & cotton pants in Hyderabad, Telangana.</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 my-2">
                            <h5 class="text-white font-weight-bold">Help Centre</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Shipping</a></li>
                                <li><a href="#">Return Process & Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3 my-2">
                            <a href="#">
                            <div class="talk-to-us-block">
                                <div class="footer-icon-styles"talk-to-us-bloc>
                                    <i class="fas fa-phone-alt"></i>
                                </div>
                                <div class="social-info">
                                    <div>Talk to us</div>
                                    <div>090637 01248</div>
                                </div>
                            </div>
                            </a>
                            <a href="#">
                            <div class="write-to-us-block">
                                <div class="footer-icon-styles">
                                    <i class="far fa-envelope"></i>
                                </div>
                                <div class="social-info">
                                    <ul class="list-unstyled">
                                        <li class="py-0">Write to us</li>
                                        <li class="py-0">support@bsggarments.com</li>
                                    </ul>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
      
            <div class="col-lg-12 text-center copy-right-bg">
                <div class="footer__copyright__text">
                    <p>
                        Copyright ©
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        All rights reserved | <a href="http://bsggarments.com/" target="_blank">BSG Garments</a>
                    </p>
                </div>
            </div>
        </footer>

        <!-- Footer Section End -->

        <!-- Search Begin -->

        <!-- <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form" id="search_form" action="<?php echo base_url('search_products') ?>" method="post">
                    <input type="text" name="tag" id="tags_suggestions" placeholder="Search here" autocomplete="off">
                    <button type="submit" style="border: none;"><i class="fa fa-search"></i></button>
                </form>
                <div id="suggesstion-box" style="background-color: white; padding: 10px; position: absolute; z-index: 9999; font-size: 16px !important; left: 30%; top: 58%;"></div>
            </div>
        </div> -->

        <!-- Search End -->

        <!-- Js Plugins -->
        <script src="<?php echo base_url(); ?>assets/user/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/jquery.nicescroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/jquery.countdown.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/jquery.slicknav.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/mixitup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/user/js/main.js"></script>

        <script type="text/javascript">
            $(function(){
                var url = window.location.href;
                $('body').find('a[href="'+url+'"]').parents('.br-menu-item').removeClass('active');
                $('body').find('a[href="'+url+'"]').parents('.br-menu-item').addClass('active');
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $("#tags_suggestions").keyup(function(){
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('user_controller/get_tags_suggestions') ?>",
                        data:'keyword='+$(this).val(),
                        success: function(data){
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").html(data);
                            $("#tags_suggestions").css("background","#FFF");
                        }
                    });
                });
            });
        </script>

        <script type="text/javascript">
            function myFunction(i){
                var name = $(i).text();
                $('#tags_suggestions').val(name);
                $("#suggesstion-box").hide();
            }
        </script>

        <script type="text/javascript">
            $("#search_form").on('submit', function(e){
                e.preventDefault();
                var tags_suggestions = $('#tags_suggestions').val();

                if(tags_suggestions == ""){
                    return false;
                }else{
                    $(this).unbind('submit').submit();
                }
            });
        </script>

    </body>
</html>