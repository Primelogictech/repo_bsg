<?php $this->load->view('user/header'); ?>

<style type="text/css">
    i.fas.fa-chevron-left {
        font-size: 15px;
        padding-right: 10px;
    }
    .input {
        margin: 10px 5px 20px 5px;
    }
    hr {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
    }
</style>

<?php if(empty($cart_data)){ echo "Your Cart is empty"; }else{ ?>
<?php echo form_open('my_address'); ?>
            
<section class="checkout-section">
    <div class="container-fluid">
        <div class="col-md-12 col-lg-10 offset-lg-1">
            <div class="row">
                <div class="col-12 col-md-8 border-right">
                    <div class="itemblock-leftblock">
                        <div class="itemblock-itemheader">
                            <div>My Shopping Bag (<?php if(isset($cart_data)){ echo count($cart_data); }; if (count($cart_data) == 1){echo " Item";}else{echo " Items";} ?>)</div>
                            <!-- <div class="itemblock-totalcartvalue">Total : ₹ <div id="finalPrice"></div></div> -->
                        </div>
                    </div>
                    <?php $price = 0; $discount = 0; 
                    $productsData = array();
                    $i=0;
                    foreach($cart_data as $data){ ?>
                    <?php
                    $productsData[$i] = array(
                        'productId' => $data['id'],
                        'price' => $data['product_price'] - ($data['product_price']*($data['discount_percent']/100)),
                    );
                    
                    ?>
                    <?php $price = $price +($data['product_price'] * (isset($data['quantity'])? $data['quantity']:1)); ?>
                    <?php 
                        if ($data['discount_percent'] != 0) {  
                            $discount = $discount+($data['product_price'] - ($data['product_price'] - ($data['product_price']*($data['discount_percent']/100))));
                        }
                    ?>
                    <div class="itemcontainer-item mb-3">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-5 col-md-3">
                                        <div class="item-image-block">
                                            <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $data['color']; ?>" class="img-fluid mx-auto d-block" alt="">
                                        </div>
                                    </div>
                                    <div class="col-9 col-md-9 pl-0">
                                        <div><?php echo $data['product_title']; ?></div>
                                        <div class="itemcontainer-iten-libk">
                                            <a href="#" class="text-dark"><?php echo $data['short_description']; ?></a>
                                        </div>
                                        <div class="itemcomponents-sellerdata">Sold by: NEXXBASE MARKETING PVT LTD</div>
                                        <div class="itemcomponent-itemsize-n-color">
                                            <span>Size : </span><?php echo $data['size']; ?>
                                            <input type='hidden' name='size[]' value='<?php echo isset($data['size']) ? $data['size']  : "none"; ?>'>
                                            <span>Color : </span><img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $data['color']; ?>" class="img-fluid itemcomponent-colorstyles" alt="">
                                            <input type='hidden' name='color[]' value='<?php echo isset($data['color']) ? $data['color']  : "none"; ?>'>
                                        </div>
                                        <div class="itemcomponent-itemquantity-n-price">
                                            <div class="d-flex qtny">
                                                Quantity : &nbsp;
                                                <a href="#" class="decreaseQty qty-count" data-dprice="<?php echo $data['product_price'] - ($data['product_price']*($data['discount_percent']/100)) ?>">
                                                    <span><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                </a>
                                                <input type="number" class="qty" name="quantity[]" value="<?php echo isset($data['quantity'])? $data['quantity']:1 ; ?>">
                                                <input name="productId" value="<?php echo $data['id']; ?>" type="hidden" />
                                                <a href="#" class="increaseQty qty-count" data-iprice="<?php echo $data['product_price'] - ($data['product_price']*($data['discount_percent']/100)) ?>">
                                                    <span><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                </a>
                                            </div>
                                            <span class="itemcomponent-prize">₹ <?php echo $data['product_price'] - ($data['product_price']*($data['discount_percent']/100)); ?></span>
                                            <?php if(!empty($data['discount_percent'])){ ?>
                                            <span class="item-component-actualprize">₹ <?php echo $data['product_price']; ?></span>
                                            <span class="itemcomponent-offprize"><?php echo $data['discount_percent']; ?>% OFF</span>
                                            <?php } ?>
                                        </div>
                                        <div class="py-2"><?php if($data['returnable'] == 0){ echo "Not Returnable"; }else{ echo "Returnable in ".$data['returnable_days']." days"; } ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="itemcomponent-remove-n-whishlistblock">
                            <div class="row">
                                <div class="col-4">
                                    <div class="itemcomponent-remove-btn">
                                        <a href="<?php echo base_url('remove_from_cart') ?>/<?php echo $data['id'] ?>" class="text-dark">REMOVE</a>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <?php 
                                        $userId = $this->session->userdata('id');
                                        $wishlist = $this->vendor_model->get_single_data(['product_id'=>$data['id'], 'user_id'=>$userId], 'tbl_wishlist');
                                    ?>
                                    <div class="itemcomponent-whishlist-btn">
                                        <a href="#" data-id="<?php echo $data['id'] ?>" data-wishlist="<?php echo empty($wishlist) ? "0" : "1";  ?>" class="wishlist text-dark"><?php if(empty($wishlist)){ ?> MOVE TO <?php }else{ ?>REMOVE FROM <?php } ?> WHISHLIST</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;} ?>
                    <div class="addtowishlist-wishlistblock">
                        <a href=#><i class="far fa-heart moveToWishlistHeartIcon"></i><span class="addtowishlist-wishlisttext">Add More From Wishlist <i class="fas fa-chevron-right float-right pt-1"></i></a>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="price-summary-head">Price Summary</div>
                        <div class="col-12">
                            <div class="price-sum-details">
                            <div class="row mb5">
                                <div class="col-7">
                                    <div>Bag Total</div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right">₹
                                    <span id='price'><?php echo $price; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb5">
                                <div class="col-7">
                                    <div>Bag Discount</div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right disc">-₹
                                    <span id="discount"><?php echo $discount; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb5">
                                <div class="col-7">
                                    <div>Sub Total</div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right disc">
                                        ₹
                                        <span id='subTotal'><?php echo ($price-$discount); ?></span>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="row mb5">-->
                            <!--    <div class="col-7">-->
                            <!--        <div>Offer Discount</div>-->
                            <!--    </div>-->
                            <!--    <div class="col-5">-->
                            <!--        <div class="text-right apply-coupon">Apply Coupon</div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="row mb5">
                                <div class="col-7">
                                    <div>Shipping Charges</div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right">NA</div>
                                </div>
                            </div>
                            <hr>
                            <div class="row mb5">
                                <div class="col-7">
                                    <div class="font-weight-bold">Grand Total</div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right font-weight-bold">₹
                                    <span id="grandTotal">
                                    <?php echo ($price-$discount); ?>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 px-0">
                                <div class="text-center mt-5">
                                    <input type="hidden" name="discount" value="<?php echo base64_encode(base64_encode($discount)); ?>">
                                    <input type="hidden" name="productData" value="<?php echo base64_encode(base64_encode(json_encode($productsData))); ?>">
                                    <input type="hidden" id="valEncode" name="price" value="<?php echo base64_encode(base64_encode($price-$discount)); ?>">
                                    <input type="submit" class="desktopContinueBtn" value="Continue">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo form_close(); ?>
<?php } ?>

<!-- Search Begin -->

<div class="search-model">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="search-close-switch">+</div>
        <form class="search-model-form" id="search_form" action="<?php echo base_url('search_products') ?>" method="post">
            <!-- <input type="text" id="search-input" placeholder="Search here....." /> -->
            <input type="text" name="tag" id="tags_suggestions" placeholder="Search here" autocomplete="off">
            <button type="submit" style="border: none;"><i class="fa fa-search"></i></button>
        </form>
        <div id="suggesstion-box" style="background-color: white; padding: 10px; position: absolute; z-index: 9999; font-size: 16px !important; left: 30%; top: 58%;"></div>
    </div>
</div>

<!-- Search End -->

<!-- Js Plugins -->
<script src="<?php echo base_url(); ?>assets/user/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/jquery.countdown.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/jquery.slicknav.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/mixitup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/user/js/main.js"></script>

<script type="text/javascript">
    $(function(){
        var url = window.location.href;
        $('body').find('a[href="'+url+'"]').parents('.br-menu-item').removeClass('active');
        $('body').find('a[href="'+url+'"]').parents('.br-menu-item').addClass('active');
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#tags_suggestions").keyup(function(){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('user_controller/get_tags_suggestions') ?>",
                data:'keyword='+$(this).val(),
                success: function(data){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#tags_suggestions").css("background","#FFF");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function myFunction(i){
        var name = $(i).text();
        $('#tags_suggestions').val(name);
        $("#suggesstion-box").hide();
    }
</script>

<script type="text/javascript">
    $("#search_form").on('submit', function(e){
        e.preventDefault();
        var tags_suggestions = $('#tags_suggestions').val();

        if(tags_suggestions == ""){
            return false;
        }else{
            $(this).unbind('submit').submit();
        }
    });
</script>

<script>
    function removeFromCart(id){
        var user_id = '<?php echo $this->session->userdata('id'); ?>';
           if (user_id != "") {
          $.ajax({
             type:'post',
             url:'<?php echo base_url(); ?>/remove_from_cart',
             data:{'productId':id},
             success: function(data){
                 if (data=="done") {
                     alert("Removed from cart");
             }
         }
          });
          location.reload();
          } else {
          alert("Please Login to add to cart");
          }
    }
    $(document).ready(function(){
        $(".increaseQty").click(function(){
            var val = $(this).closest("div.qtny").find(".qty").val();
            var productId = $(this).closest("div.qtny").find("input[name='productId']").val();
            $(this).closest("div.qtny").find(".qty").val(parseInt(val)+1);
            var originalPrice = $(this).attr('data-iprice');
            $.ajax({
             type:'post',
             url:'<?php echo base_url(); ?>add_quantity',
             data:{'productId':productId,'quantity':parseInt(val)+1},
             success: function(data){
         }
          });
          var price =  $("#price").html();
          var subTotal =  $("#subTotal").html();
          $("#price").html(parseInt(price)+parseInt(originalPrice));
          $("#subTotal").html(parseInt(subTotal)+parseInt(originalPrice));
          $("#grandTotal").html(parseInt(subTotal)+parseInt(originalPrice));
          $("#finalPrice").html(parseInt(subTotal)+parseInt(originalPrice));
          $("#valEncode").val(btoa(btoa(parseInt(subTotal)+parseInt(originalPrice))));
        });
        $(".decreaseQty").click(function(){
            var val = $(this).closest("div.qtny").find(".qty").val();
            if (parseInt(val) > 1){
            $(this).closest("div.qtny").find(".qty").val(parseInt(val)-1);
            var productId = $(this).closest("div.qtny").find("input[name='productId']").val();
            var originalPrice = $(this).attr('data-dprice');
            $.ajax({
             type:'post',
             url:'<?php echo base_url(); ?>add_quantity',
             data:{'productId':productId,'quantity':parseInt(val)-1},
             success: function(data){
         }
          });
          var price =  $("#price").html();
          var subTotal =  $("#subTotal").html();
          $("#price").html(parseInt(price)-parseInt(originalPrice));
          $("#subTotal").html(parseInt(subTotal)-parseInt(originalPrice));
          $("#grandTotal").html(parseInt(subTotal)-parseInt(originalPrice));
          $("#finalPrice").html(parseInt(subTotal)-parseInt(originalPrice));
          $("#valEncode").val(btoa(btoa(parseInt(subTotal)-parseInt(originalPrice))));
        }
        });
    });
</script>

<script>
    $(document).ready(function(){
        $(".wishlist").click(function(){
            var productId = $(this).attr('data-id');
            var wish = $(this).attr('data-wishlist');
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
                if (user_id != "") {
                    if (wish == 0){
                        $.ajax({
                            type:'post',
                            url:'<?php echo base_url('add_wishlist'); ?>',
                            data:{'productId':productId},
                            success: function(data){
                                alert(data);
                            }
                        });
                        $(this).attr('data-wishlist',"1");
                    }else{
                        $.ajax({
                            type:'post',
                            url:'<?php echo base_url('remove_wishlist'); ?>',
                            data:{'productId':productId},
                            success: function(data){
                                alert(data);
                            }
                        });
                        $(this).attr('data-wishlist',"0");
                    }
                }else {
                    alert("Please Login to add to Wishlist");
                }
        });
    });
</script>

</body>
</html>