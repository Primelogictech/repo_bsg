<?php if(isset($products) && !empty($products)){ foreach ($products as $value) { ?>
<div class="col-6 col-sm-6 col-md-3 product-information">
    <a href="<?php echo base_url('product_details'); ?>?pid=<?php echo base64_encode($value['id']); ?>" class="text-dark">
        <div class="product">
            <div>
                <?php 
                    $userId = $this->session->userdata('id');
                    $wishlist = $this->vendor_model->get_single_data(['product_id'=>$value['id'],'user_id'=>$userId], 'tbl_wishlist');
                ?>
                <!-- <div><i class="far fa-heart whishlist-icon wishlist" data-id="<?php echo $value['id']; ?>" data-wishlist="<?php echo empty($wishlist) ? "0" : "1";  ?>"></i></div> -->

                <?php if(!empty($value['images'])){ $array = explode(',', $value['images']); ?>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php $i=0; foreach ($array as $key) { ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){ echo 'active'; } ?>"></li>
                        <?php $i++; } ?>
                    </ol>
                    <div class="carousel-inner">
                        <?php $i=0; foreach ($array as $key) { ?>
                        <div class="carousel-item <?php if($i==0){ echo 'active'; } ?>">
                            <img class="d-block w-100 img-fluid" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>" alt="img" />
                        </div>
                        <?php $i++; } ?>
                    </div>
                </div>
                <?php }else{ ?>

                <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $value['banner_image']; ?>" class="img-fluid" />

                <?php } ?>

                <div class="new-btn">New</div>
            </div>
            <div class="product-price-block">₹ <strong class="prodict-list-price"><?php echo $value['product_price']; ?></strong></div>
            
            <div class="product-name"><?php echo $value['product_title']; ?></div>
        </div>
    </a>
</div>
<?php }} ?>