<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/user/css/zoom.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/xzoom/dist/xzoom.css" />
<?php include('header.php'); ?>
<style>
#overlay {
  border: 1px solid black;
  width: 400px;
  height: 400px;
  display: inline-block;
  background-image: url('ani.jpeg');
  background-repeat: no-repeat;
}
.xzoom {
    max-width: 300px;
    max-height: 450px;
    margin-bottom: 10px;
}
.xzoom-thumbs {
    text-align: center;
    margin-bottom: 10px;
    /*max-width: 300px;*/
    /*margin: 0 auto;*/
    /*overflow-x: auto;*/
    /*white-space: nowrap;*/
}
</style>

<?php if(isset($product) && !empty($product)){ ?>
<!-- Product Details Section Begin -->
<section class="product-details pt-0 px-0">
    <div class="container-fluid">
        <div class="col-12 mb-3"><a href="<?php echo base_url(); ?>">Home</a> <i class="fas fa-caret-right"></i> <a href="<?php echo base_url(); ?><?php echo $product['url']; ?>"><?php echo $product['main_category_name']; ?></a> <i class="fas fa-caret-right"></i> <a href="<?php echo base_url(); ?>products?cid=<?php echo base64_encode($product['category_id']); ?>"><?php echo $product['category_name']; ?></a> </div>
        <div class="row">
            <div class="col-lg-5 col-12 text-center" id="colorThumbs">
            <?php $arraybanner = explode(',', $attributes[0]['images']); ?>
                <img class="xzoom" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $arraybanner[0]; ?>" xoriginal="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $arraybanner[0]; ?>" />

                <div class="xzoom-thumbs">
                    <?php 
                        if(isset($attributes) && !empty($attributes[0]['images'])){ 
                        $array = explode(',', $attributes[0]['images']);
                        foreach ($array as $key=>$val) { if(!empty($val)){ 
                    ?>
                    <?php if ($key == 1) { ?>
                      <a href="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $val; ?>">
                        <img class="xzoom-gallery" width="80" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $val; ?>"  xpreview="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $val; ?>">
                      </a>
                    <?php } else { ?>
                        <a href="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $val; ?>">
                        <img class="xzoom-gallery" width="80" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $val; ?>" >
                      </a>
                    <?php } ?>
                    <?php }}} ?>
                    <?php if(isset($product['videos']) && !empty($product['videos'])) { ?>
                    <a href="<?php echo $product['videos']; ?>">
                        <img width="80" src="https://cdn.shopify.com/s/files/1/2018/8867/files/play-button.png" >
                    </a>
                    <?php } ?>
                </div>
            </div>

            <div id="overlay" onmousemove="zoomIn(event)" style="display: none;"></div>

            <div class="col-lg-6">
                <div class="product__details__text">
                    <h3><?php echo $product['product_title']; ?> <span>Brand: BSG Garments</span></h3>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>( 138 reviews )</span>
                    </div>
                    <span id="message" style="display:none"></span>
                    <div class="product__details__price">₹ <?php $dis_cost = (min(array_column($attributes, 'price')) - (min(array_column($attributes, 'price'))*($product['discount_percent']/100))); echo $dis_cost; ?> <?php if(!empty($product['discount_percent'])){ ?><span>₹ <?php echo min(array_column($attributes, 'price')); ?></span><?php } ?></div>
                    <p><?php echo html_entity_decode($product['short_description']); ?></p>
                    
                    <div class="product__details__widget">
                        <ul>
                            <?php if(isset($attributes) && !empty($attributes)){ ?>
                            <li>
                                <span>Availability:</span>
                                <div class="stock__checkbox"> In Stock</div>
                            </li>
                            <?php if(!empty($attributes[0]['color'])){ ?>
                            <li>
                                <span>Available color:</span>
                                <div class="color__checkbox row">
                                    <?php foreach ($attributes as $key) { if(!empty($key['color'])){ ?>
                                        <img class="color product_color_img" data-id="<?php echo $key['product_id']; ?>" data-color="<?php echo $key['color_name']; ?>" data-val="<?php echo $key['color']; ?>" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key['color']; ?>">&nbsp;&nbsp;
                                    <?php }} ?>
                                </div>
                            </li>
                            <?php } ?>
                            <li>
                                <span>Available size:</span>
                                <div id="size__btn" class="size__btn row">
                                    <?php if(count(array_column($attributes, 'color')) == 1){ ?>
                                    <?php 
                                    $attributes = $this->vendor_model->get_data(array('product_id'=>$attributes[0]['product_id']), 'tbl_product_stock');
                                    foreach ($attributes as $key) { ?>
                                    <p class="size product__size" data-id="<?php echo $key['id']; ?>" data-val="<?php echo $key['size']; ?>"><?php echo $key['size']; ?></p>&nbsp;&nbsp;
                                    <?php } ?>
                                    <?php } else{ ?>
                                    <?php foreach ($attributes as $key) { ?>
                                    <p class="size product__size" data-id="<?php echo $key['id']; ?>" data-val="<?php echo $key['size']; ?>"><?php echo $key['size']; ?></p>&nbsp;&nbsp;
                                    <?php } } ?>
                                </div>
                            </li>
                            <?php } ?>
                            <!-- <?php if(isset($product) && !empty($product['our_advantages'])){ ?>
                            <li>
                                <span>Promotions:</span>
                                <p class="row">
                                <?php 
                                    $array = explode(',', $product['our_advantages']);
                                    foreach ($array as $key) { if(!empty($key)){ 
                                ?>
                                <img src="<?php echo base_url(); ?>uploads/images/our_advantages_images/<?php echo $key ?>" width="15%">
                                <?php }} ?>
                                </p>
                            </li>
                            <?php } ?> -->
                        </ul>
                    </div>
                    <hr>
                    <div class="product__details__button">
                        <a class="cart-btn" id="addCart" style="cursor: pointer;"><span class="icon_bag_alt"></span> Add to cart</a>
                        <a class="cart-btn" id="buyNow" style="cursor: pointer;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" value="<?php echo $product['id'] ?>" id="finalProductId">
<!-- Items You May Like Section Begin -->
<?php if(isset($items_you_may_like) && !empty($items_you_may_like)){ ?>
<section>
    <div class="container-fluid">
        <h3 class="tittle">Items You May Like</h3>
        <div class="row horizontal-scroll">
            <?php foreach($items_you_may_like as $value){ if($value['id'] != $product['id']){ ?>
            <div class="col-6 col-sm-4 col-md-2 px-0">
                <a href="<?php echo base_url('product_details'); ?>?pid=<?php echo base64_encode($value['id']); ?>" class="text-dark">
                    <div class="direct-item-block">
                        <div class="product__image">
                            <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $value['banner_image']; ?>" class="img-fluid" alt="" />
                        </div>
                        <div class="direct-image_product-prize">₹ <?php echo $value['product_price']; ?></div>
                        <div class="direct-image_product-name"><?php echo $value['product_title']; ?></div>
                    </div>
                </a>
            </div>
            <?php }} ?>
        </div>
    </div>
</section>
<?php }else{ echo "No Products Found"; } ?>
<!-- Items You May Like Section End -->

<?php if($this->session->userdata("id") == ""){ $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; $this->session->set_userdata('login_redirect', $url); } ?>
<?php } ?>
<!-- Product Details Section End -->

<?php include('footer.php'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prettify/188.0.0/prettify.min.js"></script>
<!-- <script src="<?php //echo base_url(); ?>assets/user/js/zoom.js"></script> -->
<script>
function zoomIn(event) {
    var element = document.getElementById("overlay");
    element.style.display = "inline-block";
    var img = document.getElementById("imgZoom");
    var imgUrl = img.src;
    var posX = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
    var posY = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
    element.style.backgroundPosition = (-posX * 1) + "px " + (-posY * 1) + "px";
    element.style.backgroundImage = 'url(' + imgUrl + ')';
}
function zoomOut() {
    var element = document.getElementById("overlay");
    element.style.display = "none";
}
</script>

<script>
    function addToWishList(productId){
        var user_id = '<?php echo $this->session->userdata('id'); ?>';
        var wish = $("#wishlist").val();
            if (user_id != "") {
                if (wish == 0){
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url(); ?>add_wishlist',
                    data:{'productId':productId},
                    success: function(data){
                            $("#heart").css("color", "red");
                    }
                });
                $("#wishlist").val(1);
                } else {
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url(); ?>remove_wishlist',
                    data:{'productId':productId},
                    success: function(){
                            $("#heart").css("color", "black");
                    }
                });
                $("#wishlist").val(0);
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
    }

    function addToCart(){
        var size = $(".sizeSelected").data('val');
        var color = $(".colorSelected").data('val');
        $.ajax({
            type:'post',
            url:'<?php echo base_url(); ?>add_cart',
            data:{'productId':$("#finalProductId").val(),'size':size,'color':color},
            success: function(data){
                if (data=="done") {
                    $("#message").html("Added to cart");
                    $("#message").css("color",'green');
                    $("#message").show('slow');
                    setTimeout(function(){ $("#message").hide('slow'); }, 3000);
                }else {
                    $("#message").html("Already added to cart");
                    $("#message").css("color",'red');
                    $("#message").show('slow');
                    setTimeout(function(){ $("#message").hide('slow'); }, 3000);
                }
            }
        });
    }

    function addToCartBuyNow(){
        var size = $(".sizeSelected").data('val');
        var color = $(".colorSelected").data('val');
        $.ajax({
            type:'post',
            url:'<?php echo base_url(); ?>add_cart',
            data:{'productId':$("#finalProductId").val(),'size':size,'color':color},
            success: function(data){
                if (data=="done") {
                    window.location.href = "<?php echo base_url('checkout'); ?>";
                }else {
                    window.location.href = "<?php echo base_url('checkout'); ?>";
                }
            }
        });
    }
    $(document).ready(function(){
        $("#addCart").click(function(){
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
            if (user_id != "") {
                var colorSelected = $(".color").hasClass("colorSelected");
                var sizeSelected = $(".size").hasClass("sizeSelected");
                if (colorSelected==true && sizeSelected==true){
                    addToCart();
                }else {
                    $("#message").html("Please select Colour and Size");
                    $("#message").css("color",'red');
                    $("#message").show('slow');
                    setTimeout(function(){ $("#message").hide('slow'); }, 3000);
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
        });

        $("#buyNow").click(function(){
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
            if (user_id != "") {
                var colorSelected = $(".color").hasClass("colorSelected");
                var sizeSelected = $(".size").hasClass("sizeSelected");
                if (colorSelected==true && sizeSelected==true){
                    addToCartBuyNow();
                }else {
                    alert("Please select Colour and Size");
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
        });

        $(".color").click(function(){
            $(".color").css({'border':'1px solid #ececec'});
            $(".color").removeClass('colorSelected');
            $(this).css({'border':'1px solid black', 'border-bottom': '5px solid black'});
            $(this).addClass('colorSelected');
            var id = $(this).attr('data-id');
            var color = $(this).attr('data-color');
            if (id!=""){
                $.ajax({
                type:'post',
                url:'<?php echo base_url(); ?>getColorImages',
                data:{'id':id,'color':color},
                success: function(data){
                    $("#colorThumbs").html(data);
                }
                });
                $.ajax({
                type:'post',
                dataType : 'html',
                url:'<?php echo base_url(); ?>getColorSizes',
                data:{'id':id,'color':color},
                success: function(data){
                    $("#size__btn").html(data);
                }
                });
            }
        }); 
    });
    $(document).on('click', ".size", function () {
            $(".size").css({'background-color':'','color':'','border':''});
            $(".size").removeClass('sizeSelected');
            $(this).css({'background-color':'green','color':'white','border':'1px solid green'});
            $(this).addClass('sizeSelected');
            var id = $(this).attr('data-id');
            $("#finalProductId").val(id);
            $.ajax({
                type:'post',
                dataType : 'html',
                url:'<?php echo base_url(); ?>getColorPrice',
                data:{'id':id},
                success: function(data){
                    $(".product__details__price").html(data);
                }
                });
        });
</script>

<script src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>
<script>
    $(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
</script>