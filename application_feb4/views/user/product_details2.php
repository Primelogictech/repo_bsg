<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/user/css/zoom.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" />
<link rel="stylesheet" type="text/css" href="https://unpkg.com/xzoom/dist/xzoom.css" />
<?php include('header.php'); ?>
<style>
#overlay {
  border: 1px solid black;
  width: 400px;
  height: 400px;
  display: inline-block;
  background-image: url('ani.jpeg');
  background-repeat: no-repeat;
}
.xzoom {
    max-width: 300px;
    max-height: 450px;
    margin-bottom: 10px;
}
.xzoom-thumbs {
    text-align: center;
    margin-bottom: 10px;
    /*max-width: 300px;*/
    /*margin: 0 auto;*/
    /*overflow-x: auto;*/
    /*white-space: nowrap;*/
}
</style>

<?php if(isset($product) && !empty($product)){ ?>
<!-- Product Details Section Begin -->
<section class="product-details pt-0 px-0">
    <div class="container-fluid">
        <div class="col-12 mb-3"><a href="<?php echo base_url(); ?>">Home</a> <i class="fas fa-caret-right"></i> <a href="<?php echo base_url(); ?><?php echo $product['url']; ?>"><?php echo $product['main_category_name']; ?></a> <i class="fas fa-caret-right"></i> <a href="<?php echo base_url(); ?>products?cid=<?php echo base64_encode($product['category_id']); ?>"><?php echo $product['category_name']; ?></a> </div>
        <div class="row">
            <div class="col-lg-5 col-12 text-center">
                <img class="xzoom" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $product['banner_image']; ?>" xoriginal="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $product['banner_image']; ?>" />

                <div class="xzoom-thumbs">
                    <?php 
                        if(isset($product) && !empty($product['images'])){ 
                        $array = explode(',', $product['images']);
                        foreach ($array as $key) { if(!empty($key)){ 
                            $keys = array_keys( $array ); 
                    ?>
                    <?php if ($keys == 1) { ?>
                      <a href="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>">
                        <img class="xzoom-gallery" width="80" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>"  xpreview="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>">
                      </a>
                    <?php } else { ?>
                        <a href="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>">
                        <img class="xzoom-gallery" width="80" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>" >
                      </a>
                    <?php } ?>
                    <?php }}} ?>
                </div>
            </div>

            <div id="overlay" onmousemove="zoomIn(event)" style="display: none;"></div>

            <div class="col-lg-6">
                <div class="product__details__text">
                    <h3><?php echo $product['product_title']; ?> <span>Brand: BSG Garments</span></h3>
                    <div class="rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <span>( 138 reviews )</span>
                    </div>
                    <div class="product__details__price">₹ <?php $dis_cost = ($product['product_price'] - ($product['product_price']*($product['discount_percent']/100))); echo $dis_cost; ?> <?php if(!empty($product['discount_percent'])){ ?><span>₹ <?php echo $product['product_price']; ?></span><?php } ?></div>
                    <p><?php echo html_entity_decode($product['short_description']); ?></p>
                    
                    <div class="product__details__widget">
                        <ul>
                            <?php if(isset($attributes) && !empty($attributes)){ ?>
                            <li>
                                <span>Availability:</span>
                                <div class="stock__checkbox"> In Stock</div>
                            </li>
                            <?php if(!empty($attributes[0]['color'])){ ?>
                            <li>
                                <span>Available color:</span>
                                <div class="color__checkbox row">
                                    <?php foreach ($attributes as $key) { if(!empty($key['color'])){ ?>
                                        <img class="color product_color_img" data-val="<?php echo $key['color']; ?>" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key['color']; ?>">&nbsp;&nbsp;
                                    <?php }} ?>
                                </div>
                            </li>
                            <?php } ?>
                            <li>
                                <span>Available size:</span>
                                <div class="size__btn row">
                                    <?php foreach ($attributes as $key) { ?>
                                    <!-- <label class="active">
                                        <input class="size" type="radio" id="xs-btn" name="size" value="<?php echo $key['size']; ?>" /><?php echo $key['size']; ?>
                                    </label> -->
                                    <p class="size product__size" data-val="<?php echo $key['size']; ?>"><?php echo $key['size']; ?></p>&nbsp;&nbsp;
                                    <?php } ?>
                                </div>
                            </li>
                            <?php } ?>
                            <!-- <?php if(isset($product) && !empty($product['our_advantages'])){ ?>
                            <li>
                                <span>Promotions:</span>
                                <p class="row">
                                <?php 
                                    $array = explode(',', $product['our_advantages']);
                                    foreach ($array as $key) { if(!empty($key)){ 
                                ?>
                                <img src="<?php echo base_url(); ?>uploads/images/our_advantages_images/<?php echo $key ?>" width="15%">
                                <?php }} ?>
                                </p>
                            </li>
                            <?php } ?> -->
                        </ul>
                    </div>
                    <hr>
                    <div class="product__details__button">
                        <a class="cart-btn" id="addCart" style="cursor: pointer;"><span class="icon_bag_alt"></span> Add to cart</a>
                        <a class="cart-btn" id="buyNow" style="cursor: pointer;">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if($this->session->userdata("id") == ""){ $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; $this->session->set_userdata('login_redirect', $url); } ?>
<?php } ?>
<!-- Product Details Section End -->

<?php include('footer.php'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prettify/188.0.0/prettify.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/user/js/zoom.js"></script> -->
<script>
function zoomIn(event) {
    var element = document.getElementById("overlay");
    element.style.display = "inline-block";
    var img = document.getElementById("imgZoom");
    var imgUrl = img.src;
    var posX = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
    var posY = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
    element.style.backgroundPosition = (-posX * 1) + "px " + (-posY * 1) + "px";
    element.style.backgroundImage = 'url(' + imgUrl + ')';
}
function zoomOut() {
    var element = document.getElementById("overlay");
    element.style.display = "none";
}
</script>

<script>
    function addToWishList(productId){
        var user_id = '<?php echo $this->session->userdata('id'); ?>';
        var wish = $("#wishlist").val();
            if (user_id != "") {
                if (wish == 0){
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url(); ?>add_wishlist',
                    data:{'productId':productId},
                    success: function(data){
                            $("#heart").css("color", "red");
                    }
                });
                $("#wishlist").val(1);
                } else {
                $.ajax({
                    type:'post',
                    url:'<?php echo base_url(); ?>remove_wishlist',
                    data:{'productId':productId},
                    success: function(){
                            $("#heart").css("color", "black");
                    }
                });
                $("#wishlist").val(0);
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
    }

    function addToCart(){
        var size = $(".sizeSelected").data('val');
        var color = $(".colorSelected").data('val');
        $.ajax({
            type:'post',
            url:'<?php echo base_url(); ?>add_cart',
            data:{'productId':<?php echo $product['id'] ?>,'size':size,'color':color},
            success: function(data){
                if (data=="done") {
                    alert("Added to cart");
                }else {
                    alert("Already added to cart");
                }
            }
        });
    }

    function addToCartBuyNow(){
        var size = $(".sizeSelected").data('val');
        var color = $(".colorSelected").data('val');
        $.ajax({
            type:'post',
            url:'<?php echo base_url(); ?>add_cart',
            data:{'productId':<?php echo $product['id'] ?>,'size':size,'color':color},
            success: function(data){
                if (data=="done") {
                    window.location.href = "<?php echo base_url('checkout'); ?>";
                }else {
                    window.location.href = "<?php echo base_url('checkout'); ?>";
                }
            }
        });
    }
    $(document).ready(function(){
        $("#addCart").click(function(){
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
            if (user_id != "") {
                var colorSelected = $(".color").hasClass("colorSelected");
                var sizeSelected = $(".size").hasClass("sizeSelected");
                if (colorSelected==true && sizeSelected==true){
                    addToCart();
                }else {
                    alert("Please select Colour and Size");
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
        });

        $("#buyNow").click(function(){
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
            if (user_id != "") {
                var colorSelected = $(".color").hasClass("colorSelected");
                var sizeSelected = $(".size").hasClass("sizeSelected");
                if (colorSelected==true && sizeSelected==true){
                    addToCartBuyNow();
                }else {
                    alert("Please select Colour and Size");
                }
            }else {
                window.location.href = "<?php echo base_url('login'); ?>";
            }
        });

        $(".size").click(function(){
            $(".size").css({'background-color':'','color':'','border':''});
            $(".size").removeClass('sizeSelected');
            $(this).css({'background-color':'green','color':'white','border':'1px solid green'});
            $(this).addClass('sizeSelected');
        });

        $(".color").click(function(){
            $(".color").css({'border':'1px solid #ececec'});
            $(".color").removeClass('colorSelected');
            $(this).css({'border':'1px solid black', 'border-bottom': '5px solid black'});
            $(this).addClass('colorSelected');
        }); 
    });
</script>

<script src="https://unpkg.com/xzoom/dist/xzoom.min.js"></script>
<script>
    $(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
</script>