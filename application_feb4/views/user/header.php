<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8" />
        <meta name="description" content="BSG Garments" />
        <meta name="keywords" content="BSG Garments, unica, creative, html" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>BSG Garments</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet" />

        <!-- Css Styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/elegant-icons.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/magnific-popup.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/owl.carousel.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/slicknav.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/custom.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/ecomm.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
    </head>
    <style>
    
        div#suggesstion-box {
            position: absolute;
            z-index: 9;
            background: #fff;
            width: 94%;
            top: 80px;
        }       
        .form-group.has-search{
            margin-bottom: 0px;
        } 
        button.main-search-icon-btn{
            border:none;
            background: transparent;
            padding: 0px;
            margin: 0px;
        }
        .main-search-icon{
            color: #aaa;
            background: transparent;
            position: absolute;
            top: 15px;
            left: 26px;
        }
        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
            top: 27px;
        }
        .search-control{
            border-radius: 3px;
        }
        .search-control:focus {
            border-color: #c8c9c6;
            outline: 0;
            -webkit-box-shadow: none;
            box-shadow: none;,
        }
       
        /*@media (max-width: 576px){*/
        /*    .carousel-item.main-banner-carousel-item a img{*/
        /*        height: 120px;*/
        /*    }*/
        /*}*/
        @media (max-width: 767px){
            .form-group.has-search{
                margin-bottom: 1rem;
            } 
            /*.carousel-item.main-banner-carousel-item a img{*/
            /*    height: 213px;*/
            /*}*/
        }
        /*@media (max-width: 991px){*/
        /*    .carousel-item.main-banner-carousel-item a img{*/
        /*        height: 252px;*/
        /*    }*/
        /*}*/
        /*@media (max-width: 1199px){*/
        /*    .carousel-item.main-banner-carousel-item a img{*/
        /*        height: 340px;*/
        /*    }*/
        /*}*/
        /*@media (min-width: 1200px){*/
        /*     .carousel-item.main-banner-carousel-item a img{*/
        /*        height: 450px;*/
        /*    }*/
        /*}*/
    </style>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a><?php }else{ ?>
                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                    <?php } ?>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                <a class="text-dark" href="<?php echo base_url('my_orders'); ?>">My Orders</a>
                <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a>
                <?php }  ?>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a>
                                    <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a><?php }else{ ?>
                                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-light">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <div class="header__logo">
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/images/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <nav class="header__menu mobile-menu text-left">
                                <ul>
                                    <li class="br-menu-item"><a href="<?php echo base_url(); ?>">Mens</a></li>
                                    <li class="br-menu-item"><a href="<?php echo base_url('kids'); ?>">Kids</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-lg-4 col-md-4 my-auto">
                            <div class="form-group has-search">
                                <form action="<?php echo base_url('products') ?>" method="post">
                                    <div class="d-flex">
                                        <button type="submit" class="main-search-icon-btn"><i class="fa fa-search main-search-icon"></i></button>
                                        <input type="text" name="tag" id="tags_suggestions" class="form-control my-0 search-control" placeholder="Search" autocomplete="off">
                                    </div>
                                </form>
                            </div>
                            <div id="suggesstion-box"></div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="header__nav__option">
                                <!--<a href="#" class="search-switch"><img src="<?php echo base_url(); ?>uploads/images/icon/search.png" alt="" /></a>-->
                                 <a href="<?php echo base_url('checkout'); ?>" class="text-dark"><div class="pr8 d-inline">Basket</div><img src="<?php echo base_url(); ?>uploads/images/icon/cart.png" alt="" /> <span><?php if(isset($cart)){ echo $cart; } ?></span></a>
                                <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                                <a class="text-dark" href="<?php echo base_url('my_orders'); ?>">My Orders</a>
                                <!--<a href="#"><img src="<?php echo base_url(); ?>uploads/images/icon/heart.png" alt="" /></a>-->
                                <!--<a href="<?php echo base_url('checkout'); ?>"><img src="<?php echo base_url(); ?>uploads/images/icon/cart.png" alt="" /> <span><?php if(isset($cart)){ echo $cart; } ?></span></a>-->
                                <!--<div class="price">₹0.00</div>-->
                                <?php } ?>
                            </div>
                        </div>
                        
                    </div>
                    <div class="canvas__open"><i class="fa fa-bars"></i></div>
                </div>
            </div>
            <div class="offer_strip">
                <marquee><?php if(isset($scrolling_message) && !empty($scrolling_message['scrolling_message'])){ echo $scrolling_message['scrolling_message']; } ?></marquee>
            </div>
        </header>
        <!-- Header Section End -->
        <div class="main__wrapper">