<?php
class Vendor_model extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	public function get_data($where,$table)
	{
		return $this->db->where($where)->get($table)->result_array();
	}
	public function get_data_order_by_desc($where,$table)
	{
		return $this->db->where($where)->order_by('id',"DESC")->get($table)->result_array();
	}
	public function get_single_data($where,$table)
	{
		return $this->db->where($where)->get($table)->row_array();
	}
	public function update_data($data,$where,$table)
	{
		return $this->db->where($where)->update($table,$data);
	}
	public function insert_data($data,$table)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function insert_batch($data, $table)
    {
       return $this->db->insert_batch($table, $data);
    }
	public function delete_data($where,$table)
	{
		  $this->db->where($where);
		  return $this->db->delete($table);
	}
	public function get_count($where, $table)
	{
		// return $this->db->select('*')->from($table)->get()->num_rows();
		return $this->db->where($where)->get($table)->num_rows();
	}
	public function get_data_by_priority($where='', $table='')
	{
		if(!empty($where)){
			$this->db->where($where);
		}
		return $this->db->order_by('priority',"ASC")->get($table)->result_array();
	}
	public function get_data_by_limit($where, $order_by, $limit, $table)
	{
		return $this->db->where($where)->order_by('id', $order_by)->limit($limit)->get($table)->result_array();
	}
	
	public function get_product_data($id='')
	{
		$this->db->select('tbl_products.*, tbl_main_categories.main_category_name, tbl_main_categories.url, tbl_categories.category_name');
		$this->db->from('tbl_products');
		$this->db->join('tbl_main_categories', 'tbl_main_categories.id = tbl_products.main_category_id');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		if(!empty($id)){
		    $this->db->where('tbl_products.id', $id);
		}
		$query = $this->db->get();
		return $query->row_array();
	}

	public function get_products($where='')
	{
		$this->db->select('tbl_products.*, tbl_categories.category_name');
		$this->db->from('tbl_products');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		if(!empty($where)){
		    $this->db->where('tbl_products.id', $where);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_vendor_products($vendor, $where='')
	{
		$this->db->select('tbl_products.*, tbl_categories.category_name');
		$this->db->from('tbl_products');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		$this->db->where('tbl_products.seller_id', $vendor);
		if(!empty($where)){
		    $this->db->where('tbl_products.id', $where);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_tags()
	{
		$this->db->select('tbl_tags.*, tbl_main_categories.main_category_name, tbl_categories.category_name');
		$this->db->from('tbl_tags');
		$this->db->join('tbl_main_categories', 'tbl_main_categories.id = tbl_tags.main_category_id');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_tags.category_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_attribute_masters()
	{
		$this->db->select('tbl_attribute_masters.*, tbl_main_categories.main_category_name, tbl_categories.category_name');
		$this->db->from('tbl_attribute_masters');
		$this->db->join('tbl_main_categories', 'tbl_main_categories.id = tbl_attribute_masters.main_category_id');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_attribute_masters.category_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function ajax_get_sale_items()
	{
		return $this->db->get('tbl_create_sale_items');
	}

	public function ajax_get_products_for_sale($main_category_id='', $category_id='', $main_cat_filter='', $cat_filter='')
	{
		$this->db->select('tbl_products.*, tbl_categories.category_name');
		$this->db->from('tbl_products');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		if(!empty($main_category_id)){
			$this->db->where('tbl_products.main_category_id', $main_category_id);
		}
		if(!empty($category_id)){
			$this->db->where('tbl_products.category_id', $category_id);
		}

		if(!empty($main_cat_filter)){
			$this->db->where('tbl_products.main_category_id', $main_cat_filter);
		}
		if(!empty($cat_filter)){
			$this->db->where('tbl_products.category_id', $cat_filter);
		}
		$query = $this->db->get();
		return $query;
	}


	// ADMIN SALES CONTROLLER QUERIES
	public function get_orders($where=null,$orderBy=null)
	{
		$this->db->select('tbl_orders.*, tbl_users.name');
		$this->db->from('tbl_orders');
		$this->db->join('tbl_users', 'tbl_users.id = tbl_orders.user_id');
		if ($where!=null){
			$this->db->where('tbl_orders.order_status', $where);
		}
		if ($orderBy!=null){
			$this->db->order_by("tbl_orders.id", "asc");
		} else{
			$this->db->order_by("tbl_orders.id", "desc");
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_products_by_order($id='')
	{
		$this->db->select('tbl_order_products.*, tbl_order_return_products.status as return_status,tbl_order_return_products.created_at,tbl_order_return_products.return_issue,tbl_order_return_products.comments,tbl_order_return_products.id as return_id');
		$this->db->from('tbl_order_products');
		$this->db->join('tbl_order_return_products', 'tbl_order_return_products.order_product_id = tbl_order_products.id','left');
		if(!empty($id)){
		    $this->db->where('tbl_order_products.order_id', $id);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function get_order_data($where='')
	{
		$this->db->select('tbl_orders.*, tbl_users.name');
		$this->db->from('tbl_orders');
		$this->db->join('tbl_users', 'tbl_users.id = tbl_orders.user_id');
		$this->db->join('tbl_order_products', 'tbl_order_products.order_id = tbl_orders.id');
		if(!empty($where)){
		    $this->db->where('tbl_orders.id', $where);
		}
		$query = $this->db->get();
		return $query->row_array();
	}
	public function get_return_orders($where=null,$orderBy=null)
	{
		$this->db->select('tbl_orders.*, tbl_users.name,tbl_order_return_products.status as return_status');
		$this->db->from('tbl_orders');
		$this->db->join('tbl_users', 'tbl_users.id = tbl_orders.user_id');
		$this->db->join('tbl_order_return_products', 'tbl_order_return_products.order_id = tbl_orders.id');
		if ($where!=null){
			$this->db->where('tbl_orders.order_status', $where);
		}
		if ($orderBy!=null){
			$this->db->order_by("tbl_orders.id", "asc");
		} else{
			$this->db->order_by("tbl_orders.id", "desc");
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getShipments($id=null)
	{
		$this->db->select('tbl_shipments.*');
		$this->db->from('tbl_shipments');
		if(!empty($id)){
		    $this->db->where('tbl_shipments.seller_id', $id);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	public function getOrderShipemts($id,$sellerId=null){
		$this->db->select('tbl_shipments.*');
		$this->db->from('tbl_shipments');
		$this->db->where('tbl_shipments.order_id', $id);
		if(!empty($sellerId)){
		    $this->db->where('tbl_shipments.seller_id', $sellerId);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	// USER QUERIES

	public function get_cart_details($where='')
	{
		// $this->db->select('tbl_products.*, tbl_categories.category_name, tbl_cart.size, tbl_cart.color, tbl_cart.quantity');
		// $this->db->from('tbl_cart');
        // $this->db->join('tbl_products', 'tbl_products.id = tbl_cart.product_id');
		// $this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		// $this->db->where('tbl_cart.user_id', $where);
		// $query = $this->db->get();
		// return $query->result_array();
		$this->db->select('tbl_product_stock.id,tbl_product_stock.price product_price,tbl_categories.category_name,
		tbl_product_stock.size,tbl_product_stock.color,tbl_products.product_title,
		tbl_products.short_description,tbl_cart.quantity,tbl_products.discount_percent,
		tbl_products.returnable,tbl_products.returnable_days,');
		$this->db->from('tbl_cart');
		$this->db->join('tbl_product_stock', 'tbl_product_stock.id = tbl_cart.product_id');
		$this->db->join('tbl_products', 'tbl_products.id = tbl_product_stock.product_id');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		$this->db->where('tbl_cart.user_id', $where);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getUserProducts($whereCondition){
		$this->db->select('tbl_order_products.*,tbl_products.*, tbl_categories.category_name');
		$this->db->from('tbl_order_products');
		$this->db->join('tbl_products', 'tbl_order_products.product_id = tbl_products.id');
		$this->db->join('tbl_categories', 'tbl_categories.id = tbl_products.category_id');
		$this->db->where($whereCondition);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getOrderProducts($where){
		$this->db->select('tbl_order_products.*,tbl_orders.payment_method,tbl_orders.created_on as placed_on, tbl_orders.order_total, tbl_orders.id as orderId');
		$this->db->from('tbl_orders');
		$this->db->join('tbl_order_products', 'tbl_orders.id = tbl_order_products.order_id');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_tags_suggestions($where)
	{
		$this->db->select('tbl_tags.tags');
		$this->db->from('tbl_tags');
		if(!empty($where)){
		    $this->db->like($where);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

    public function get_products_suggestions($tag)
	{
		$this->db->select('tbl_products.*');
		$this->db->from('tbl_products');
		if(!empty($tag)){
			$this->db->like('tags', $tag);
			$this->db->or_like('product_title', $tag);
			$this->db->or_like('short_description', $tag);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_products_with_ids($sid='', $search_keyword='', $min_price_filter='', $max_price_filter='', $sleeves='', $type='',$design='', $size_filter='')
	{
		$this->db->select('tbl_products.*, tbl_product_stock.size');
		$this->db->from('tbl_product_stock');
		$this->db->join('tbl_products', 'tbl_products.id=tbl_product_stock.product_id', 'right');
		if(!empty($sid)){
			$this->db->where_in('tbl_products.id', $sid);
		}
		if(!empty($search_keyword)){
			$this->db->like('tbl_products.tags', $search_keyword);
			$this->db->or_like('tbl_products.product_title', $search_keyword);
		}
		if(!empty($min_price_filter)){
			$this->db->where('tbl_products.product_price >=', $min_price_filter);
		}
		if(!empty($max_price_filter)){
			$this->db->where('tbl_products.product_price <=', $max_price_filter);
		}
		if(!empty($sleeves)){
			$this->db->where('tbl_products.sleeve_length', $sleeves);
		}
		if(!empty($type)){
			$this->db->where('tbl_products.type', $type);
		}
		if(!empty($design)){
			$this->db->where('tbl_products.design', $design);
		}
		if(!empty($size_filter)){
			$this->db->where('tbl_product_stock.size', $size_filter);
		}
		$this->db->group_by('tbl_products.id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_products_ajax($cid='', $search_keyword='', $min_price_filter='', $max_price_filter='', $sleeves='', $type='',$design='', $size_filter='')
	{
		$this->db->select('tbl_products.*, tbl_product_stock.size');
		$this->db->from('tbl_product_stock');
		$this->db->join('tbl_products', 'tbl_products.id=tbl_product_stock.product_id', 'right');
		if(!empty($cid)){
		    $this->db->where('tbl_products.category_id', $cid);
		}
		if(!empty($search_keyword)){
			$this->db->like('tbl_products.tags', $search_keyword);
			$this->db->or_like('tbl_products.product_title', $search_keyword);
		}
		if(!empty($min_price_filter)){
			$this->db->where('tbl_products.product_price >=', $min_price_filter);
		}
		if(!empty($max_price_filter)){
			$this->db->where('tbl_products.product_price <=', $max_price_filter);
		}
		if(!empty($sleeves)){
			$this->db->where('tbl_products.sleeve_length', $sleeves);
		}
		if(!empty($type)){
			$this->db->where('tbl_products.type', $type);
		}
		if(!empty($design)){
			$this->db->where('tbl_products.design', $design);
		}
		if(!empty($size_filter)){
			$this->db->where('tbl_product_stock.size', $size_filter);
		}
		$this->db->group_by('tbl_products.id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getAttributes($where,$table)
	{
		return $this->db->where($where)->group_by('color_name')->get($table)->result_array();
	}
}