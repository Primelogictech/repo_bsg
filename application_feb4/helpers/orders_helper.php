<?php
class Orders{

    public static $orderPlaced = 1;

    public static $vendorApproved = 2;

    public static $vendorCancelled = 3;

    public static $handOverToCourier = 4;

    public static $inProgress = 5;

    public static $shipped = 6;

    public static function checkOrderStatus($status=null,$role=null){
        switch($status){
            case 1:
                return "Order Placed";
            break;
            case 2:
                return "In Progress, Order Approved";
            break;
            case 3:
                return "Order Cancelled";
            break;
            case 4:
                if ($role=="User"){
                    return "Shipped";
                } else if ($role=="Admin"){
                    return "Courier picked up";
                } else{
                    return "None";
                }
            break;
            case 5:
                return "In Progress";
            break;
            case 6:
                return "Shipped";
            break;
            default:
                return "No Status";
            break;
        }
    }
    public static function getSleeves(){
        $data = [
            'half'=>'Half Sleeves',
            'full'=>'Full Sleeves',
            'short'=>'Short Sleeves',
            'threeByfour'=>'3/4 Sleeves',
            'sleeveless'=>'Sleeveless',
            'roolUp'=>'Roll Up Sleeves'

        ];
        return $data;
    }
    public static function getType(){
        $data = [
            'casuals'=>'Casuals',
            'sports'=>'Sports',
            'formals'=>'Formals',
            'ethenics' => 'Ethenics',
            'sweatshirts'=>'Sweatshirts',
        ];
        return $data;
    }
    public static function getDesign(){
        $data = [
            'printed'=>'Printed',
            'solid'=>'Solid',
            'textured'=>'Textured',
            'striped' => 'Striped',
            'colorBlocked'=>'ColorBlocked',
        ];
        return $data;
    }
}