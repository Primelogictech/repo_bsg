<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('orders');
		$this->load->model('vendor_model');	
	}
	
	public function products()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['products'] = $this->vendor_model->get_products();
		
		$product_session = array('short_description'=>'', 'description'=>'', 'main_category_id'=>'', 'category_id'=>'', 'sub_category_id'=>'', 'estimated_delivery_days'=>'', 'returnable'=>'', 'returnable_days'=>'', 'not_returnable_reason'=>'', 'payment_methods'=>'', 'cod'=>'', 'no_cod_reason'=>'', 'international_shipping'=>'', 'international_shipping_countries'=>'', 'no_international_shipping_reason'=>'', 'price_including_tax'=>'', 'tax'=>'', 'discount_applicable'=>'', 'discount_applicable_if_products'=>'', 'discount_percent'=>'', 'discount_validity'=>'', 'delivery_charge'=>'', 'local_delivery_exception_amount'=>'', 'non_local_delivery_exception_amount'=>'', 'local_delivery_charge_amount'=>'', 'non_local_delivery_charge_amount'=>'', 'reward_points_per_item'=>'');
		$this->session->unset_userdata($product_session);
		$this->load->view('admin/products', $data);
	}
	public function view_product($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['products'] = $this->vendor_model->get_products(base64_decode($id));
		$this->load->view('admin/view_product', $data);
	}
	public function add_product($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
		$data['tax'] = $this->vendor_model->get_data(array(), 'tbl_taxes');
		$data['our_advantages_data'] = $this->vendor_model->get_single_data(array(), 'tbl_our_advantages');
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_products');
			$data['product_attributes'] = $this->vendor_model->get_data(array('product_id'=>base64_decode($id)), 'tbl_product_stock');
		}
		$this->load->view('admin/add_product', $data);
	}
	public function upload_product($id='')
	{
	    
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		
		$id = $this->input->post('id');
		$product_title = $this->input->post('product_title', TRUE);
		$short_description = $this->input->post('short_description', TRUE);
		$description = htmlentities($this->input->post('description'));
     
		$main_category_id = $this->input->post('main_category_id', TRUE);
		$category_id = $this->input->post('category_id', TRUE);

		$size = $this->input->post('size');
		$quantity = $this->input->post('quantity');
		$colorName = $this->input->post('colorName');
		$productPrice = $this->input->post('productPrice');

		$attributes = $this->input->post('attributes', TRUE);
		
		$estimated_delivery_days = $this->input->post('estimated_delivery_days', TRUE);
		$returnable = $this->input->post('returnable', TRUE);
		$returnable_days = $this->input->post('returnable_days', TRUE);
		$not_returnable_reason = $this->input->post('not_returnable_reason', TRUE);
		
		$payment_methods = $this->input->post('payment_methods', TRUE);
		$cod = $this->input->post('cod', TRUE);
		$no_cod_reason = $this->input->post('no_cod_reason', TRUE);
		$international_shipping = $this->input->post('international_shipping', TRUE);
		$international_shipping_countries = $this->input->post('international_shipping_countries', TRUE);
		$no_international_shipping_reason = $this->input->post('no_international_shipping_reason', TRUE);
		
		$product_price = $this->input->post('product_price', TRUE);
		$price_including_tax = $this->input->post('price_including_tax', TRUE);
		$tax = $this->input->post('tax', TRUE);
		
		$discount_applicable = $this->input->post('discount_applicable', TRUE);
		$discount_applicable_if_products = $this->input->post('discount_applicable_if_products', TRUE);
		$discount_percent = $this->input->post('discount_percent', TRUE);
		$discount_validity = $this->input->post('discount_validity', TRUE);
		
		$delivery_charge = $this->input->post('delivery_charge', TRUE);
		$local_delivery_exception_amount = $this->input->post('local_delivery_exception_amount', TRUE);
		$non_local_delivery_exception_amount = $this->input->post('non_local_delivery_exception_amount', TRUE);
		$local_delivery_charge_amount = $this->input->post('local_delivery_charge_amount', TRUE);
		
		$non_local_delivery_charge_amount = $this->input->post('non_local_delivery_charge_amount', TRUE);
		$reward_points_per_item = $this->input->post('reward_points_per_item', TRUE);
                $attributes = $this->input->post('attributes', TRUE);
		$related_products = $this->input->post('related_products', TRUE);
		$sleeves = $this->input->post('sleeves');
		$type = $this->input->post('type');
		$design = $this->input->post('design');
		
		$videos = $this->input->post('videos', TRUE);
		$rowCount = $_POST['rowCount'];
       
		if(!empty($this->input->post('our_advantages'))){
		    $our_advantages = implode(',', $this->input->post('our_advantages'));
        }else{
            $our_advantages = NULL;
		}
		
		if(!empty($this->input->post('tags'))){
		    $tags = implode(',', $this->input->post('tags'));
        }else{
            $tags = NULL;
        }

		$vendor_data = array('sleeve_length'=>$sleeves,'type'=>$type,'design'=>$design,'product_title'=>$product_title, 'short_description'=>$short_description, 'description'=>$description, 'main_category_id'=>$main_category_id, 'category_id'=>$category_id, 'estimated_delivery_days'=>$estimated_delivery_days, 'returnable'=>$returnable, 'returnable_days'=>$returnable_days, 'not_returnable_reason'=>$not_returnable_reason, 'payment_methods'=>$payment_methods, 'cod'=>$cod, 'no_cod_reason'=>$no_cod_reason, 'international_shipping'=>$international_shipping, 'international_shipping_countries'=>$international_shipping_countries, 'no_international_shipping_reason'=>$no_international_shipping_reason, 'product_price'=>$product_price, 'price_including_tax'=>$price_including_tax, 'tax'=>$tax, 'discount_applicable'=>$discount_applicable, 'discount_applicable_if_products'=>$discount_applicable_if_products, 'discount_percent'=>$discount_percent, 'discount_validity'=>$discount_validity, 'delivery_charge'=>$delivery_charge, 'local_delivery_exception_amount'=>$local_delivery_exception_amount, 'non_local_delivery_exception_amount'=>$non_local_delivery_exception_amount, 'local_delivery_charge_amount'=>$local_delivery_charge_amount, 'non_local_delivery_charge_amount'=>$non_local_delivery_charge_amount, 'reward_points_per_item'=>$reward_points_per_item, 'related_products'=>$related_products, 'videos'=>$videos, 'our_advantages'=>$our_advantages, 'tags'=>$tags, 'created_at'=>date('Y-m-d H:i:s'));

		if(!empty($attributes)){
			$vendor_data = array_merge($vendor_data, ['attributes'=>$attributes]);
		}

		if(isset($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name']))
		{
			if(!empty($id)){
				$delete_image = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_products');
				if(!empty($delete_image['banner_image'])){
					unlink("uploads/images/product_images/".$delete_image['banner_image']);
				}
			}
        	$config['upload_path'] = 'uploads/images/product_images/';
            $config['allowed_types'] = '*';
            $config['file_name'] = strtolower($product_title)."_".date("Y_m_d_H_i_s");
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('banner_image'))
	        {
	        	$error =  $this->upload->display_errors();
	        	$this->session->set_flashdata('danger', $error);
	        	redirect('add_product');
	        }
	        else
	        {
	        	$imageData = $this->upload->data();
	            $banner_image =  $imageData['file_name'];
	            $vendor_data = array_merge($vendor_data, ['banner_image'=>$banner_image]);
	        }
		}
		//var_dump($rowCount);
		
		if(!empty($rowCount)){
		$imagesData = array();
		foreach ($rowCount as $key=>$val){
			if(isset($_FILES['images'.$val]['name']))
				{ 
					$files = $_FILES;
					$count = count($_FILES['images'.$val]['name']);
					$fileNames = array();
					for($i=0; $i<$count; $i++){
						$_FILES['images']['name']= $files['images'.$val]['name'][$i];
						$_FILES['images']['type']= $files['images'.$val]['type'][$i];
						$_FILES['images']['tmp_name']= $files['images'.$val]['tmp_name'][$i];
						$_FILES['images']['error']= $files['images'.$val]['error'][$i];
						$_FILES['images']['size']= $files['images'.$val]['size'][$i];

						$config['upload_path'] = 'uploads/images/product_images/';
						$config['allowed_types'] = '*';
						$config['file_name'] = strtolower($product_title)."_images_".($i+1).'_'.date("Y_m_d_H_i_s");
						$this->load->library('upload', $config);
						$this->upload->initialize($config);

						if(!$this->upload->do_upload('images'))
						{
							$error =  $this->upload->display_errors();
							$this->session->set_flashdata('danger', $error);
							redirect('add_product');
						}
						else
						{
							$upload_data = $this->upload->data();
							$fileNames[] = $upload_data['file_name'];
						}
					}
					
					array_push($imagesData, implode(',', $fileNames));
				}

		}
	}
		if(isset($_FILES['bank_offers']['name']) && !empty($_FILES['bank_offers']['name'][0]))
		{
	        $files = $_FILES;
        	$count = count($_FILES['bank_offers']['name']);
        	for($i=0; $i<$count; $i++){
                $_FILES['bank_offers']['name']= $files['bank_offers']['name'][$i];
                $_FILES['bank_offers']['type']= $files['bank_offers']['type'][$i];
                $_FILES['bank_offers']['tmp_name']= $files['bank_offers']['tmp_name'][$i];
                $_FILES['bank_offers']['error']= $files['bank_offers']['error'][$i];
                $_FILES['bank_offers']['size']= $files['bank_offers']['size'][$i];

                $config['upload_path'] = 'uploads/images/product_images/';
	            $config['allowed_types'] = '*';
	            $config['file_name'] = strtolower($product_title)."_bank_offers_".($i+1).'_'.date("Y_m_d_H_i_s");
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);

	            if(!$this->upload->do_upload('bank_offers'))
		        {
		        	$error =  $this->upload->display_errors();
		        	$this->session->set_flashdata('danger', $error);
		        	redirect('add_product');
		        }
		        else
		        {
		        	$upload_data = $this->upload->data();
                	$fileNames[] = $upload_data['file_name'];
		        }
            }
            if(!empty($id)){
				$get_bank_offers = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_products');
				if(!empty($get_bank_offers['bank_offers'])){
					array_push($fileNames, $get_bank_offers['bank_offers']);
				}
			}
	        $vendor_data = array_merge($vendor_data, ['bank_offers'=>implode(',', $fileNames)]);
	    }
	    

	  //   if(isset($_FILES['color']['name']) && !empty($_FILES['color']['name'])){
			// $files = $_FILES;
			// $count = count($_FILES['color']['name']);
   //      	for($i=0; $i<$count; $i++){
			// 	$_FILES['color']['name'] = $files['color']['name'][$i];
	  //           $_FILES['color']['type'] = $files['color']['type'][$i];
	  //           $_FILES['color']['tmp_name'] = $files['color']['tmp_name'][$i];
	  //           $_FILES['color']['error'] = $files['color']['error'][$i];
	  //           $_FILES['color']['size'] = $files['color']['size'][$i];

	  //       	$config['upload_path'] = 'uploads/images/product_images/';
	  //           $config['allowed_types'] = '*';
	  //           $config['file_name'] = strtolower($product_title)."_color_".$i."_".date("Y_m_d_H_i_s");
	  //           $this->load->library('upload', $config);
	  //           $this->upload->initialize($config);
	  //           $this->upload->do_upload('color');
	  //       	$color[] = $this->upload->data();
	  //       }
	  //   }

		// if(isset($_FILES['Colour']['name']) && !empty($_FILES['Colour']['name']))
		// {
	    //     $files = $_FILES;
        // 	$count = count($_FILES['Colour']['name']);
        // 	for($i=0; $i<$count; $i++){
        //         $_FILES['Colour']['name']= $files['Colour']['name'][$i];
        //         $_FILES['Colour']['type']= $files['Colour']['type'][$i];
        //         $_FILES['Colour']['tmp_name']= $files['Colour']['tmp_name'][$i];
        //         $_FILES['Colour']['error']= $files['Colour']['error'][$i];
        //         $_FILES['Colour']['size']= $files['Colour']['size'][$i];

        //         $config['upload_path'] = 'uploads/images/colours/';
	    //         $config['allowed_types'] = 'png|jpg|jpeg';
	    //         $config['file_name'] = $product_title."_image_".$i.time();
	    //         $this->load->library('upload', $config);
	    //         $this->upload->initialize($config);

        //         $this->upload->do_upload('Colour');

        //         $upload_data = $this->upload->data();

        //         $fileNames2[] = $upload_data['file_name'];

        //     }
        //     $attrdata = array_merge($vendor_data['attributes'], ['Colour'=>$fileNames2]);
        //     $vendor_data = array_merge($vendor_data, ['attributes'=>$attrdata]);
	    // }

		if(!empty($product_title) && !empty($short_description) && !empty($main_category_id) && !empty($category_id)){
			if(!empty($id)){
				//$data= $this->vendor_model->delete_data(array('product_id'=>$id),'tbl_product_stock');
				$productId = $id;
        		$data = $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_products');
        		$this->session->set_flashdata('success','Product updated Successfully');
        	}else{
				$data= $this->vendor_model->insert_data($vendor_data,'tbl_products');
				$productId = $data;
        		$product_session = array('short_description'=>$short_description, 'description'=>$description, 'main_category_id'=>$main_category_id, 'category_id'=>$category_id, 'estimated_delivery_days'=>$estimated_delivery_days, 'returnable'=>$returnable, 'returnable_days'=>$returnable_days, 'not_returnable_reason'=>$not_returnable_reason, 'payment_methods'=>$payment_methods, 'cod'=>$cod, 'no_cod_reason'=>$no_cod_reason, 'international_shipping'=>$international_shipping, 'international_shipping_countries'=>$international_shipping_countries, 'no_international_shipping_reason'=>$no_international_shipping_reason, 'price_including_tax'=>$price_including_tax, 'tax'=>$tax, 'discount_applicable'=>$discount_applicable, 'discount_applicable_if_products'=>$discount_applicable_if_products, 'discount_percent'=>$discount_percent, 'discount_validity'=>$discount_validity, 'delivery_charge'=>$delivery_charge, 'local_delivery_exception_amount'=>$local_delivery_exception_amount, 'non_local_delivery_exception_amount'=>$non_local_delivery_exception_amount, 'local_delivery_charge_amount'=>$local_delivery_charge_amount, 'non_local_delivery_charge_amount'=>$non_local_delivery_charge_amount, 'reward_points_per_item'=>$reward_points_per_item);
				$this->session->set_userdata($product_session);
        		$this->session->set_flashdata('success','Product added Successfully');
			}
			if(empty($id)){
			$length = sizeof($size);
				for($i=0; $i<$length; $i++){

					/*var_dump($productPrice);
					die();*/
					 $att_data = array('product_id'=>$productId, 'size'=>$size[$i],
					 'quantity'=>$quantity[$i],'color_name'=>$colorName[$i]
					 ,'price'=>$productPrice[$i],'images'=>$imagesData[$i]);
					
					if(isset($_FILES['color']['name']) && !empty($_FILES['color']['name'][$i])){

						$files = $_FILES;

						$_FILES['colors']['name']= $files['color']['name'][$i];
		                $_FILES['colors']['type']= $files['color']['type'][$i];
		                $_FILES['colors']['tmp_name']= $files['color']['tmp_name'][$i];
		                $_FILES['colors']['error']= $files['color']['error'][$i];
		                $_FILES['colors']['size']= $files['color']['size'][$i];

			        	$config['upload_path'] = 'uploads/images/product_images/';
			            $config['allowed_types'] = '*';
			            $config['file_name'] = strtolower($product_title)."_color_".$i."_".date("Y_m_d_H_i_s");
			            $this->load->library('upload', $config);
			            $this->upload->initialize($config);
			            if(!$this->upload->do_upload('colors')){
				        	$error =  $this->upload->display_errors();
				        	$this->session->set_flashdata('danger', $error);
				        	redirect('add_product');
				        }else{
				        	$imageData = $this->upload->data();
				            $color =  $imageData['file_name'];
				            $att_data = array_merge($att_data, ['color'=>$color]);
				        }
			        }
			       

					$this->vendor_model->insert_data($att_data,'tbl_product_stock');
					}
				}
        	redirect('add_product');
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('add_product');
		}
	}
	public function delete_product($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$this->vendor_model->delete_data(array('product_id'=>base64_decode($id)),'tbl_product_stock');
		$delete_image = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_products');
		if(!empty($delete_image['banner_image'])){
			unlink("uploads/images/product_images/".$delete_image['banner_image']);
		}
		if(!empty($delete_image['images'])){
			$images = explode(',', $delete_image['images']);

	        foreach($images as $value){
                unlink("uploads/images/product_images/".$value);
            }
		}
		
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_products');
		$this->session->set_flashdata('danger','Product removed successfully');
		redirect('all_products');
	}
	public function delete_product_images($id='', $name='')
	{
		$delete_image = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_products');
		if(!empty($delete_image['images'])){
			$images = explode(',', $delete_image['images']);

	        if (($key = array_search($name, $images)) !== false) {
                unset($images[$key]);
                unlink("uploads/images/product_images/".$name);
            }

            $insert_data = $this->db->where('id',$id)->set('images', implode(',', $images))->update('tbl_products');
		}

		redirect('edit_product/'.base64_encode($id));
	}
	public function delete_product_bank_offers($id='', $name='')
	{
		$delete_bank_offers = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_products');
		if(!empty($delete_bank_offers['bank_offers'])){
			$bank_offers = explode(',', $delete_bank_offers['bank_offers']);

	        if (($key = array_search($name, $bank_offers)) !== false) {
                unset($bank_offers[$key]);
                unlink("uploads/images/product_images/".$name);
            }

            $insert_data = $this->db->where('id',$id)->set('bank_offers', implode(',', $bank_offers))->update('tbl_products');
		}

		redirect('edit_product/'.base64_encode($id));
	}

	public function get_category_list($value='')
	{
		$main_category_id = $this->input->post('main_category_id');
		$category_id = $this->input->post('category_id');
		$data=$this->vendor_model->get_data(array('main_category_id'=>$main_category_id),$table="tbl_categories");
		$msg="<option value=''>Select Category</option>";
		foreach ($data as  $value) {
			$msg.="<option value='".$value['id']."' ";
			if(isset($category_id)){  if($category_id == $value['id']){ 
				$msg.="selected"; 
			}}
			$msg.= ">".$value['category_name']."</option>";
		}
		echo $msg;
		
	}

	public function get_tags($value='')
	{
		$main_category_id = $this->input->post('main_category_id');
		$category_id = $this->input->post('category_id');
		$product_tags = explode(',', $this->input->post('tags'));
		$tags_list = $this->vendor_model->get_single_data(array('main_category_id'=>$main_category_id, 'category_id'=>$category_id), "tbl_tags");
		
		$msg="";
		
		if(!empty($tags_list)){
			$all_tags = explode(',', $tags_list['tags']);
			
			foreach ($all_tags as  $value) {
				$msg.="<input type='checkbox' name='tags[]' value='".$value."' ";
				if(isset($product_tags)){  if(in_array($value, $product_tags)){ 
					$msg.="checked"; 
				}}
				$msg.= "/> ".$value." &nbsp; ";
			}
		}
		echo $msg;	
	}
	public function get_attributes_list($value='')
	{ 
		$main_category_id = $this->input->post('main_category_id');
		$category_id = $this->input->post('category_id');
		$is_add = $this->input->post('is_add');

		$data = $this->vendor_model->get_single_data(array('main_category_id'=>$main_category_id, 'category_id'=>$category_id), $table="tbl_attribute_masters");

		$msg = ''; 
        if (isset($data['attributes'])) {
			$array = explode(',', $data['attributes']);

			foreach ($array as  $value) {
	            $attData = $this->vendor_model->get_single_data(array('id'=>$value), 'tbl_attributes');
	            $attr =  isset($attData['attribute_name']) ? $attData['attribute_name'] : " ";
				$msg .= '<div class="col-md-3"><div class="form-group">';
	            $msg .= '<label>'.$attr.':</label>';
	            $attValues = $this->vendor_model->get_data(array('attribute_id'=>$value), 'tbl_attribute_values');
	            if (!empty($attValues)) {
	            	$msg .= '<select type="text" name="attributes['.str_replace(" ","_","$attr").'][]" class="form-control">';
	            foreach ($attValues as $attValues){
	                $msg .= '<option>'.$attValues['value'].'</option>';
	            }
	            $msg .= '</select>';
	            } else if($attr=='Colour'){
	            	$msg .= '<input type="file" name="'.str_replace(" ","_","$attr").'[]" class="form-control">';     
	            }else {
	            	$msg .= '<input type="number" name="attributes['.str_replace(" ","_","$attr").'][]" class="form-control">';    
	            }
	            $msg .= '</div></div>';
	        }
	        if(empty($is_add)){
		        $msg .= '<div class="col-md-1">
		                    <div class="form-group">
		                        <label style="opacity: 0">add</label>
		                        <input type="button" class="add-new-btn btn form-control" value="+">
		                    </div>
		                </div>';
	        }
			echo $msg;
       	}
	}
	public function listColors(){
		$id = $_POST['id'];
		if(isset($id) && !empty($id)){
			$data['product_attributes'] = $this->vendor_model->get_data(array('product_id'=>$id), 'tbl_product_stock');
			$this->load->view('admin/listColors', $data);
		}
	}
	public function deleteColors(){
		$id = $_POST['id'];
		if(isset($id) && !empty($id)){
			 $this->vendor_model->delete_data(array('id'=>$id), 'tbl_product_stock');
		}
	}
	public function editColors($id){
		$id = base64_decode($id);
		if(isset($id) && !empty($id)){
			$data['product_attributes'] = $this->vendor_model->get_data(array('id'=>$id), 'tbl_product_stock');
			$data['edit'] = "edit";
			$this->load->view('admin/editColors', $data);
		}
	}
	public function updateColors(){
		$size = $this->input->post('size');
		$id = $this->input->post('id');
		$quantity = $this->input->post('quantity');
		$colorName = $this->input->post('colorName');
		$productPrice = $this->input->post('productPrice');
		if(isset($_FILES['images']['name'])){ 
			$files = $_FILES;
			$count = count($_FILES['images']['name']);
			$fileNames = array();
			for($i=0; $i<$count; $i++){
				$_FILES['images']['name']= $files['images']['name'][$i];
				$_FILES['images']['type']= $files['images']['type'][$i];
				$_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
				$_FILES['images']['error']= $files['images']['error'][$i];
				$_FILES['images']['size']= $files['images']['size'][$i];

				$config['upload_path'] = 'uploads/images/product_images/';
				$config['allowed_types'] = '*';
				$config['file_name'] = "color_images_".($i+1).'_'.date("Y_m_d_H_i_s");
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if(!$this->upload->do_upload('images'))
				{
					$error =  $this->upload->display_errors();
					$this->session->set_flashdata('danger', $error);
					redirect('add_product');
				}
				else
				{
					$upload_data = $this->upload->data();
					$fileNames[] = $upload_data['file_name'];
				}
			}
			$imagesData = implode(',', $fileNames);
		}
		$att_data = array( 'size'=>$size,'quantity'=>$quantity,
		'color_name'=>$colorName,'price'=>$productPrice);
		if (isset($imageData) && !empty($imageData)){
			$att_data = array_merge($att_data, ['images'=>$imagesData]);
		}
		if(isset($_FILES['color']['name']) && !empty($_FILES['color']['name'])){
			$files = $_FILES;
			$_FILES['colors']['name']= $files['color']['name'];
			$_FILES['colors']['type']= $files['color']['type'];
			$_FILES['colors']['tmp_name']= $files['color']['tmp_name'];
			$_FILES['colors']['error']= $files['color']['error'];
			$_FILES['colors']['size']= $files['color']['size'];
			$config['upload_path'] = 'uploads/images/product_images/';
			$config['allowed_types'] = '*';
			$config['file_name'] = strtolower($product_title)."_color_".date("Y_m_d_H_i_s");
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('colors')){
				$error =  $this->upload->display_errors();
				$this->session->set_flashdata('danger', $error);
				redirect('add_product');
			}else{
				$imageData = $this->upload->data();
				$color =  $imageData['file_name'];
				//$att_data = array_merge($att_data, ['color'=>$color]);
			}
		}
		if (isset($$color) && !empty($$color)){
			$att_data = array_merge($att_data, ['color'=>$color]);
		}
		$this->vendor_model->update_data($att_data,array('id'=>$id), 'tbl_product_stock');
		redirect('all_products');
	}
}