-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 01, 2021 at 07:42 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `primelog_bsg`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attributes`
--

CREATE TABLE `tbl_attributes` (
  `id` int(11) NOT NULL,
  `attribute_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attributes`
--

INSERT INTO `tbl_attributes` (`id`, `attribute_name`) VALUES
(1, 'Size'),
(2, 'Colour'),
(3, 'Cloth Type'),
(4, 'Quantity'),
(5, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attribute_masters`
--

CREATE TABLE `tbl_attribute_masters` (
  `id` int(11) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `attributes` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attribute_masters`
--

INSERT INTO `tbl_attribute_masters` (`id`, `main_category_id`, `category_id`, `attributes`, `status`, `created_at`) VALUES
(1, 1, 3, '1', 1, '2021-02-15 22:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attribute_values`
--

CREATE TABLE `tbl_attribute_values` (
  `id` int(11) NOT NULL,
  `attribute_id` int(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `tbl_attribute_values`
--

INSERT INTO `tbl_attribute_values` (`id`, `attribute_id`, `value`) VALUES
(1, 1, 'S'),
(2, 1, 'M'),
(3, 1, 'L'),
(4, 1, 'XL'),
(5, 1, 'XXL'),
(6, 3, 'Silk'),
(7, 3, 'Cotton'),
(8, 3, 'Lenin'),
(9, 1, 'XXXL'),
(10, 5, '4XL');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `user_id`, `product_id`, `quantity`, `size`, `color`) VALUES
(1, 1, 1, 1, 'M', 'lehanga_color_1_2020_12_09_14_07_52.jpg'),
(3, 4, 35, 1, 'XL', 'reymond_shirt_color_2_2020_12_13_15_55_23.jpg'),
(4, 1, 59, 1, 'M', 'sweatshirt_color_0_2020_12_15_12_15_49.jpg'),
(8, 2, 58, 1, 'S', 'sweatshirt_color_0_2020_12_15_12_15_49.jpg'),
(9, 4, 73, 1, 'L', 'men_slim_fit_solid_cut_away_collar_casual_shirt_color_1_2020_12_21_23_00_30.jpeg'),
(10, 4, 71, 1, 'XXL', 'men_slim_fit_solid_cut_away_collar_casual_shirt_color_0_2020_12_21_23_00_30.jpeg'),
(12, 4, 138, 1, 'S', 'pantaloons_junior_color_0_2021_01_12_21_55_57.jpg'),
(16, 4, 153, 1, 'L', 'pantaloons_junior_color_2_2021_01_12_23_04_21.jpg'),
(17, 1, 155, 1, 'M', 'primeam_quality_printed_shirts__color_0_2021_01_13_11_21_33.jpeg'),
(23, 1, 153, 1, 'L', 'pantaloons_junior_color_2_2021_01_12_23_04_21.jpg'),
(25, 1, 152, 1, 'M', 'pantaloons_junior_color_1_2021_01_12_23_04_21.png'),
(26, 1, 151, 1, 'S', 'pantaloons_junior_color_0_2021_01_12_23_04_21.jpg'),
(42, 1, 232, 1, 'M', 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_50_27.jpg'),
(43, 1, 259, 1, 'XL', 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_15_05_51.jpg'),
(44, 1, 240, 1, 'XL', 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_14_26_15.jpg'),
(54, 6, 295, 1, 'M', 'american_crew_mens_regular_fit_polos_color_0_2021_02_10_08_30_22.jpg'),
(60, 1, 294, 1, 'S', 'american_crew_mens_regular_fit_polos_color_0_2021_02_10_08_30_22.jpg'),
(61, 5, 300, 1, 'XXL', 'boys_chest_printed_hooded_sweatshirt_color_0_2021_02_11_13_17_51.jpg'),
(62, 1, 308, 2, 'L', 'manyavar_color_2_2021_02_14_21_01_30.jpg'),
(64, 2, 311, 1, '38', 'shirt_color_0_2021_02_16_09_17_11.jpg'),
(65, 4, 311, 1, '38', 'shirt_color_0_2021_02_16_09_17_11.jpg'),
(66, 1, 207, 1, 'S', 'arun_shirt_color_0_2021_02_01_22_44_20.jpg'),
(68, 7, 324, 1, 'M', 'pyjamas_color_0_2021_02_17_13_07_04.jpg'),
(69, 5, 335, 1, 'M', 'regularfit_cheks_shirts_color_0_2021_02_17_19_58_10.jpeg'),
(73, 1, 359, 1, 'M', 'indigo_print_shirts__color_0_2021_02_26_12_59_15.jpg'),
(78, 4, 357, 1, 'XL', 'indigo_print_shirts_with_regular_fit_color_0_2021_02_26_10_45_09.jpg'),
(79, 2, 364, 1, 'L', 'plain_shirt_color_1_2021_02_26_13_29_07.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`id`, `category_name`, `category_image`, `main_category_id`, `status`, `created_at`) VALUES
(2, 'Indian Wear', 'indian_wear_2020_12_21_22_31_44.jpg', 1, 1, '2020-12-21 22:31:44'),
(3, 'Casual Shirts', 'casual_shirts_2020_12_21_22_32_03.jpg', 1, 1, '2020-12-21 22:32:03'),
(4, 'Inner Wear', 'inner_wear_2020_12_21_22_32_28.jpg', 1, 1, '2020-12-21 22:32:28'),
(5, 'Jeans', 'jeans_2020_12_21_22_32_43.jpg', 1, 1, '2020-12-21 22:32:43'),
(6, 'Track Pants', 'track_pants_2020_12_21_22_32_56.JPG', 1, 1, '2020-12-21 22:32:56'),
(7, 'T-Shirts', 't-shirts_2020_12_21_22_33_08.jpg', 1, 1, '2020-12-21 22:33:08'),
(8, 'Accessories', 'accessories_2020_12_21_22_34_59.jpg', 1, 1, '2020-12-21 22:34:59'),
(9, 'Trousers', 'trousers_2021_01_12_14_22_17.jpg', 1, 1, '2021-01-12 14:22:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_create_sale_items`
--

CREATE TABLE `tbl_create_sale_items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `products` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_top_banners`
--

CREATE TABLE `tbl_home_top_banners` (
  `id` int(11) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `linked_to` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_home_top_banners`
--

INSERT INTO `tbl_home_top_banners` (`id`, `banner_image`, `main_category_id`, `discount`, `linked_to`, `status`) VALUES
(1, 'home_top_banner_2020_10_19_13_35_45.jpg', 1, 0, 1, 1),
(2, 'home_top_banner_2020_10_19_13_37_34.jpg', 1, 0, NULL, 1),
(3, 'home_top_banner_2020_10_19_13_37_52.jpg', 1, 0, NULL, 1),
(4, 'home_top_banner_2020_11_16_23_04_33.jpg', 1, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_main_categories`
--

CREATE TABLE `tbl_main_categories` (
  `id` int(11) NOT NULL,
  `main_category_name` varchar(255) NOT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_main_categories`
--

INSERT INTO `tbl_main_categories` (`id`, `main_category_name`, `url`) VALUES
(1, 'Mens', 'mens'),
(2, 'Kids', 'kids');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `order_total` int(255) NOT NULL,
  `delivery_charge` float DEFAULT NULL,
  `discount_amount` float DEFAULT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `payment_order_id` varchar(255) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`id`, `user_id`, `order_total`, `delivery_charge`, `discount_amount`, `payment_method`, `payment_status`, `payment_id`, `payment_order_id`, `address_id`, `order_status`, `created_on`) VALUES
(1, 4, 300, NULL, NULL, 'online', 'captured', 'pay_GDDsILRPze9raB', 'order_GDDru5Dy12hPpm', 3, '1', '2020-12-15 06:51:15'),
(2, 4, 1100, NULL, NULL, 'online', 'captured', 'pay_GDN0Ei4KcQLpN6', 'order_GDN06OUStNwEBi', 3, '1', '2020-12-15 15:47:01'),
(3, 4, 200, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '1', '2020-12-17 11:42:28'),
(4, 4, 221, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '1', '2020-12-17 11:57:26'),
(5, 4, 2110, NULL, NULL, 'online', 'captured', 'pay_GG8RG7KOLO3OCx', 'order_GG8R8nFSs7PPwp', 3, '1', '2020-12-22 15:29:12'),
(6, 4, 600, NULL, NULL, 'online', 'captured', 'pay_GOSeOg61guzgHD', 'order_GOSe5ZWpxgVEXl', 3, '1', '2021-01-12 16:27:32'),
(7, 4, 600, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '1', '2021-01-13 05:11:02'),
(8, 1, 1103, NULL, NULL, 'cod', NULL, NULL, NULL, 5, '1', '2021-01-13 07:19:43'),
(9, 6, 588, NULL, NULL, 'cod', NULL, NULL, NULL, 20, '1', '2021-01-14 04:49:50'),
(10, 6, 588, NULL, NULL, 'online', 'captured', 'pay_GP7QPdXVtxZib2', 'order_GP7N5QVNpBX8YC', 20, '1', '2021-01-14 08:20:51'),
(11, 6, 588, NULL, NULL, 'online', 'captured', 'pay_GP7Tz04v52HpWx', 'order_GP7Rtdfh6b0ujf', 20, '1', '2021-01-14 08:24:08'),
(12, 6, 588, NULL, NULL, 'online', 'captured', 'pay_GP7VyrtR63PxSV', 'order_GP7VX6ZIiX7J6A', 20, '1', '2021-01-14 08:26:03'),
(13, 6, 588, NULL, NULL, 'online', 'captured', 'pay_GP7Xiagv19jngO', 'order_GP7XOEqXOlrmzE', 20, '1', '2021-01-14 08:27:39'),
(14, 6, 588, NULL, NULL, 'cod', NULL, NULL, NULL, 20, '1', '2021-01-14 08:28:41'),
(15, 4, 4000, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '1', '2021-02-04 13:31:58'),
(16, 1, 1169, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-07 08:00:30'),
(17, 1, 1169, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-07 08:02:54'),
(18, 1, 570, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-07 08:07:28'),
(19, 1, 564, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-07 08:23:35'),
(20, 5, 341, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-08 17:03:28'),
(21, 1, 570, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-08 17:49:06'),
(22, 1, 570, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-08 17:51:08'),
(23, 1, 570, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-08 18:00:16'),
(24, 4, 650, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '5', '2021-02-09 14:53:54'),
(25, 4, 301, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '5', '2021-02-09 15:43:33'),
(26, 1, 1188, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-10 09:33:21'),
(27, 1, 1176, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-10 09:34:37'),
(28, 1, 1152, NULL, NULL, 'cod', NULL, NULL, NULL, 0, '1', '2021-02-11 07:56:05'),
(29, 4, 300, NULL, NULL, 'cod', NULL, NULL, NULL, 3, '5', '2021-02-16 03:49:26'),
(30, 7, 433, 100, NULL, 'cod', NULL, NULL, NULL, 21, '1', '2021-02-17 07:08:42'),
(31, 5, 20, 0, NULL, 'online', 'captured', 'pay_Gg8BDyHN0k2PNi', 'order_Gg87lGC4jJs1MZ', 6, '5', '2021-02-26 08:08:55'),
(32, 8, 40, 0, NULL, 'online', 'captured', 'pay_GnEq3dyiW6fjtS', 'order_GnEpmoSV8eKXHM', 22, '1', '2021-03-16 07:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_products`
--

CREATE TABLE `tbl_order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) NOT NULL,
  `product_info` text NOT NULL,
  `order_product_status` varchar(255) NOT NULL,
  `status_info` varchar(255) DEFAULT NULL,
  `product_price` float NOT NULL,
  `tracking_id` varchar(255) DEFAULT NULL,
  `delivery_partner` varchar(255) DEFAULT NULL,
  `placed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_products`
--

INSERT INTO `tbl_order_products` (`id`, `order_id`, `product_id`, `color`, `size`, `quantity`, `product_info`, `order_product_status`, `status_info`, `product_price`, `tracking_id`, `delivery_partner`, `placed_on`) VALUES
(1, 1, 58, 'sweatshirt_color_0_2020_12_15_12_15_49.jpg', 'S', '3', '{\"id\":\"58\",\"product_id\":\"37\",\"color\":\"sweatshirt_color_0_2020_12_15_12_15_49.jpg\",\"color_name\":\"Red\",\"size\":\"S\",\"clothtype\":null,\"images\":\"sweatshirt_images_1_2020_12_15_12_15_49.jpg,sweatshirt_images_2_2020_12_15_12_15_49.jpg,sweatshirt_images_3_2020_12_15_12_15_49.jpg\",\"quantity\":\"10\",\"price\":\"100\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-15 06:45:49\"}', '1', '{\"1\":\"15 Dec 2020 12:21:15 pm\"}', 100, NULL, NULL, '2020-12-15 06:51:15'),
(2, 2, 56, 'salwar_suit_color_0_2020_12_14_14_19_13.jpg', 'S', '4', '{\"id\":\"56\",\"product_id\":\"36\",\"color\":\"salwar_suit_color_0_2020_12_14_14_19_13.jpg\",\"color_name\":\"Blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"salwar_suit_images_1_2020_12_14_14_19_13.jpg,salwar_suit_images_2_2020_12_14_14_19_13.jpg,salwar_suit_images_3_2020_12_14_14_19_13.jpg,salwar_suit_images_4_2020_12_14_14_19_13.jpg\",\"quantity\":\"20\",\"price\":\"200\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-14 08:49:13\"}', '1', '{\"1\":\"15 Dec 2020 09:17:01 pm\"}', 200, NULL, NULL, '2020-12-15 15:47:01'),
(3, 2, 59, 'sweatshirt_color_1_2020_12_15_12_15_49.jpg', 'M', '1', '{\"id\":\"59\",\"product_id\":\"37\",\"color\":\"sweatshirt_color_1_2020_12_15_12_15_49.jpg\",\"color_name\":\"Red\",\"size\":\"M\",\"clothtype\":null,\"images\":\"sweatshirt_images_1_2020_12_15_12_15_491.jpg,sweatshirt_images_2_2020_12_15_12_15_491.jpg,sweatshirt_images_3_2020_12_15_12_15_491.jpg\",\"quantity\":\"20\",\"price\":\"300\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-15 06:45:49\"}', '1', '{\"1\":\"15 Dec 2020 09:17:01 pm\"}', 300, NULL, NULL, '2020-12-15 15:47:01'),
(4, 3, 56, 'salwar_suit_color_0_2020_12_14_14_19_13.jpg', 'S', '1', '{\"id\":\"56\",\"product_id\":\"36\",\"color\":\"salwar_suit_color_0_2020_12_14_14_19_13.jpg\",\"color_name\":\"Blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"salwar_suit_images_1_2020_12_14_14_19_13.jpg,salwar_suit_images_2_2020_12_14_14_19_13.jpg,salwar_suit_images_3_2020_12_14_14_19_13.jpg,salwar_suit_images_4_2020_12_14_14_19_13.jpg\",\"quantity\":\"20\",\"price\":\"200\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-14 08:49:13\"}', '1', '{\"1\":\"17 Dec 2020 05:12:28 pm\"}', 200, NULL, NULL, '2020-12-17 11:42:28'),
(5, 4, 56, 'salwar_suit_color_0_2020_12_14_14_19_13.jpg', 'S', '1', '{\"id\":\"56\",\"product_id\":\"36\",\"color\":\"salwar_suit_color_0_2020_12_14_14_19_13.jpg\",\"color_name\":\"Blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"salwar_suit_images_1_2020_12_14_14_19_13.jpg,salwar_suit_images_2_2020_12_14_14_19_13.jpg,salwar_suit_images_3_2020_12_14_14_19_13.jpg,salwar_suit_images_4_2020_12_14_14_19_13.jpg\",\"quantity\":\"20\",\"price\":\"200\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-14 08:49:13\"}', '1', '{\"1\":\"17 Dec 2020 05:27:26 pm\"}', 200, NULL, NULL, '2020-12-17 11:57:26'),
(6, 4, 57, 'salwar_suit_color_1_2020_12_14_14_19_13.jpg', 'L', '1', '{\"id\":\"57\",\"product_id\":\"36\",\"color\":\"salwar_suit_color_1_2020_12_14_14_19_13.jpg\",\"color_name\":\"Green\",\"size\":\"L\",\"clothtype\":null,\"images\":\"salwar_suit_images_1_2020_12_14_14_19_131.jpg,salwar_suit_images_2_2020_12_14_14_19_131.jpg,salwar_suit_images_3_2020_12_14_14_19_131.jpg,salwar_suit_images_4_2020_12_14_14_19_131.jpg\",\"quantity\":\"20\",\"price\":\"21\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-14 08:49:13\"}', '1', '{\"1\":\"17 Dec 2020 05:27:26 pm\"}', 21, NULL, NULL, '2020-12-17 11:57:26'),
(7, 5, 55, 'reymond_shirt_color_2_2020_12_13_15_55_23.jpg', 'XL', '2', '{\"id\":\"55\",\"product_id\":\"35\",\"color\":\"reymond_shirt_color_2_2020_12_13_15_55_23.jpg\",\"color_name\":\"blue\",\"size\":\"XL\",\"clothtype\":null,\"images\":\"reymond_shirt_images_1_2020_12_13_15_55_232.jpg,reymond_shirt_images_2_2020_12_13_15_55_232.jpg\",\"quantity\":\"25\",\"price\":\"765\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-13 10:25:23\"}', '1', '{\"1\":\"22 Dec 2020 08:59:12 pm\"}', 765, NULL, NULL, '2020-12-22 15:29:12'),
(8, 5, 56, 'salwar_suit_color_0_2020_12_14_14_19_13.jpg', 'S', '1', '{\"id\":\"56\",\"product_id\":\"36\",\"color\":\"salwar_suit_color_0_2020_12_14_14_19_13.jpg\",\"color_name\":\"Blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"salwar_suit_images_1_2020_12_14_14_19_13.jpg,salwar_suit_images_2_2020_12_14_14_19_13.jpg,salwar_suit_images_3_2020_12_14_14_19_13.jpg,salwar_suit_images_4_2020_12_14_14_19_13.jpg\",\"quantity\":\"20\",\"price\":\"200\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-14 08:49:13\"}', '1', '{\"1\":\"22 Dec 2020 08:59:12 pm\"}', 200, NULL, NULL, '2020-12-22 15:29:12'),
(9, 5, 71, 'men_slim_fit_solid_cut_away_collar_casual_shirt_color_0_2020_12_21_23_00_30.jpeg', 'XXL', '1', '{\"id\":\"71\",\"product_id\":\"38\",\"color\":\"men_slim_fit_solid_cut_away_collar_casual_shirt_color_0_2020_12_21_23_00_30.jpeg\",\"color_name\":\"Blue\",\"size\":\"XXL\",\"clothtype\":null,\"images\":\"men_slim_fit_solid_cut_away_collar_casual_shirt_images_1_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_2_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_3_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_4_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_5_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_6_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_7_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_8_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_9_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_10_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_11_2020_12_21_23_00_30.jpeg,men_slim_fit_solid_cut_away_collar_casual_shirt_images_12_2020_12_21_23_00_30.jpeg\",\"quantity\":\"2\",\"price\":\"380\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2020-12-21 17:30:30\"}', '1', '{\"1\":\"22 Dec 2020 08:59:12 pm\"}', 380, NULL, NULL, '2020-12-22 15:29:12'),
(10, 6, 136, 'anirudh_shirt_color_2_2021_01_12_21_08_12.jpg', 'L', '1', '{\"id\":\"136\",\"product_id\":\"61\",\"color\":\"anirudh_shirt_color_2_2021_01_12_21_08_12.jpg\",\"color_name\":\"blue\",\"size\":\"L\",\"clothtype\":null,\"images\":\"anirudh_shirt_images_1_2021_01_12_21_08_122.jpg,anirudh_shirt_images_2_2021_01_12_21_08_122.jpg\",\"quantity\":\"16\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-12 15:38:12\"}', '1', '{\"1\":\"12 Jan 2021 09:57:32 pm\"}', 600, NULL, NULL, '2021-01-12 16:27:32'),
(11, 7, 136, 'anirudh_shirt_color_2_2021_01_12_21_08_12.jpg', 'L', '1', '{\"id\":\"136\",\"product_id\":\"61\",\"color\":\"anirudh_shirt_color_2_2021_01_12_21_08_12.jpg\",\"color_name\":\"blue\",\"size\":\"L\",\"clothtype\":null,\"images\":\"anirudh_shirt_images_1_2021_01_12_21_08_122.jpg,anirudh_shirt_images_2_2021_01_12_21_08_122.jpg\",\"quantity\":\"16\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-12 15:38:12\"}', '1', '{\"1\":\"13 Jan 2021 10:41:02 am\"}', 600, NULL, NULL, '2021-01-13 05:11:02'),
(12, 8, 151, 'pantaloons_junior_color_0_2021_01_12_23_04_21.jpg', 'S', '1', '{\"id\":\"151\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_0_2021_01_12_23_04_21.jpg\",\"color_name\":\"dark blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_12_23_04_21.jpg,pantaloons_junior_images_2_2021_01_12_23_04_21.jpg\",\"quantity\":\"1\",\"price\":\"526\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-12 17:34:21\"}', '1', '{\"1\":\"13 Jan 2021 12:49:43 pm\"}', 526, NULL, NULL, '2021-01-13 07:19:43'),
(13, 8, 152, 'pantaloons_junior_color_1_2021_01_12_23_04_21.png', 'M', '1', '{\"id\":\"152\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_12_23_04_21.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_12_23_04_211.jpg,pantaloons_junior_images_2_2021_01_12_23_04_211.jpg\",\"quantity\":\"0\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-12 17:34:21\"}', '1', '{\"1\":\"13 Jan 2021 12:49:43 pm\"}', 600, NULL, NULL, '2021-01-13 07:19:43'),
(14, 9, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 10:19:50 am\"}', 600, NULL, NULL, '2021-01-14 04:49:50'),
(15, 10, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 01:50:51 pm\"}', 600, NULL, NULL, '2021-01-14 08:20:51'),
(16, 11, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 01:54:08 pm\"}', 600, NULL, NULL, '2021-01-14 08:24:08'),
(17, 12, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 01:56:03 pm\"}', 600, NULL, NULL, '2021-01-14 08:26:03'),
(18, 13, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 01:57:39 pm\"}', 600, NULL, NULL, '2021-01-14 08:27:39'),
(19, 14, 178, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'M', '1', '{\"id\":\"178\",\"product_id\":\"62\",\"color\":\"pantaloons_junior_color_1_2021_01_13_22_23_46.png\",\"color_name\":\"black\",\"size\":\"M\",\"clothtype\":null,\"images\":\"pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-13 16:53:46\"}', '1', '{\"1\":\"14 Jan 2021 01:58:41 pm\"}', 600, NULL, NULL, '2021-01-14 08:28:41'),
(20, 15, 208, 'arun_shirt_color_1_2021_02_01_22_44_20.jpg', 'M', '1', '{\"id\":\"208\",\"product_id\":\"82\",\"color\":\"arun_shirt_color_1_2021_02_01_22_44_20.jpg\",\"color_name\":\"Blue\",\"size\":\"M\",\"clothtype\":null,\"images\":\"arun_shirt_images_1_2021_02_01_22_44_201.jpg,arun_shirt_images_2_2021_02_01_22_44_201.jpg,arun_shirt_images_3_2021_02_01_22_44_201.jpg,arun_shirt_images_4_2021_02_01_22_44_201.jpg\",\"quantity\":\"20\",\"price\":\"4000\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-01 17:14:20\"}', '1', '{\"1\":\"04 Feb 2021 07:01:58 pm\"}', 4000, NULL, NULL, '2021-02-04 13:31:58'),
(21, 16, 228, 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg', 'S', '2', '{\"id\":\"228\",\"product_id\":\"92\",\"color\":\"american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"S\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_6_2021_02_07_13_22_28.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-07 07:52:28\"}', '1', '{\"1\":\"07 Feb 2021 01:30:30 pm\"}', 599, NULL, NULL, '2021-02-07 08:00:30'),
(22, 17, 228, 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg', 'S', '2', '{\"id\":\"228\",\"product_id\":\"92\",\"color\":\"american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"S\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_07_13_22_28.jpg,american_crew_mens_regular_fit_polos_images_6_2021_02_07_13_22_28.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-07 07:52:28\"}', '1', '{\"1\":\"07 Feb 2021 01:32:54 pm\"}', 599, NULL, NULL, '2021-02-07 08:02:54'),
(23, 18, 228, 'american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg', 'S', '1', '{\"id\":\"228\",\"product_id\":\"92\",\"color\":\"american_crew_mens_regular_fit_polos_color_0_2021_02_07_13_22_28.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"S\",\"clothtype\":null,\"images\":\"color_images_1_2021_02_07_13_35_42.jpg,color_images_2_2021_02_07_13_35_42.jpg,color_images_3_2021_02_07_13_35_42.jpg,color_images_4_2021_02_07_13_35_42.jpg,color_images_5_2021_02_07_13_35_42.jpg,color_images_6_2021_02_07_13_35_42.jpg,color_images_7_2021_02_07_13_35_42.jpg,color_images_8_2021_02_07_13_35_42.jpg,color_images_9_2021_02_07_13_35_42.jpg,color_images_10_2021_02_07_13_35_42.jpg,color_images_11_2021_02_07_13_35_42.jpg,color_images_12_2021_02_07_13_35_42.jpg,color_images_13_2021_02_07_13_35_42.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-07 07:52:28\"}', '1', '{\"1\":\"07 Feb 2021 01:37:28 pm\"}', 599, NULL, NULL, '2021-02-07 08:07:28'),
(24, 19, 230, 'american_crew_mens_regular_fit_polos_color_1_2021_02_07_13_47_03.jpg', 'M', '1', '{\"id\":\"230\",\"product_id\":\"93\",\"color\":\"american_crew_mens_regular_fit_polos_color_1_2021_02_07_13_47_03.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"M\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_07_13_47_031.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_07_13_47_031.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_07_13_47_031.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_07_13_47_031.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_07_13_47_031.jpg,american_crew_mens_regular_fit_polos_images_6_2021_02_07_13_47_031.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-07 08:17:03\"}', '1', '{\"1\":\"07 Feb 2021 01:53:35 pm\"}', 599, NULL, NULL, '2021-02-07 08:23:35'),
(25, 20, 260, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_0_2021_02_08_22_22_27', 'M', '1', '{\"id\":\"260\",\"product_id\":\"105\",\"color\":\"primeam_quality_regularfit_printed_shirts*_?340.00_color_0_2021_02_08_22_22_27\",\"color_name\":\"Orange\",\"size\":\"M\",\"clothtype\":null,\"images\":\"primeam_quality_regularfit_printed_shirts*_?340.00_images_1_2021_02_08_22_22_27\",\"quantity\":\"1\",\"price\":\"340\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-08 16:52:27\"}', '1', '{\"1\":\"08 Feb 2021 10:33:28 pm\"}', 340, NULL, NULL, '2021-02-08 17:03:28'),
(26, 21, 276, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_23_16_52.jpg', 'S', '1', '{\"id\":\"276\",\"product_id\":\"109\",\"color\":\"american_crew_mens_regular_fit_polos_color_1_2021_02_08_23_16_52.jpg\",\"color_name\":\"Royal Blue & Navy Melange\",\"size\":\"S\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_08_23_16_52.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-08 17:46:52\"}', '1', '{\"1\":\"08 Feb 2021 11:19:06 pm\"}', 599, NULL, NULL, '2021-02-08 17:49:06'),
(27, 22, 274, 'none', 'XXL', '1', '{\"id\":\"274\",\"product_id\":\"108\",\"color\":null,\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"XXL\",\"clothtype\":null,\"images\":null,\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-08 17:08:39\"}', '1', '{\"1\":\"08 Feb 2021 11:21:08 pm\"}', 599, NULL, NULL, '2021-02-08 17:51:08'),
(28, 23, 282, 'american_crew_mens_regular_fit_polos_color_3_2021_02_08_23_28_36.jpg', 'XL', '1', '{\"id\":\"282\",\"product_id\":\"110\",\"color\":\"american_crew_mens_regular_fit_polos_color_3_2021_02_08_23_28_36.jpg\",\"color_name\":\"blue\",\"size\":\"XL\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_28_363.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_28_363.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_28_363.jpg\",\"quantity\":\"1\",\"price\":\"599\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-08 17:58:36\"}', '1', '{\"1\":\"08 Feb 2021 11:30:16 pm\"}', 599, NULL, NULL, '2021-02-08 18:00:16'),
(29, 24, 184, 'anirudh_shirt_color_3_2021_01_15_14_54_45.jpg', 'XL', '1', '{\"id\":\"184\",\"product_id\":\"61\",\"color\":\"anirudh_shirt_color_3_2021_01_15_14_54_45.jpg\",\"color_name\":\"blue\",\"size\":\"XL\",\"clothtype\":null,\"images\":\"anirudh_shirt_images_1_2021_01_15_14_54_453.jpg,anirudh_shirt_images_2_2021_01_15_14_54_453.jpg,anirudh_shirt_images_3_2021_01_15_14_54_453.jpg,anirudh_shirt_images_4_2021_01_15_14_54_453.jpg,anirudh_shirt_images_5_2021_01_15_14_54_453.jpg,anirudh_shirt_images_6_2021_01_15_14_54_453.jpg,anirudh_shirt_images_7_2021_01_15_14_54_453.jpg,anirudh_shirt_images_8_2021_01_15_14_54_453.jpg\",\"quantity\":\"15\",\"price\":\"650\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-01-15 09:24:45\"}', '2', '{\"1\":\"09 Feb 2021 08:23:54 pm\",\"2\":\"09 Feb 2021 09:25:54 pm\"}', 650, NULL, NULL, '2021-02-09 14:53:54'),
(30, 25, 291, 'zara_kurti_one_color_0_2021_02_09_21_05_09.jpg', 'S', '1', '{\"id\":\"291\",\"product_id\":\"121\",\"color\":\"zara_kurti_one_color_0_2021_02_09_21_05_09.jpg\",\"color_name\":\"Blue\",\"size\":\"S\",\"clothtype\":null,\"images\":\"zara_kurti_one_images_1_2021_02_09_21_05_09.jpg,zara_kurti_one_images_2_2021_02_09_21_05_09.jpg,zara_kurti_one_images_3_2021_02_09_21_05_09.jpg\",\"quantity\":\"20\",\"price\":\"300\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-09 15:35:09\"}', '2', '{\"1\":\"09 Feb 2021 09:13:33 pm\",\"2\":\"09 Feb 2021 09:23:24 pm\"}', 300, NULL, NULL, '2021-02-09 15:43:33'),
(31, 26, 295, 'american_crew_mens_regular_fit_polos_color_1_2021_02_10_08_30_22.jpg', 'M', '2', '{\"id\":\"295\",\"product_id\":\"122\",\"color\":\"american_crew_mens_regular_fit_polos_color_1_2021_02_10_08_30_22.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"M\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_10_08_30_221.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_10_08_30_221.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_10_08_30_221.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-10 03:00:22\"}', '1', '{\"1\":\"10 Feb 2021 03:03:21 pm\"}', 600, NULL, NULL, '2021-02-10 09:33:21'),
(32, 27, 294, 'american_crew_mens_regular_fit_polos_color_0_2021_02_10_08_30_22.jpg', 'S', '2', '{\"id\":\"294\",\"product_id\":\"122\",\"color\":\"american_crew_mens_regular_fit_polos_color_0_2021_02_10_08_30_22.jpg\",\"color_name\":\"Grey Melange & Anthra Melange\",\"size\":\"S\",\"clothtype\":null,\"images\":\"american_crew_mens_regular_fit_polos_images_1_2021_02_10_08_30_22.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_10_08_30_22.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_10_08_30_22.jpg\",\"quantity\":\"1\",\"price\":\"600\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-10 03:00:22\"}', '1', '{\"1\":\"10 Feb 2021 03:04:37 pm\"}', 600, NULL, NULL, '2021-02-10 09:34:37'),
(33, 28, 299, 'boys_chest_printed_hooded_sweatshirt_color_3_2021_02_11_13_17_52.jpg', 'XL', '4', '{\"id\":\"299\",\"product_id\":\"125\",\"color\":\"boys_chest_printed_hooded_sweatshirt_color_3_2021_02_11_13_17_52.jpg\",\"color_name\":\"grey\",\"size\":\"XL\",\"clothtype\":null,\"images\":\"boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_513.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_513.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_513.jpg\",\"quantity\":\"1\",\"price\":\"300\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-11 07:47:52\"}', '1', '{\"1\":\"11 Feb 2021 01:26:05 pm\"}', 300, NULL, NULL, '2021-02-11 07:56:05'),
(34, 29, 311, 'shirt_color_0_2021_02_16_09_17_11.jpg', '38', '1', '{\"id\":\"311\",\"product_id\":\"128\",\"color\":\"shirt_color_0_2021_02_16_09_17_11.jpg\",\"color_name\":\"red\",\"size\":\"38\",\"clothtype\":null,\"images\":\"shirt_images_1_2021_02_16_09_17_11.jpg,shirt_images_2_2021_02_16_09_17_11.jpg\",\"quantity\":\"30\",\"price\":\"300\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-16 03:47:11\"}', '2', '{\"1\":\"16 Feb 2021 09:19:26 am\",\"2\":\"17 Feb 2021 05:48:24 pm\"}', 300, NULL, NULL, '2021-02-16 03:49:26'),
(35, 30, 321, 'black_tshirt_color_4_2021_02_17_12_25_28.jpg', 'M', '1', '{\"id\":\"321\",\"product_id\":\"130\",\"color\":\"black_tshirt_color_4_2021_02_17_12_25_28.jpg\",\"color_name\":\"red\",\"size\":\"M\",\"clothtype\":null,\"images\":\"black_tshirt_images_1_2021_02_17_12_25_284.jpg,black_tshirt_images_2_2021_02_17_12_25_284.jpg\",\"quantity\":\"2\",\"price\":\"350\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-17 06:55:28\"}', '1', '{\"1\":\"17 Feb 2021 12:38:42 pm\"}', 350, NULL, NULL, '2021-02-17 07:08:42'),
(36, 31, 363, 'plain_shirt_color_0_2021_02_26_13_29_07.jpg', 'M', '1', '{\"id\":\"363\",\"product_id\":\"142\",\"color\":\"plain_shirt_color_0_2021_02_26_13_29_07.jpg\",\"color_name\":\"light pink\",\"size\":\"M\",\"clothtype\":null,\"images\":\"plain_shirt_images_1_2021_02_26_13_29_07.jpg,plain_shirt_images_2_2021_02_26_13_29_07.jpg,plain_shirt_images_3_2021_02_26_13_29_07.jpg,plain_shirt_images_4_2021_02_26_13_29_07.jpg,plain_shirt_images_5_2021_02_26_13_29_07.jpg,plain_shirt_images_6_2021_02_26_13_29_07.jpg,plain_shirt_images_7_2021_02_26_13_29_07.jpg\",\"quantity\":\"5\",\"price\":\"20\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-26 07:59:07\"}', '4', '{\"1\":\"26 Feb 2021 01:38:55 pm\",\"2\":\"26 Feb 2021 01:43:49 pm\",\"4\":\"26 Feb 2021 01:45:16 pm\"}', 20, '14458875212', 'Bsg Delivery', '2021-02-26 08:08:55'),
(37, 32, 365, 'plain_shirt_color_2_2021_02_26_13_29_07.jpg', 'XL', '1', '{\"id\":\"365\",\"product_id\":\"142\",\"color\":\"plain_shirt_color_2_2021_02_26_13_29_07.jpg\",\"color_name\":\"rama \",\"size\":\"XL\",\"clothtype\":null,\"images\":\"plain_shirt_images_1_2021_02_26_13_29_072.jpg\",\"quantity\":\"5\",\"price\":\"40\",\"sold_out\":\"0\",\"in_stock\":\"0\",\"created_on\":\"2021-02-26 07:59:07\"}', '1', '{\"1\":\"16 Mar 2021 12:42:58 pm\"}', 40, NULL, NULL, '2021-03-16 07:12:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_return_products`
--

CREATE TABLE `tbl_order_return_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `return_issue` varchar(255) DEFAULT NULL,
  `comments` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_otp`
--

CREATE TABLE `tbl_otp` (
  `id` int(11) NOT NULL,
  `mobile_number` varchar(100) NOT NULL,
  `otp` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_otp`
--

INSERT INTO `tbl_otp` (`id`, `mobile_number`, `otp`, `created_at`) VALUES
(1, '8639930690', '324410', '2020-11-06 23:04:04'),
(2, '9052304477', '273782', '2020-11-06 23:04:44'),
(3, '7801019797', '824217', '2020-11-10 21:39:03'),
(4, '9177852662', '542937', '2021-01-12 15:36:19'),
(5, '8527193357', '420977', '2021-02-17 12:29:33'),
(6, '7989328565', '760059', '2021-03-16 12:37:46'),
(7, '9493220266', '693016', '2021-03-18 08:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_our_advantages`
--

CREATE TABLE `tbl_our_advantages` (
  `id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_our_advantages`
--

INSERT INTO `tbl_our_advantages` (`id`, `images`) VALUES
(1, 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `main_category_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `attributes` text NOT NULL,
  `estimated_delivery_days` int(11) NOT NULL,
  `returnable` int(11) DEFAULT NULL,
  `returnable_days` int(11) DEFAULT NULL,
  `not_returnable_reason` varchar(255) DEFAULT NULL,
  `payment_methods` varchar(100) NOT NULL,
  `cod` int(11) DEFAULT NULL,
  `no_cod_reason` varchar(255) DEFAULT NULL,
  `international_shipping` int(11) NOT NULL,
  `international_shipping_countries` varchar(255) DEFAULT NULL,
  `no_international_shipping_reason` varchar(255) DEFAULT NULL,
  `product_price` float NOT NULL,
  `price_including_tax` int(11) NOT NULL,
  `tax` float DEFAULT NULL,
  `discount_applicable` int(11) NOT NULL,
  `discount_applicable_if_products` int(11) DEFAULT NULL COMMENT 'On purchase of how many items discount to be applied',
  `discount_percent` int(11) DEFAULT NULL,
  `discount_validity` date DEFAULT NULL,
  `delivery_charge` int(11) NOT NULL,
  `local_delivery_exception_amount` float DEFAULT NULL,
  `non_local_delivery_exception_amount` float DEFAULT NULL,
  `local_delivery_charge_amount` float DEFAULT NULL COMMENT 'Up to which amount delivery fee will be charged',
  `non_local_delivery_charge_amount` float DEFAULT NULL,
  `reward_points_per_item` int(11) DEFAULT NULL,
  `related_products` varchar(255) DEFAULT NULL,
  `banner_image` varchar(255) NOT NULL,
  `images` text,
  `videos` varchar(255) DEFAULT NULL,
  `our_advantages` varchar(255) DEFAULT NULL,
  `bank_offers` varchar(255) DEFAULT NULL,
  `tags` text,
  `sleeve_length` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `design` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`id`, `product_title`, `short_description`, `description`, `main_category_id`, `category_id`, `attributes`, `estimated_delivery_days`, `returnable`, `returnable_days`, `not_returnable_reason`, `payment_methods`, `cod`, `no_cod_reason`, `international_shipping`, `international_shipping_countries`, `no_international_shipping_reason`, `product_price`, `price_including_tax`, `tax`, `discount_applicable`, `discount_applicable_if_products`, `discount_percent`, `discount_validity`, `delivery_charge`, `local_delivery_exception_amount`, `non_local_delivery_exception_amount`, `local_delivery_charge_amount`, `non_local_delivery_charge_amount`, `reward_points_per_item`, `related_products`, `banner_image`, `images`, `videos`, `our_advantages`, `bank_offers`, `tags`, `sleeve_length`, `type`, `design`, `status`, `created_at`) VALUES
(35, 'Reymond Shirt', 'Raymond Shirt', '&lt;p&gt;Raymond Shirt&lt;/p&gt;\n', 1, 1, '', 15, 1, 16, '', 'All', 1, '', 0, '', 'Demostic only', 77, 1, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 3, '0', 'reymond_shirt_2020_12_13_15_55_23.jpg', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-13 15:55:23'),
(36, 'Salwar Suit', 'Beautiful Salwar suit', '&lt;p&gt;Beautiful Salwar Suit&lt;/p&gt;\n', 1, 1, '', 20, 1, 1, '', 'All', 1, '', 1, 'USA', '', 999, 1, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', 'salwar_suit_2020_12_14_14_19_13.jpg', NULL, '', 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png', NULL, NULL, NULL, NULL, NULL, 1, '2020-12-14 14:19:13'),
(37, 'Sweatshirt', 'Short Description', '&lt;p&gt;Long Description&lt;/p&gt;\n', 1, 1, '', 3, 1, 20, '', 'All', 1, '', 1, 'USA', '', 300, 1, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', 'sweatshirt_2020_12_15_12_15_49.jpg', NULL, '', 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png', NULL, NULL, NULL, NULL, NULL, 1, '2020-12-15 12:15:49'),
(42, 'walkaroo men casual sandals', 'walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions\n\n\n', '&lt;p&gt;walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions&lt;br /&gt;\n&amp;nbsp;&lt;/p&gt;\n', 1, 11, '', 15, 1, 30, '', 'All', 1, '', 1, 'USA', '', 449, 1, 0, 1, 5, 2, '2021-01-21', 1, 300, 499, 50, 70, 1, '0', 'walkaroo_men_casual_sandals_2021_01_12_16_46_03.jpg', NULL, '0', NULL, NULL, NULL, 'roolUp', 'casuals', 'colorBlocked', 1, '2021-01-12 16:46:03'),
(49, 'walkaroo men casual sandals', 'walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions\n\n\n', '&lt;p&gt;walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions&lt;br /&gt;\n&amp;nbsp;&lt;/p&gt;\n', 1, 11, '', 15, 1, 30, '', 'All', 1, '', 1, 'USA', '', 449, 1, 0, 1, 5, 2, '2021-01-21', 1, 300, 499, 50, 70, 1, '0', 'walkaroo_men_casual_sandals_2021_01_12_16_47_51.jpg', NULL, '0', NULL, NULL, NULL, 'roolUp', 'casuals', 'colorBlocked', 1, '2021-01-12 16:47:51'),
(50, 'walkaroo men casual sandals', 'walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions\n\n\n', '&lt;p&gt;walkaroo men casual sandals is crafted with premium quality mesh and phylon sole to give you matchless comfort. keep walking with ease you will feel impressions&lt;br /&gt;\n&amp;nbsp;&lt;/p&gt;\n', 1, 11, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', 'walkaroo_men_casual_sandals_2021_01_12_16_48_56.jpg', NULL, '0', NULL, NULL, NULL, 'roolUp', 'casuals', 'colorBlocked', 1, '2021-01-12 17:44:25'),
(62, 'pantaloons junior', 'boys  casual jeans featuring a buttoned waist, basic pockets and a regular fit . it is crafted from fine fabric', '&lt;p&gt;boys &amp;nbsp;casual jeans featuring a buttoned waist, basic pockets and a regular fit . it is crafted from fine fabric&lt;/p&gt;\n', 2, 12, '', 15, 1, 30, '', 'All', 1, '', 1, 'USA', '', 599, 1, 0, 1, 5, 2, '0000-00-00', 1, 300, 400, 50, 90, 0, '0', 'pantaloons_junior_2021_01_12_21_55_57.jpg', NULL, '', NULL, NULL, NULL, 'full', 'formals', 'colorBlocked', 1, '2021-01-13 22:23:46'),
(68, 'Boy\'s Cotton Hooded Hoodie', ' Instructions: Hand Wash Only\nFit Type: Regular\nFabric: 100% Cotton\nSleeve: Long Sleeve\nNeck Style: Hooded\nClosure: Zip\nPattern: Solid', '&lt;ul&gt;\n	&lt;li&gt;&amp;nbsp;Instructions: Hand Wash Only&lt;/li&gt;\n	&lt;li&gt;Fit Type: Regular&lt;/li&gt;\n	&lt;li&gt;Fabric: 100% Cotton&lt;/li&gt;\n	&lt;li&gt;Sleeve: Long Sleeve&lt;/li&gt;\n	&lt;li&gt;Neck Style: Hooded&lt;/li&gt;\n	&lt;li&gt;Closure: Zip&lt;/li&gt;\n	&lt;li&gt;Pattern: Solid&lt;/li&gt;\n&lt;/ul&gt;\n', 2, 16, '', 15, 1, 30, '', 'All', 1, '', 1, 'Australia', '', 499, 1, 0, 1, 2, 5, '0000-00-00', 1, 499, 400, 40, 80, 0, '0', 'boys_cotton_hooded_hoodie_2021_01_31_13_26_47.jpg', NULL, '', 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png', NULL, NULL, 'full', 'sweatshirts', 'printed', 1, '2021-01-31 13:26:47'),
(71, 'sweatshirt and Hoodies', 'boys cotton Hooded Hoodie', '&lt;p&gt;boys cotton Hooded Hoodie&lt;/p&gt;\n', 2, 17, '', 15, 1, 30, '', 'All', 1, '', 1, 'India', '', 399, 1, 0, 1, 2, 5, '0000-00-00', 1, 300, 400, 40, 80, 0, '0', 'sweatshirt_and_hoodies_2021_01_31_14_20_41.jpg', NULL, '', NULL, NULL, NULL, 'full', 'casuals', 'printed', 1, '2021-01-31 14:20:41'),
(72, 'sweatshirt and Hoodies', 'boys cotton Hooded Hoodie', '&lt;p&gt;boys cotton Hooded Hoodie&lt;/p&gt;\n', 2, 17, '', 15, 1, 30, '', 'All', 1, '', 1, 'India', '', 399, 1, 0, 1, 2, 5, '0000-00-00', 1, 300, 400, 40, 80, 0, '0', 'sweatshirt_and_hoodies_2021_01_31_14_22_04.jpg', NULL, '', NULL, NULL, NULL, 'full', 'casuals', 'printed', 1, '2021-01-31 14:22:04'),
(73, 'sweatshirt and Hoodies', 'boys cotton Hooded Hoodie', '&lt;p&gt;boys cotton Hooded Hoodie&lt;/p&gt;\n', 2, 17, '', 15, 1, 30, '', 'All', 1, '', 1, 'India', '', 399, 1, 0, 1, 2, 5, '0000-00-00', 1, 300, 400, 40, 80, 0, '0', 'sweatshirt_and_hoodies_2021_01_31_14_22_16.jpg', NULL, '', NULL, NULL, NULL, 'full', 'casuals', 'printed', 1, '2021-01-31 14:22:16'),
(74, 'sweatshirt and Hoodies', 'boys cotton Hooded Hoodie', '&lt;p&gt;boys cotton Hooded Hoodie&lt;/p&gt;\n', 2, 17, '', 15, 1, 30, '', 'All', 1, '', 1, 'Australia', '', 499, 0, 18, 0, 0, 0, '0000-00-00', 1, 200, 300, 20, 40, 0, '0', 'sweatshirt_and_hoodies_2021_01_31_14_24_58.jpg', NULL, '', 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png', NULL, NULL, 'full', 'casuals', 'printed', 1, '2021-01-31 14:24:58'),
(75, 'sweatshirt and Hoodies', 'boys cotton Hooded Hoodie\nCare Instructions: Hand Wash Only\n100% cotton\nHooded Sweatshirt\nFull sleeve\nMade up of high quality 190 gsm breathable fabrics.\nWill be same as shown in the image.', '&lt;p&gt;boys cotton Hooded Hoodie&lt;/p&gt;\n\n&lt;ul&gt;\n	&lt;li&gt;Care Instructions: Hand Wash Only&lt;/li&gt;\n	&lt;li&gt;100% cotton&lt;/li&gt;\n	&lt;li&gt;Hooded Sweatshirt&lt;/li&gt;\n	&lt;li&gt;Full sleeve&lt;/li&gt;\n	&lt;li&gt;Made up of high quality 190 gsm breathable fabrics.&lt;/li&gt;\n	&lt;li&gt;Will be same as shown in the image.&lt;/li&gt;\n&lt;/ul&gt;\n', 2, 17, '', 15, 1, 30, '', 'All', 1, '', 1, 'Australia', '', 499, 0, 18, 0, 0, 0, '0000-00-00', 1, 200, 300, 20, 40, 0, '0', 'sweatshirt_and_hoodies_2021_01_31_14_27_57.jpg', NULL, '', 'our_advantages_images_1.png,our_advantages_images_2.png,our_advantages_images_3.png', NULL, NULL, 'full', 'casuals', 'printed', 1, '2021-01-31 14:27:57'),
(85, 'Netplay', 'Short Description1', '&lt;p&gt;Long Description&lt;/p&gt;\n', 2, 0, '', 30, 1, 20, '', 'All', 1, '', 1, 'USA', '', 5000, 1, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 3, '0', 'netplay_2021_02_01_23_42_36.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 16:12:29'),
(89, 'shirts', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 2, 13, '', 15, 1, 30, '', 'All', 1, '', 1, 'USA', '', 499, 1, 0, 1, 2, 4, '0000-00-00', 1, 499, 499, 30, 60, 0, '0', 'shirts_2021_02_04_20_02_55.jpeg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-04 20:02:55'),
(105, 'Primeam quality Regularfit printed shirts* ?340.00/-', 'PRIMEAM QUALITY *PRINTED SHIRTS* \n\nFITTING: *Regular*\nSTYLE: *FULL SLEEVE*\nCOL???? *6* \nRATIO ???? *1111* \nSIZES ???? *M L XL* ', '&lt;p&gt;FABRIC- *100%* COTTON FABRIC WITH SILICON SOFTNER WASHES&lt;/p&gt;\n\n&lt;p&gt;PRICE????&lt;br /&gt;\nM size *340rs* per *PC&amp;nbsp;&lt;br /&gt;\nL size *350rs* per *PC&amp;nbsp;&lt;br /&gt;\nXL size *360rs* per *PC&amp;nbsp;&lt;/p&gt;\n\n&lt;p&gt;??Size measurements????&lt;br /&gt;\n&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;Chest &amp;nbsp; &amp;nbsp;legnth shoulder&lt;/p&gt;\n\n&lt;p&gt;M &amp;nbsp; &amp;nbsp; &amp;nbsp;20.5 &amp;nbsp; &amp;nbsp; 28. &amp;nbsp; &amp;nbsp; &amp;nbsp; 17.5&lt;br /&gt;\nL &amp;nbsp; &amp;nbsp; &amp;nbsp; 21.5 &amp;nbsp; &amp;nbsp; 29. &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;18&lt;br /&gt;\nXL &amp;nbsp; &amp;nbsp; 22.5 &amp;nbsp; &amp;nbsp; 30. &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;18.5&lt;/p&gt;\n', 1, 0, '', 5, 0, 0, 'As per Rules', 'All', 0, 'As per Rules', 0, '', 'As per Rules', 340, 1, 0, 0, 0, 0, '0000-00-00', 1, 50, 50, 50, 49, 0, '0', 'primeam_quality_regularfit_printed_shirts*_?340.00_2021_02_08_22_22_27', NULL, '', NULL, NULL, 'plain shirts', 'full', 'casuals', 'colorBlocked', 1, '2021-02-09 16:11:43'),
(106, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-08 22:35:55'),
(107, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-08 22:36:53'),
(108, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 1, 18, '', 30, 1, 15, '', 'All', 1, '', 1, 'India', '', 599, 1, 0, 1, 3, 5, '0000-00-00', 1, 300, 400, 20, 40, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_09_16_39_38.jpg', NULL, '', NULL, NULL, NULL, 'half', 'sports', 'printed', 1, '2021-02-09 16:39:38'),
(109, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 1, 18, '', 30, 1, 15, '', 'All', 1, '', 1, 'India', '', 599, 1, 0, 1, 3, 5, '0000-00-00', 1, 300, 400, 20, 40, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_09_16_33_14.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 16:33:14'),
(110, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description 123', '&lt;p&gt;long Description 123&lt;/p&gt;\n', 1, 0, '', 30, 1, 15, '', 'All', 1, '', 1, 'India', '', 599, 1, 0, 1, 3, 5, '0000-00-00', 1, 300, 400, 20, 40, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_08_23_28_36.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 16:10:07'),
(111, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description ', '&lt;p&gt;long Description&amp;nbsp;&lt;/p&gt;\n', 1, 0, '', 30, 1, 15, '', 'All', 1, '', 1, 'India', '', 599, 1, 0, 1, 3, 5, '0000-00-00', 1, 300, 400, 20, 40, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_08_23_33_58.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 16:09:47'),
(112, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:30:32'),
(113, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:31:10'),
(114, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:31:14'),
(115, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:32:53'),
(116, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:34:54'),
(117, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 1, 0, 0, '0', '0', 0, '0', 0, '', '0', 0, 1, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_09_20_40_26.jpg', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:40:26'),
(118, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 0, 0, 0, '0', '0', 0, '0', 0, '0', '0', 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, 0, 0, 0, 0, '0', '', NULL, '0', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:44:51'),
(119, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 30, 1, 15, '', 'All', 1, '', 1, 'USA', '', 599, 1, 0, 1, 2, 4, '0000-00-00', 1, 300, 350, 30, 60, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_09_20_44_53.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-09 20:44:53'),
(122, 'AMERICAN CREW Men\'s Regular Fit Polos', 'Short Description', '&lt;p&gt;long Description&lt;/p&gt;\n', 1, 18, '', 30, 1, 15, '', 'All', 1, '', 1, 'India', '', 600, 1, 0, 1, 1, 2, '0000-00-00', 1, 400, 450, 20, 40, 0, '0', 'american_crew_mens_regular_fit_polos_2021_02_11_12_58_12.jpg', NULL, '', NULL, NULL, NULL, 'half', 'casuals', 'printed', 1, '2021-02-11 12:58:12'),
(125, 'Boys\' Chest Printed Hooded Sweatshirt', 'Care Instructions: Machine Wash\nFit Type: Regular\nColor Name: Multicolor\n100% Cotton\nLong sleeve\nMachine wash\n', '&lt;ul&gt;\n	&lt;li&gt;Care Instructions: Machine Wash&lt;/li&gt;\n	&lt;li&gt;Fit Type: Regular&lt;/li&gt;\n	&lt;li&gt;Color Name: Multicolor&lt;/li&gt;\n	&lt;li&gt;100% Cotton&lt;/li&gt;\n	&lt;li&gt;Long sleeve&lt;/li&gt;\n	&lt;li&gt;Machine wash&lt;/li&gt;\n&lt;/ul&gt;\n\n&lt;hr /&gt;', 1, 19, '', 30, 1, 15, '', 'All', 1, '', 1, 'Australia', '', 300, 1, 0, 1, 4, 4, '0000-00-00', 1, 300, 300, 20, 40, 0, '0', '', NULL, '', NULL, NULL, NULL, 'full', 'sweatshirts', 'printed', 1, '2021-02-11 22:14:59'),
(131, 'jeans', 'boys jeans', '&lt;p&gt;boys party wear , regular jeans&lt;/p&gt;\n', 2, 12, '', 10, 1, 7, '', '0', 0, '0', 0, '0', '0', 550, 1, 0, 1, 10, 10, '0000-00-00', 1, 0, 0, 100, 0, 0, '0', 'pyjamas_2021_02_17_13_07_04.jpg', NULL, '', NULL, NULL, NULL, 'roolUp', 'ethenics', 'printed', 1, '2021-02-17 14:01:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_attributes`
--

CREATE TABLE `tbl_product_attributes` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(10) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_stock`
--

CREATE TABLE `tbl_product_stock` (
  `id` int(11) NOT NULL,
  `product_id` int(10) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `color_name` varchar(255) NOT NULL,
  `size` varchar(255) DEFAULT NULL,
  `clothtype` varchar(255) DEFAULT NULL,
  `images` text,
  `quantity` int(255) NOT NULL,
  `price` int(255) NOT NULL,
  `sold_out` int(255) NOT NULL DEFAULT '0',
  `in_stock` int(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product_stock`
--

INSERT INTO `tbl_product_stock` (`id`, `product_id`, `color`, `color_name`, `size`, `clothtype`, `images`, `quantity`, `price`, `sold_out`, `in_stock`, `created_on`) VALUES
(53, 35, 'reymond_shirt_color_0_2020_12_13_15_55_23.jpg', 'red', 'S', NULL, 'reymond_shirt_images_1_2020_12_13_15_55_23.jpg,reymond_shirt_images_2_2020_12_13_15_55_23.jpg', 18, 500, 0, 0, '2020-12-13 10:25:23'),
(54, 35, 'reymond_shirt_color_1_2020_12_13_15_55_23.jpg', 'red', 'M', NULL, 'reymond_shirt_images_1_2020_12_13_15_55_231.jpg,reymond_shirt_images_2_2020_12_13_15_55_231.jpg', 20, 555, 0, 0, '2020-12-13 10:25:23'),
(55, 35, 'reymond_shirt_color_2_2020_12_13_15_55_23.jpg', 'blue', 'XL', NULL, 'reymond_shirt_images_1_2020_12_13_15_55_232.jpg,reymond_shirt_images_2_2020_12_13_15_55_232.jpg', 25, 765, 0, 0, '2020-12-13 10:25:23'),
(56, 36, 'salwar_suit_color_0_2020_12_14_14_19_13.jpg', 'Blue', 'S', NULL, 'salwar_suit_images_1_2020_12_14_14_19_13.jpg,salwar_suit_images_2_2020_12_14_14_19_13.jpg,salwar_suit_images_3_2020_12_14_14_19_13.jpg,salwar_suit_images_4_2020_12_14_14_19_13.jpg', 20, 200, 0, 0, '2020-12-14 08:49:13'),
(57, 36, 'salwar_suit_color_1_2020_12_14_14_19_13.jpg', 'Green', 'L', NULL, 'salwar_suit_images_1_2020_12_14_14_19_131.jpg,salwar_suit_images_2_2020_12_14_14_19_131.jpg,salwar_suit_images_3_2020_12_14_14_19_131.jpg,salwar_suit_images_4_2020_12_14_14_19_131.jpg', 20, 21, 0, 0, '2020-12-14 08:49:13'),
(58, 37, 'sweatshirt_color_0_2020_12_15_12_15_49.jpg', 'Red', 'S', NULL, 'sweatshirt_images_1_2020_12_15_12_15_49.jpg,sweatshirt_images_2_2020_12_15_12_15_49.jpg,sweatshirt_images_3_2020_12_15_12_15_49.jpg', 10, 100, 0, 0, '2020-12-15 06:45:49'),
(59, 37, 'sweatshirt_color_1_2020_12_15_12_15_49.jpg', 'Red', 'M', NULL, 'sweatshirt_images_1_2020_12_15_12_15_491.jpg,sweatshirt_images_2_2020_12_15_12_15_491.jpg,sweatshirt_images_3_2020_12_15_12_15_491.jpg', 20, 300, 0, 0, '2020-12-15 06:45:49'),
(103, 42, 'walkaroo_men_casual_sandals_color_0_2021_01_12_16_46_03.jpg', 'black', 'S', NULL, 'walkaroo_men_casual_sandals_images_1_2021_01_12_16_46_03.jpg,walkaroo_men_casual_sandals_images_2_2021_01_12_16_46_03.jpg,walkaroo_men_casual_sandals_images_3_2021_01_12_16_46_03.jpg,walkaroo_men_casual_sandals_images_4_2021_01_12_16_46_03.jpg,walkaroo_men_casual_sandals_images_5_2021_01_12_16_46_03.jpg', 2, 449, 0, 0, '2021-01-12 11:16:03'),
(108, 49, 'walkaroo_men_casual_sandals_color_0_2021_01_12_16_47_51.jpg', 'black', 'S', NULL, 'walkaroo_men_casual_sandals_images_1_2021_01_12_16_47_51.jpg,walkaroo_men_casual_sandals_images_2_2021_01_12_16_47_51.jpg,walkaroo_men_casual_sandals_images_3_2021_01_12_16_47_51.jpg,walkaroo_men_casual_sandals_images_4_2021_01_12_16_47_51.jpg,walkaroo_men_casual_sandals_images_5_2021_01_12_16_47_51.jpg', 2, 449, 0, 0, '2021-01-12 11:17:51'),
(117, 50, NULL, 'black', 'S', NULL, NULL, 2, 449, 0, 0, '2021-01-12 12:14:25'),
(118, 50, 'walkaroo_men_casual_sandals_color_1_2021_01_12_17_44_25.jpg', 'tan', 'M', NULL, NULL, 2, 549, 0, 0, '2021-01-12 12:14:25'),
(177, 62, 'pantaloons_junior_color_0_2021_01_13_22_23_46.jpg', 'dark blue', 'S', NULL, 'pantaloons_junior_images_1_2021_01_13_22_23_46.jpg,pantaloons_junior_images_2_2021_01_13_22_23_46.jpg,pantaloons_junior_images_3_2021_01_13_22_23_46.jpg,pantaloons_junior_images_4_2021_01_13_22_23_46.jpg,pantaloons_junior_images_5_2021_01_13_22_23_46.jpg,pantaloons_junior_images_6_2021_01_13_22_23_46.jpg,pantaloons_junior_images_7_2021_01_13_22_23_46.jpg', 1, 526, 0, 0, '2021-01-13 16:53:46'),
(178, 62, 'pantaloons_junior_color_1_2021_01_13_22_23_46.png', 'black', 'M', NULL, 'pantaloons_junior_images_1_2021_01_13_22_23_461.jpg,pantaloons_junior_images_2_2021_01_13_22_23_461.jpg,pantaloons_junior_images_3_2021_01_13_22_23_461.jpg,pantaloons_junior_images_4_2021_01_13_22_23_461.jpg,pantaloons_junior_images_5_2021_01_13_22_23_461.jpg,pantaloons_junior_images_6_2021_01_13_22_23_461.jpg,pantaloons_junior_images_7_2021_01_13_22_23_461.jpg', 1, 600, 0, 0, '2021-01-13 16:53:46'),
(179, 62, 'pantaloons_junior_color_2_2021_01_13_22_23_46.jpg', 'light blue', 'L', NULL, 'pantaloons_junior_images_1_2021_01_13_22_23_462.jpg,pantaloons_junior_images_2_2021_01_13_22_23_462.jpg,pantaloons_junior_images_3_2021_01_13_22_23_462.jpg,pantaloons_junior_images_4_2021_01_13_22_23_462.jpg,pantaloons_junior_images_5_2021_01_13_22_23_462.jpg,pantaloons_junior_images_6_2021_01_13_22_23_462.jpg,pantaloons_junior_images_7_2021_01_13_22_23_462.jpg', 1, 400, 0, 0, '2021-01-13 16:53:46'),
(180, 62, 'pantaloons_junior_color_3_2021_01_13_22_23_46.png', 'grey', 'XL', NULL, 'pantaloons_junior_images_1_2021_01_13_22_23_463.jpg,pantaloons_junior_images_2_2021_01_13_22_23_463.jpg,pantaloons_junior_images_3_2021_01_13_22_23_463.jpg,pantaloons_junior_images_4_2021_01_13_22_23_463.jpg,pantaloons_junior_images_5_2021_01_13_22_23_463.jpg,pantaloons_junior_images_6_2021_01_13_22_23_463.jpg,pantaloons_junior_images_7_2021_01_13_22_23_463.jpg', 1, 800, 0, 0, '2021-01-13 16:53:46'),
(191, 68, 'boys_cotton_hooded_hoodie_color_0_2021_01_31_13_26_47.png', 'pink', 'S', NULL, NULL, 10, 600, 0, 0, '2021-01-31 07:56:47'),
(196, 71, 'sweatshirt_and_hoodies_color_0_2021_01_31_14_20_41.png', 'dusty pink', 'S', NULL, NULL, 1, 499, 0, 0, '2021-01-31 08:50:41'),
(197, 72, 'sweatshirt_and_hoodies_color_0_2021_01_31_14_22_04.png', 'dusty pink', 'S', NULL, NULL, 2, 499, 0, 0, '2021-01-31 08:52:04'),
(198, 73, 'sweatshirt_and_hoodies_color_0_2021_01_31_14_22_16.png', 'dusty pink', 'S', NULL, NULL, 2, 499, 0, 0, '2021-01-31 08:52:16'),
(199, 74, 'sweatshirt_and_hoodies_color_0_2021_01_31_14_24_58.png', 'dusty pink', 'S', NULL, NULL, 1, 499, 0, 0, '2021-01-31 08:54:58'),
(200, 75, 'sweatshirt_and_hoodies_color_0_2021_01_31_14_27_57.png', 'dusty pink', 'S', NULL, NULL, 1, 499, 0, 0, '2021-01-31 08:57:57'),
(213, 85, 'netplay_color_0_2021_02_01_23_42_36.jpg', 'Red', 'S', NULL, 'netplay_images_1_2021_02_01_23_42_36.jpg,netplay_images_2_2021_02_01_23_42_36.jpg,netplay_images_3_2021_02_01_23_42_36.jpg', 30, 300, 0, 0, '2021-02-01 18:12:36'),
(214, 85, 'netplay_color_1_2021_02_01_23_42_36.jpg', 'Red', 'M', NULL, 'netplay_images_1_2021_02_01_23_42_361.jpg,netplay_images_2_2021_02_01_23_42_361.jpg,netplay_images_3_2021_02_01_23_42_361.jpg', 50, 400, 0, 0, '2021-02-01 18:12:36'),
(222, 89, 'shirts_color_0_2021_02_04_20_02_56.jpg', 'blue', 'S', NULL, 'shirts_images_1_2021_02_04_20_02_55.jpg,shirts_images_2_2021_02_04_20_02_56.jpg', 1, 599, 0, 0, '2021-02-04 14:32:56'),
(223, 89, 'shirts_color_1_2021_02_04_20_02_56.jpg', 'black', 'M', NULL, 'color_images_1_2021_02_04_20_29_30.jpg', 1, 599, 0, 0, '2021-02-04 14:32:56'),
(260, 105, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_0_2021_02_08_22_22_27', 'Orange', 'M', NULL, 'primeam_quality_regularfit_printed_shirts*_?340.00_images_1_2021_02_08_22_22_27', 1, 340, 0, 0, '2021-02-08 16:52:27'),
(261, 105, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_1_2021_02_08_22_22_27', 'Orange', 'L', NULL, 'primeam_quality_regularfit_printed_shirts*_?3401.00_images_1_2021_02_08_22_22_27', 1, 350, 0, 0, '2021-02-08 16:52:27'),
(262, 105, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_2_2021_02_08_22_22_27', 'Orange', 'XL', NULL, 'primeam_quality_regularfit_printed_shirts*_?3402.00_images_1_2021_02_08_22_22_27', 1, 360, 0, 0, '2021-02-08 16:52:27'),
(263, 105, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_3_2021_02_08_22_22_27', 'Green', 'M', NULL, 'primeam_quality_regularfit_printed_shirts*_?3403.00_images_1_2021_02_08_22_22_27', 2, 340, 0, 0, '2021-02-08 16:52:27'),
(264, 105, 'primeam_quality_regularfit_printed_shirts*_?340.00_color_4_2021_02_08_22_22_27', 'Black', 'XL', NULL, 'primeam_quality_regularfit_printed_shirts*_?3404.00_images_1_2021_02_08_22_22_27', 1, 360, 0, 0, '2021-02-08 16:52:27'),
(265, 106, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_22_35_55.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_35_55.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_35_55.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_35_55.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_35_55.jpg', 1, 599, 0, 0, '2021-02-08 17:05:55'),
(266, 106, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_22_35_55.jpg', 'Grey Melange & Anthra Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_35_551.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_35_551.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_35_551.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_35_551.jpg', 66, 599, 0, 0, '2021-02-08 17:05:55'),
(267, 107, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_22_36_53.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_36_53.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_36_53.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_36_53.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_36_53.jpg', 1, 599, 0, 0, '2021-02-08 17:06:53'),
(268, 107, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_22_36_53.jpg', 'Grey Melange & Anthra Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_36_531.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_36_531.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_36_531.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_36_531.jpg', 1, 599, 0, 0, '2021-02-08 17:06:53'),
(269, 107, 'american_crew_mens_regular_fit_polos_color_2_2021_02_08_22_36_53.jpg', 'Grey Melange & Anthra Melange', 'L', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_36_532.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_36_532.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_36_532.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_36_532.jpg', 1, 599, 0, 0, '2021-02-08 17:06:53'),
(270, 108, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_22_38_39.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_38_39.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_38_39.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_38_39.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_38_39.jpg', 1, 599, 0, 0, '2021-02-08 17:08:39'),
(271, 108, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_22_38_39.jpg', 'Grey Melange & Anthra Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_38_391.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_38_391.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_38_391.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_38_391.jpg', 1, 599, 0, 0, '2021-02-08 17:08:39'),
(272, 108, 'american_crew_mens_regular_fit_polos_color_2_2021_02_08_22_38_39.jpg', 'Grey Melange & Anthra Melange', 'L', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_38_392.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_38_392.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_38_392.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_38_392.jpg', 1, 599, 0, 0, '2021-02-08 17:08:39'),
(273, 108, 'american_crew_mens_regular_fit_polos_color_3_2021_02_08_22_38_39.jpg', 'Grey Melange & Anthra Melange', 'XL', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_22_38_393.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_22_38_393.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_22_38_393.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_22_38_393.jpg', 1, 599, 0, 0, '2021-02-08 17:08:39'),
(274, 108, NULL, 'Grey Melange & Anthra Melange', 'XXL', NULL, NULL, 1, 599, 0, 0, '2021-02-08 17:08:39'),
(275, 109, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_23_16_52.jpg', 'black', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_16_52.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_16_52.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_16_52.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_16_52.jpg', 1, 599, 0, 0, '2021-02-08 17:46:52'),
(276, 109, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_23_16_52.jpg', 'Royal Blue & Navy Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_16_521.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_08_23_16_52.jpg', 1, 599, 0, 0, '2021-02-08 17:46:52'),
(277, 109, 'american_crew_mens_regular_fit_polos_color_2_2021_02_08_23_16_52.jpg', 'Black & Charcoal Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_16_522.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_16_522.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_16_522.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_16_522.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_08_23_16_521.jpg', 1, 599, 0, 0, '2021-02-08 17:46:52'),
(278, 109, 'american_crew_mens_regular_fit_polos_color_3_2021_02_08_23_16_52.jpg', 'light green', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_16_523.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_16_523.jpg', 1, 599, 0, 0, '2021-02-08 17:46:52'),
(279, 110, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_23_28_36.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_28_36.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_28_36.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_28_36.jpg', 1, 599, 0, 0, '2021-02-08 17:58:36'),
(280, 110, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_23_28_36.jpg', 'Black & Charcoal Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_28_361.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_28_361.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_28_361.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_28_36.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_08_23_28_36.jpg', 1, 599, 0, 0, '2021-02-08 17:58:36'),
(281, 110, 'american_crew_mens_regular_fit_polos_color_2_2021_02_08_23_28_36.jpg', 'light green', 'L', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_28_362.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_28_362.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_28_362.jpg', 1, 599, 0, 0, '2021-02-08 17:58:36'),
(282, 110, 'american_crew_mens_regular_fit_polos_color_3_2021_02_08_23_28_36.jpg', 'blue', 'XL', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_28_363.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_28_363.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_28_363.jpg', 1, 599, 0, 0, '2021-02-08 17:58:36'),
(283, 111, 'american_crew_mens_regular_fit_polos_color_0_2021_02_08_23_33_58.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_33_58.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_33_58.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_33_58.jpg', 1, 599, 0, 0, '2021-02-08 18:03:58'),
(284, 111, 'american_crew_mens_regular_fit_polos_color_1_2021_02_08_23_33_58.jpg', 'Grey Melange & Anthra Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_08_23_33_581.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_08_23_33_581.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_08_23_33_581.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_08_23_33_58.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_08_23_33_58.jpg,american_crew_mens_regular_fit_polos_images_6_2021_02_08_23_33_58.jpg', 1, 599, 0, 0, '2021-02-08 18:03:58'),
(285, 119, 'american_crew_mens_regular_fit_polos_color_0_2021_02_09_20_44_53.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_09_20_44_53.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_09_20_44_53.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_09_20_44_53.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_09_20_44_53.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_09_20_44_53.jpg', 1, 599, 0, 0, '2021-02-09 15:14:53'),
(286, 119, 'american_crew_mens_regular_fit_polos_color_1_2021_02_09_20_44_53.jpg', 'Grey Melange & Anthra Melange', 'L', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_09_20_44_531.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_09_20_44_531.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_09_20_44_531.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_09_20_44_531.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_09_20_44_531.jpg', 1, 599, 0, 0, '2021-02-09 15:14:53'),
(287, 119, 'american_crew_mens_regular_fit_polos_color_2_2021_02_09_20_44_53.jpg', 'Grey Melange & Anthra Melange', 'XXL', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_09_20_44_532.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_09_20_44_532.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_09_20_44_532.jpg,american_crew_mens_regular_fit_polos_images_4_2021_02_09_20_44_532.jpg,american_crew_mens_regular_fit_polos_images_5_2021_02_09_20_44_532.jpg', 1, 599, 0, 0, '2021-02-09 15:14:53'),
(294, 122, 'american_crew_mens_regular_fit_polos_color_0_2021_02_10_08_30_22.jpg', 'Grey Melange & Anthra Melange', 'S', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_10_08_30_22.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_10_08_30_22.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_10_08_30_22.jpg', 1, 600, 0, 0, '2021-02-10 03:00:22'),
(295, 122, 'american_crew_mens_regular_fit_polos_color_1_2021_02_10_08_30_22.jpg', 'Grey Melange & Anthra Melange', 'M', NULL, 'american_crew_mens_regular_fit_polos_images_1_2021_02_10_08_30_221.jpg,american_crew_mens_regular_fit_polos_images_2_2021_02_10_08_30_221.jpg,american_crew_mens_regular_fit_polos_images_3_2021_02_10_08_30_221.jpg', 1, 600, 0, 0, '2021-02-10 03:00:22'),
(296, 125, 'boys_chest_printed_hooded_sweatshirt_color_0_2021_02_11_13_17_51.jpg', 'Grey', 'S', NULL, 'boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_51.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_51.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_51.jpg', 1, 300, 0, 0, '2021-02-11 07:47:51'),
(297, 125, 'boys_chest_printed_hooded_sweatshirt_color_1_2021_02_11_13_17_51.jpg', 'grey', 'M', NULL, 'boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_511.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_511.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_511.jpg', 1, 300, 0, 0, '2021-02-11 07:47:52'),
(298, 125, 'boys_chest_printed_hooded_sweatshirt_color_2_2021_02_11_13_17_52.jpg', 'grey', 'L', NULL, 'boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_512.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_512.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_512.jpg', 1, 300, 0, 0, '2021-02-11 07:47:52'),
(299, 125, 'boys_chest_printed_hooded_sweatshirt_color_3_2021_02_11_13_17_52.jpg', 'grey', 'XL', NULL, 'boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_513.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_513.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_513.jpg', 1, 300, 0, 0, '2021-02-11 07:47:52'),
(300, 125, 'boys_chest_printed_hooded_sweatshirt_color_4_2021_02_11_13_17_52.jpg', 'grey', 'XXL', NULL, 'boys_chest_printed_hooded_sweatshirt_images_1_2021_02_11_13_17_514.jpg,boys_chest_printed_hooded_sweatshirt_images_2_2021_02_11_13_17_514.jpg,boys_chest_printed_hooded_sweatshirt_images_3_2021_02_11_13_17_514.jpg', 1, 300, 0, 0, '2021-02-11 07:47:52'),
(323, 131, 'pyjamas_color_0_2021_02_17_13_07_04.jpg', 'blue', 'S', NULL, 'pyjamas_images_1_2021_02_17_13_07_04.jpg,pyjamas_images_2_2021_02_17_13_07_04.jpg', 1, 500, 0, 0, '2021-02-17 07:37:04'),
(324, 131, 'pyjamas_color_1_2021_02_17_13_07_04.jpg', 'blue', 'M', NULL, 'pyjamas_images_1_2021_02_17_13_07_041.jpg,pyjamas_images_2_2021_02_17_13_07_041.jpg', 2, 500, 0, 0, '2021-02-17 07:37:04'),
(325, 131, 'pyjamas_color_2_2021_02_17_13_07_04.jpg', 'blue', 'L', NULL, 'pyjamas_images_1_2021_02_17_13_07_042.jpg,pyjamas_images_2_2021_02_17_13_07_042.jpg', 3, 500, 0, 0, '2021-02-17 07:37:04'),
(326, 131, 'pyjamas_color_3_2021_02_17_13_07_04.jpg', 'black', 'S', NULL, 'pyjamas_images_1_2021_02_17_13_07_043.jpg,pyjamas_images_2_2021_02_17_13_07_043.jpg', 1, 500, 0, 0, '2021-02-17 07:37:04'),
(327, 131, 'pyjamas_color_4_2021_02_17_13_07_04.jpg', 'black', 'M', NULL, 'pyjamas_images_1_2021_02_17_13_07_044.jpg,pyjamas_images_2_2021_02_17_13_07_044.jpg', 2, 500, 0, 0, '2021-02-17 07:37:04'),
(328, 131, 'pyjamas_color_5_2021_02_17_13_07_04.jpg', 'black', 'L', NULL, 'pyjamas_images_1_2021_02_17_13_07_045.jpg,pyjamas_images_2_2021_02_17_13_07_045.jpg', 1, 500, 0, 0, '2021-02-17 07:37:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_recently_viewed`
--

CREATE TABLE `tbl_recently_viewed` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_recently_viewed`
--

INSERT INTO `tbl_recently_viewed` (`id`, `user_id`, `product_id`, `created_at`) VALUES
(1, 4, '140,128,128,121,121,61,36,35,108,82,86,61,63,42,62,61,61,62,61,62,61,38,38,38,38,38,35,37,36,35,36,36,37,36,37,36,36,35,37,37,37,37,36,37,37,35,35,35,35,35,35,35,35,35,35', '2021-03-03 11:46:16'),
(2, 2, '142,37,128,37,37', '2021-03-03 13:28:12'),
(3, 5, '142,142,142,141,134,105,134,105,129,129,125,105,105,105,63,63,63,63,63', '2021-02-26 13:33:43'),
(4, 6, '122,122,62,62,62,62,62,62,62', '2021-02-10 14:34:26'),
(5, 7, '131,130,132,131,131,131,89,131,132,131,131,130', '2021-02-17 19:18:18'),
(6, 8, '142,142,35,142,142', '2021-03-16 16:07:09'),
(7, 9, '142', '2021-03-18 08:21:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_scrolling_message`
--

CREATE TABLE `tbl_scrolling_message` (
  `id` int(11) NOT NULL,
  `scrolling_message` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_scrolling_message`
--

INSERT INTO `tbl_scrolling_message` (`id`, `scrolling_message`, `created_at`) VALUES
(1, 'BSG Garments Festival Offer Buy One Get One Branded Jeans - Hurry Up!', '2020-11-27 13:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipments`
--

CREATE TABLE `tbl_shipments` (
  `id` int(11) NOT NULL,
  `order_id` int(255) NOT NULL,
  `order_product_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `seller_id` int(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `tracking_id` varchar(255) NOT NULL,
  `delivery_partner` varchar(255) NOT NULL,
  `shipment_status` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_shipments`
--

INSERT INTO `tbl_shipments` (`id`, `order_id`, `order_product_id`, `product_id`, `seller_id`, `product_name`, `product_image`, `tracking_id`, `delivery_partner`, `shipment_status`, `created_on`) VALUES
(1, 31, 36, 363, 0, 'Plain Shirt', 'plain_shirt_2021_02_26_13_29_07.jpg', '14458875212', 'Bsg Delivery', '6', '2021-02-26 08:15:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tags`
--

CREATE TABLE `tbl_tags` (
  `id` int(11) NOT NULL,
  `tags` text,
  `main_category_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tags`
--

INSERT INTO `tbl_tags` (`id`, `tags`, `main_category_id`, `category_id`, `created_at`) VALUES
(1, 'printed shirts,plain shirts,checks shirts', 1, 3, '2021-01-12 14:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_taxes`
--

CREATE TABLE `tbl_taxes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tbl_taxes`
--

INSERT INTO `tbl_taxes` (`id`, `name`, `percentage`, `created_on`) VALUES
(1, 'GST', '18', '2020-10-19 12:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions`
--

CREATE TABLE `tbl_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `order_id` int(255) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_order_id` varchar(255) NOT NULL,
  `transaction_info` text NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transactions`
--

INSERT INTO `tbl_transactions` (`id`, `user_id`, `order_id`, `payment_id`, `payment_order_id`, `transaction_info`, `created_on`) VALUES
(1, 4, 1, 'pay_GDDsILRPze9raB', 'order_GDDru5Dy12hPpm', '{\"id\":\"pay_GDDsILRPze9raB\",\"entity\":\"payment\",\"amount\":30000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GDDru5Dy12hPpm\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"AIRP\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":655,\"tax\":100,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1608015071}', '2020-12-15 06:51:15'),
(2, 4, 2, 'pay_GDN0Ei4KcQLpN6', 'order_GDN06OUStNwEBi', '{\"id\":\"pay_GDN0Ei4KcQLpN6\",\"entity\":\"payment\",\"amount\":110000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GDN06OUStNwEBi\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":2401,\"tax\":366,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1608047216}', '2020-12-15 15:47:01'),
(3, 4, 5, 'pay_GG8RG7KOLO3OCx', 'order_GG8R8nFSs7PPwp', '{\"id\":\"pay_GG8RG7KOLO3OCx\",\"entity\":\"payment\",\"amount\":211000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GG8R8nFSs7PPwp\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"HDFC\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":4606,\"tax\":702,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1608650947}', '2020-12-22 15:29:12'),
(4, 4, 6, 'pay_GOSeOg61guzgHD', 'order_GOSe5ZWpxgVEXl', '{\"id\":\"pay_GOSeOg61guzgHD\",\"entity\":\"payment\",\"amount\":60000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GOSe5ZWpxgVEXl\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"HDFC\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":1310,\"tax\":200,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1610468847}', '2021-01-12 16:27:32'),
(5, 6, 10, 'pay_GP7QPdXVtxZib2', 'order_GP7N5QVNpBX8YC', '{\"id\":\"pay_GP7QPdXVtxZib2\",\"entity\":\"payment\",\"amount\":58800,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GP7N5QVNpBX8YC\",\"invoice_id\":null,\"international\":false,\"method\":\"card\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":\"card_GP7QPmFFKuMwg8\",\"bank\":null,\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":1088,\"tax\":0,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1610612439}', '2021-01-14 08:20:51'),
(6, 6, 11, 'pay_GP7Tz04v52HpWx', 'order_GP7Rtdfh6b0ujf', '{\"id\":\"pay_GP7Tz04v52HpWx\",\"entity\":\"payment\",\"amount\":58800,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GP7Rtdfh6b0ujf\",\"invoice_id\":null,\"international\":false,\"method\":\"upi\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":null,\"wallet\":null,\"vpa\":\"anusha@kotak\",\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":1284,\"tax\":196,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1610612641}', '2021-01-14 08:24:08'),
(7, 6, 12, 'pay_GP7VyrtR63PxSV', 'order_GP7VX6ZIiX7J6A', '{\"id\":\"pay_GP7VyrtR63PxSV\",\"entity\":\"payment\",\"amount\":58800,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GP7VX6ZIiX7J6A\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"ICIC\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":1284,\"tax\":196,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1610612755}', '2021-01-14 08:26:03'),
(8, 6, 13, 'pay_GP7Xiagv19jngO', 'order_GP7XOEqXOlrmzE', '{\"id\":\"pay_GP7Xiagv19jngO\",\"entity\":\"payment\",\"amount\":58800,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GP7XOEqXOlrmzE\",\"invoice_id\":null,\"international\":false,\"method\":\"wallet\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":null,\"wallet\":\"airtelmoney\",\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":1284,\"tax\":196,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1610612854}', '2021-01-14 08:27:39'),
(9, 5, 31, 'pay_Gg8BDyHN0k2PNi', 'order_Gg87lGC4jJs1MZ', '{\"id\":\"pay_Gg8BDyHN0k2PNi\",\"entity\":\"payment\",\"amount\":2000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_Gg87lGC4jJs1MZ\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"HDFC\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":48,\"tax\":8,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1614326880}', '2021-02-26 08:08:55'),
(10, 8, 32, 'pay_GnEq3dyiW6fjtS', 'order_GnEpmoSV8eKXHM', '{\"id\":\"pay_GnEq3dyiW6fjtS\",\"entity\":\"payment\",\"amount\":4000,\"currency\":\"INR\",\"status\":\"captured\",\"order_id\":\"order_GnEpmoSV8eKXHM\",\"invoice_id\":null,\"international\":false,\"method\":\"netbanking\",\"amount_refunded\":0,\"refund_status\":null,\"captured\":true,\"description\":\"BSG Garments\",\"card_id\":null,\"bank\":\"SBIN\",\"wallet\":null,\"vpa\":null,\"email\":\"bsggarments@gmail.com\",\"contact\":\"+919999999999\",\"notes\":{},\"fee\":94,\"tax\":14,\"error_code\":null,\"error_description\":null,\"error_source\":null,\"error_step\":null,\"error_reason\":null,\"acquirer_data\":{},\"created_at\":1615878709}', '2021-03-16 07:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `mobile_number`, `password`, `role`, `status`, `created_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '9999999999', '$2y$10$.NfrKZhd/iGXmB2R51MiCOsEZkH/YJZVJ3p1HG6uqJ5Ela0V5aCye', 'Admin', 1, '2020-10-19 12:35:05'),
(2, 'Sid', 'user@gmail.com', '9848012345', '$2y$10$Yr7xsqjkXUeiugv2oj9dEujsOLj5xXP5znL02ZQjPmgWt1LBWHBUK', 'User', 1, '2020-10-20 20:38:58'),
(3, 'Arun', 'arunadfstuff1@gmail.com', '9052304477', '$2y$10$uj3uIYv5FhQ.1MKbf1jWMOzzazPOKG2cYOEoGzPn4aga4s1BVTgVK', 'User', 1, '2020-10-23 13:45:40'),
(4, 'Arun Eppanapelly', 'arun.e@primelogictech.com', '8639930690', '$2y$10$.NfrKZhd/iGXmB2R51MiCOsEZkH/YJZVJ3p1HG6uqJ5Ela0V5aCye', 'User', 1, '2020-11-05 19:56:55'),
(5, 'Ramu', 'ramugoud9493@gmail.com', '7801019797', '$2y$10$VeAOPdn1Y7.rj3mL4V81f.jALnI6LoiVhUhiIUZLSXUKGAJjxjuxe', 'User', 1, '2020-11-10 21:39:41'),
(6, 'anusha', 'anusha.eppanapalli@gmail.com', '9177852662', '$2y$10$.dLzm30N4E5FTZozNt6/YuICf/9YHMCSwLzhMCIKvFODJ9OcumNSS', 'User', 1, '2021-01-12 15:36:54'),
(7, 'sravani', 'sravani.mvsn@gmail.com', '8527193357', '$2y$10$Dm/L15xFxGylK/8pPUwpwOQShxrT3dGi2N7Y1mTjOY9Mu20x9hU8e', 'User', 1, '2021-02-17 12:30:13'),
(8, 'KR NARESH', 'naresh.kr0605@gmail.com', '7989328565', '$2y$10$xVn.SZ6s3OxpcKqlxLm8OeKACmOuKo0dHsCvXhHweAfPkbEy10lRi', 'User', 1, '2021-03-16 12:38:23'),
(9, 'Swaraj', 'nesayswarajkumar95@gmail.com', '9493220266', '$2y$10$idy5xoNS7CKp30ElqT8rTOiLxPCbMkwseiovR0fpwFiYvaenXaU6e', 'User', 1, '2021-03-18 08:20:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_addresses`
--

CREATE TABLE `tbl_user_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `primary_address` varchar(10) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `address_lane` text NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `address_type` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_addresses`
--

INSERT INTO `tbl_user_addresses` (`id`, `user_id`, `primary_address`, `full_name`, `mobile`, `zip`, `address_lane`, `landmark`, `city`, `state`, `address_type`, `created_on`) VALUES
(1, 2, '1', 'Sid', '9848012345', '500501', 'Road No 3, KPHB, Hyderabad', 'Remedy Hospital', 'Hyderabad', 'Telangana', 'HOME', '2020-10-22 06:07:23'),
(2, 3, NULL, 'Arun', '9052304477', '560068', 'SF #6', 'Dmart', 'KPHB', 'Telangana', 'WORK', '2020-10-23 08:22:54'),
(3, 4, NULL, 'Arun Eppanapelly', '8639930690', '45431', 'Flat 306', 'Dmart', 'Madhapur', 'Telangana', 'HOME', '2020-11-05 14:29:06'),
(6, 5, '1', 'Ram', '07801019797', '500081', 'Plot # 41', 'ValueLabs, Hitech City, Phase 2,', 'Hyderabad', 'Telangana', 'HOME', '2020-11-10 16:17:55'),
(18, 1, '1', 'arun', 'jkhdjkhtijh', '500001', 'h.no 7-2-98,srnagar', 'kfc', 'Vijayawada', 'Telangana', 'HOME', '2021-01-13 17:45:30'),
(19, 1, '0', 'kiran', '9191999999', '44546565767', 'h.no 7-2-98', 'kfc', 'Vijayawada', 'Andhra Pradesh', 'HOME', '2021-01-13 18:24:50'),
(20, 6, '0', 'anusha', '9000000001', '500038', 'h.no 7-2-98,srnagar', 'kfc', 'Karimnagar', 'Telangana', 'HOME', '2021-01-14 04:49:20'),
(21, 7, '1', 'Sravani', '8527193357', '500062', 'kapra', 'kapra', 'Hyderabad', 'Telangana', 'HOME', '2021-02-17 07:05:16'),
(22, 8, '0', 'KR NARESH', '7989328565', '500015', 'PAI INTERNATIONAL THIRUMALAGIRI', 'NEAR BY KHADHIM FOOT WARE', 'Hyderabad', 'Telangana', 'WORK', '2021-03-16 07:10:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wishlist`
--

CREATE TABLE `tbl_wishlist` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wishlist`
--

INSERT INTO `tbl_wishlist` (`id`, `user_id`, `product_id`, `created_on`) VALUES
(1, 4, 73, '2020-12-30 05:50:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attributes`
--
ALTER TABLE `tbl_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_attribute_masters`
--
ALTER TABLE `tbl_attribute_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_attribute_values`
--
ALTER TABLE `tbl_attribute_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_create_sale_items`
--
ALTER TABLE `tbl_create_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_home_top_banners`
--
ALTER TABLE `tbl_home_top_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_main_categories`
--
ALTER TABLE `tbl_main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order_return_products`
--
ALTER TABLE `tbl_order_return_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_otp`
--
ALTER TABLE `tbl_otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_our_advantages`
--
ALTER TABLE `tbl_our_advantages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_attributes`
--
ALTER TABLE `tbl_product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product_stock`
--
ALTER TABLE `tbl_product_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_recently_viewed`
--
ALTER TABLE `tbl_recently_viewed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_scrolling_message`
--
ALTER TABLE `tbl_scrolling_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_shipments`
--
ALTER TABLE `tbl_shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_taxes`
--
ALTER TABLE `tbl_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_addresses`
--
ALTER TABLE `tbl_user_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attributes`
--
ALTER TABLE `tbl_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_attribute_masters`
--
ALTER TABLE `tbl_attribute_masters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_attribute_values`
--
ALTER TABLE `tbl_attribute_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_create_sale_items`
--
ALTER TABLE `tbl_create_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_home_top_banners`
--
ALTER TABLE `tbl_home_top_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_main_categories`
--
ALTER TABLE `tbl_main_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_order_products`
--
ALTER TABLE `tbl_order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_order_return_products`
--
ALTER TABLE `tbl_order_return_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_otp`
--
ALTER TABLE `tbl_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_our_advantages`
--
ALTER TABLE `tbl_our_advantages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tbl_product_attributes`
--
ALTER TABLE `tbl_product_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product_stock`
--
ALTER TABLE `tbl_product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;

--
-- AUTO_INCREMENT for table `tbl_recently_viewed`
--
ALTER TABLE `tbl_recently_viewed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_scrolling_message`
--
ALTER TABLE `tbl_scrolling_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_shipments`
--
ALTER TABLE `tbl_shipments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_tags`
--
ALTER TABLE `tbl_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_taxes`
--
ALTER TABLE `tbl_taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_user_addresses`
--
ALTER TABLE `tbl_user_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_wishlist`
--
ALTER TABLE `tbl_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
