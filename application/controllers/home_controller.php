<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('vendor_model');	
	}
	
	public function top_banners()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
	    $data['top_banners'] = $this->vendor_model->get_data(array(), 'tbl_home_top_banners');
	    $data['sales'] = $this->vendor_model->get_data(array(), 'tbl_create_sale_items');
		$this->load->view('admin/home/top_banners', $data);
	}
	public function add_top_banners()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['heading'] = "Top Banner";
		$data['back'] = base_url('home/top_banners');
		$data['action'] = base_url('home_controller/upload_banners');
		$data['error_redirect'] = "home/add_top_banners";
		$data['success_redirect'] = "home/top_banners";
		$data['table'] = "tbl_home_top_banners";
		$data['upload_path'] = "uploads/images/top_banners/";
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		$this->load->view('admin/home/add_banner', $data);
	}
	public function upload_banners()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$main_category_id = $this->input->post('main_category_id');
		$discount = $this->input->post('discount', TRUE);

		$error_redirect = $this->input->post('error_redirect', TRUE);
		$success_redirect = $this->input->post('success_redirect', TRUE);
		$table = $this->input->post('table', TRUE);
		$upload_path = $this->input->post('upload_path', TRUE);

		if(isset($_FILES['banner_image']['name']) && !empty($_FILES['banner_image']['name']))
		{
        	$config['upload_path'] = $upload_path;
            $config['allowed_types'] = '*';
            $config['file_name'] = 'home_top_banner_'.date("Y_m_d_H_i_s");
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('banner_image'))
	        {
	        	$error =  $this->upload->display_errors();
	        	$this->session->set_flashdata('danger', $error);
	        	redirect($error_redirect);
	        }
	        else
	        {
	        	$imageData = $this->upload->data();
	            $banner_image = $imageData['file_name'];
	        }
        }
        
        $vendor_data = array('main_category_id'=>$main_category_id, 'discount'=>$discount, 'banner_image'=>$banner_image);

		if(!empty($banner_image)){
    		$data= $this->vendor_model->insert_data($vendor_data, $table);
    		$this->session->set_flashdata('success','Top Banner added Successfully');
        	redirect($success_redirect);
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect($error_redirect);
		}
	}
	public function delete_top_banner($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$delete_image = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_home_top_banners');
		if(!empty($delete_image['banner_image'])){
			unlink("uploads/images/top_banners/".$delete_image['banner_image']);
		}
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_home_top_banners');
		$this->session->set_flashdata('danger','Top Banner removed successfully');
		redirect('home/top_banners');
	}
	public function top_banner_status($id='', $status='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data= $this->vendor_model->update_data(array('status'=>$status), array('id'=>base64_decode($id)), 'tbl_home_top_banners');
		if($status == 1){
			$this->session->set_flashdata('success','Top Banner status changed successfully');
		}else{
			$this->session->set_flashdata('danger','Top Banner status changed successfully');
		}
		redirect('home/top_banners');
	}

	public function direct_cart_items()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
		$data['sub_categories'] = $this->vendor_model->get_data(array(), 'tbl_sub_categories');
		if($this->session->userdata('role') == "admin"){
			$data['products'] = $this->vendor_model->get_products();
		}
		if($this->session->userdata('role') == "vendor"){
			$id = $this->session->userdata("id");
			$data['products'] = $this->vendor_model->get_vendor_products($id);
		}
		$this->load->view('admin/home/direct_cart_items', $data);
	}
	public function get_direct_cart_items($value='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$category = $this->input->get_post('category');
		$sub_category = $this->input->get_post('sub_category');
		$show_only_checked = $this->input->get_post('show_only_checked');

		if($this->session->userdata('role') == "admin"){
			if(!empty($category) && empty($sub_category)) {
				$records = $this->vendor_model->ajax_get_products(array('tbl_products.main_category_id'=>10, 'tbl_products.category_id'=>$category));
			}elseif(empty($category) && !empty($sub_category)) {
				$records = $this->vendor_model->ajax_get_products(array('tbl_products.main_category_id'=>10, 'tbl_products.sub_category_id'=>$sub_category));
			}elseif(!empty($category) && !empty($sub_category)) {
				$records = $this->vendor_model->ajax_get_products(array('tbl_products.main_category_id'=>10, 'tbl_products.category_id'=>$category, 'tbl_products.sub_category_id'=>$sub_category));
			}else {
				$records = $this->vendor_model->ajax_get_products(array('tbl_products.main_category_id'=>10));
			}
		}
		$data = [];
        $check = $this->vendor_model->get_single_data(array(), 'tbl_home_direct_cart_items');
        if(!empty($check)){
        	$check_array = explode(',', $check['items']);
        }

		foreach($records->result() as $record){
			$add = "<input type='checkbox' name='add_item' class='add_item' value='".$record->id."'";
			if(isset($check_array) && !empty($check_array)) {
				if(in_array($record->id, $check_array)){
					$add .= "checked";
				}
			}
			$add .= ">";
			if($show_only_checked == 1){
		        $check = $this->vendor_model->get_single_data(array(), 'tbl_home_direct_cart_items');
		        if(!empty($check)){
		        	$check_array = explode(',', $check['items']);
		        }
		        if(isset($check_array) && !empty($check_array)) {
					if(in_array($record->id, $check_array)){
						$data[] = array(
							"id"=>$add,
							"product_title"=>$record->product_title,
							"product_price"=>$record->product_price,
							"category_name"=>$record->category_name,
							"sub_category_name"=>$record->sub_category_name,
						);
					}
				}
			}else{
				$data[] = array(
					"id"=>$add,
					"product_title"=>$record->product_title,
					"product_price"=>$record->product_price,
					"category_name"=>$record->category_name,
					"sub_category_name"=>$record->sub_category_name,
				);
			}
			
		}

		$response = array(
			"draw" => $draw,
			"recordsTotal" => $records->num_rows(),
			"recordsFiltered" => $records->num_rows(),
			"data" => $data
		);

		echo json_encode($response);
      	exit();
	}
	public function upload_direct_cart_items()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$add_all = $this->input->get_post('add_all');
		$add_products_id = $this->input->get_post('add_products_id');
		$remove_products_id = $this->input->get_post('remove_products_id');

		if(!empty($add_all) && ($add_all == "yes")){
			$query = $this->vendor_model->get_data(array(), 'tbl_products');
			
			$array = [];
			foreach ($query as $value){
				array_push($array, $value['id']);
			}
			
			if(!empty($array)){
				$items = implode(',', $array);
			}else{
				$items = "";
			}
			
			$check = $this->vendor_model->get_data(array(), 'tbl_home_direct_cart_items');
			if(empty($check)){
				$insert_data = $this->vendor_model->insert_data(array('items'=>$items),'tbl_home_direct_cart_items');
			}else{
				$insert_data = $this->vendor_model->update_data(array('items'=>$items), array('id'=>$check[0]['id']), 'tbl_home_direct_cart_items');
			}
			
			if($insert_data){
				echo "Success";
			}else{
				echo "Something went wrong. Please try after sometime.";
			}
		}else{
			if(!empty($add_products_id) || !empty($remove_products_id)){
				$check = $this->vendor_model->get_single_data(array(), 'tbl_home_direct_cart_items');
				if(!empty($check)){
					if(!empty($check['items'])){
						$items = $check['items'];
						$products = explode(',', $check['items']);
						
						if(!empty($remove_products_id)){
							foreach ($remove_products_id as $value) {
								if (($key = array_search($value, $products)) !== false) {
									unset($products[$key]);
									$items = implode(',', $products);
								}
							}
						}
						if(!empty($add_products_id)){
							foreach ($add_products_id as $value) {
								if (!(($key = array_search($value, $products)) !== false)) {
									$items .= ','.$value;
								}
							}
						}
						$insert_data = $this->db->where('id', $check['id'])->set('items', $items)->update('tbl_home_direct_cart_items');
					}else{
						$insert_data = $this->db->where('id', $check['id'])->set('items', implode(',', $this->input->get_post('add_products_id')))->update('tbl_home_direct_cart_items');
					}
				}else{
					$insert_data = $this->vendor_model->insert_data(array('items'=>implode(',', $this->input->get_post('add_products_id'))), 'tbl_home_direct_cart_items');
				}
				
				if($insert_data){
					echo "Success";
				}else{
					echo "Failed";
				}
			}else{
				echo "Something went wrong. Please try again.";
			}
		}
	}

	public function banner_linked_to($value='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$id = $this->input->get_post('id');
		$linked_to = $this->input->get_post('linked_to');
		$table = $this->input->get_post('table');

		if(!empty($id) && !empty($table)){
			$insert_data = $this->vendor_model->update_data(array('linked_to'=>$linked_to), array('id'=>$id), $table);
			if($insert_data){
				echo "Success";
			}else{
				echo "Failed";
			}
		}
	}

	public function view_sale_products($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['products'] = $this->vendor_model->get_products();
		
		if(!empty($id)){
			$data['sale_products'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_create_sale_items');
		}
		$this->load->view('admin/home/view_products', $data);
	}
}