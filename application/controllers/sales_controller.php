<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('orders');
        $this->load->model('vendor_model');
    }

    public function orders($value='')
    {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
        {
			redirect('admin');
		}
        $data['orders'] = $this->vendor_model->get_orders();
        $this->load->view('admin/sales/orders', $data);
    }

    public function order_details($id='')
    {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
        {
			redirect('admin');
		}
        if(!empty($id)){
            $data['order_data'] = $this->vendor_model->get_order_data(base64_decode($id));
            //var_dump($data['order_data']);exit;
            $data['address'] = $this->vendor_model->get_single_data(array('id'=>$data['order_data']['address_id']), 'tbl_user_addresses');
            $data['products'] = $this->vendor_model->get_products_by_order(base64_decode($id));
            $data['shipments'] = $this->vendor_model->getOrderShipemts(base64_decode($id));
            $data['user_data'] = $this->vendor_model->get_single_data(array('id'=>$data['order_data']['user_id']), 'tbl_users');
        }
        $this->load->view('admin/sales/order_details', $data);
    }
    public function return_confirm($id='')
    {
         $return = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_order_return_products');
         $this->vendor_model->update_data(['status'=>2],['id'=>$id],'tbl_order_return_products');
         redirect('sales/order_details/'.base64_encode($return['order_id']));
    }
    public function return_request_list($value='')
    {
        if($this->session->userdata("id")=="")
        {
            redirect('admin');
        }
        if($this->session->userdata('role') == "admin"){
            $data['orders'] = $this->vendor_model->get_return_orders();
        }
        if($this->session->userdata('role') == "vendor"){
            $data['orders'] = $this->vendor_model->get_return_orders_by_seller($this->session->userdata('id'));
        }
        $this->load->view('admin/sales/return_orders', $data);
    }

    public function change_product_order_status()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if($status == "Approve"){
            $status = Orders::$vendorApproved;
            $orderStatus = Orders::$inProgress;
        }
        if($status == "Reject"){
            $status = Orders::$vendorCancelled;
            $orderStatus = Orders::$vendorCancelled;
        }
        if(!empty($id) && !empty($status)){
            $check = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_order_products');
            $statusInfo = json_decode($check['status_info'],true);
            $statusInfo[$status] = date('d M Y h:i:s a');
            if($check['order_product_status']==Orders::$orderPlaced){
                $data= $this->vendor_model->update_data(array('order_product_status'=>$status,'status_info'=>json_encode($statusInfo)) , array('id'=>$id), 'tbl_order_products');
                $order_id = $check['order_id'];
                $data= $this->vendor_model->update_data(array('order_status'=>$orderStatus) , array('id'=>$order_id), 'tbl_orders');
                if($data){
                    echo "Success";
                }else{
                    echo "Failed";
                }
            }else{
                echo "Order Status already changed";
            }
        }else{
            echo "Something went wrong. Please try again.";
        }
    }
    public function handover_to_delivery()
    {
        $order_product_id = $this->input->post('order_product_id');
        $order_id = $this->input->post('order_id');
        $tracking_id = $this->input->post('tracking_id');
        $delivery_partner = $this->input->post('delivery_partner');

        $vendor_data = array('order_product_status'=>Orders::$handOverToCourier,
         'tracking_id'=>$tracking_id, 'delivery_partner'=>$delivery_partner);
       
        if(!empty($order_product_id) && !empty($tracking_id) && !empty($delivery_partner)){

            $check = $this->vendor_model->get_single_data(array('id'=>$order_product_id), 'tbl_order_products');
            $statusInfo = json_decode($check['status_info'],true);
            $statusInfo[Orders::$handOverToCourier] = date('d M Y h:i:s a');
            $vendor_data['status_info'] = json_encode($statusInfo);

            if($check['order_product_status']==Orders::$vendorApproved && empty($check['tracking_id']) && empty($check['delivery_partner'])){
                $data= $this->vendor_model->update_data($vendor_data , array('id'=>$order_product_id), 'tbl_order_products');
                if($data){
                    $this->createShipment($order_product_id,$check['order_id'],$check['product_id'],$tracking_id,$delivery_partner);
                    $this->session->set_flashdata('success', 'Handovered to Delivery Partner');
			        redirect('sales/order_details/'.base64_encode($order_id));
                }else{
                    $this->session->set_flashdata('danger', 'Failed');
			        redirect('sales/order_details/'.base64_encode($order_id));
                }
            }else{
                $this->session->set_flashdata('danger', 'Order already Handovered to a Delivery Partner');
			    redirect('sales/order_details/'.base64_encode($order_id));
            }
        }else{
            $this->session->set_flashdata('danger', 'Please input all fields');
			redirect('sales/order_details/'.base64_encode($order_id));
        }
    }

    public function shipments($value='')
    {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
        {
			redirect('admin');
		}
         $data['shipments'] = $this->vendor_model->getShipments();
        $this->load->view('admin/sales/shipments',$data);
    }

    public function createShipment($order_product_id,$order_id,$product_id,$tracking_id,$delivery_partner){
        if (!empty($order_product_id) && !empty($order_id) && !empty($product_id) && 
        !empty($tracking_id) && !empty($delivery_partner)){
           
        $productDatas = $this->vendor_model->get_single_data(array('product_id'=>$product_id), 'tbl_order_products');
        
        $productData = $this->vendor_model->get_single_data(array('id'=>json_decode($productDatas['product_info'])->product_id), 'tbl_products');
        
        $shipmentData = array(
            'order_id' => $order_id,
            'order_product_id' => $order_product_id,
            'product_id' => $product_id,
            
            'product_name' => $productData['product_title'],
            'product_image' => $productData['banner_image'],
            'tracking_id' => $tracking_id,
            'delivery_partner' => $delivery_partner,
            'shipment_status' => Orders::$shipped,
        );
        $checkShpmentExist = $this->vendor_model->get_single_data(array('order_id'=>$order_id,'order_product_id' => $order_product_id,), 'tbl_shipments');
        if (empty($checkShpmentExist)){
        $this->vendor_model->insert_data($shipmentData,'tbl_shipments');
        }
        return true;
    }
    }
    public function orderFilter($val){
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
        {
			redirect('admin');
        }
        if ($val == "new"){
            $status = Orders::$orderPlaced;
        } else if($val== "inProgress") {
            $status = Orders::$inProgress;
        } else if($val== "completed") {
            $status = Orders::$orderCompleted;
        } else if($val== "rejected") {
            $status = Orders::$vendorCancelled;
        } else if($val== "returned") {
            $status = Orders::$returned;
        }else if($val== "old") {
            $status = Orders::$returned;
            $orderBy = "orderBy";
        }else{
            $status = null;
        }
        if ($status!=null){
            $where = $status;
        } else{
        $where = null;
        }
        if (!isset($orderBy)){
            $orderBy = null;
        }
        
        $data['orders'] = $this->vendor_model->get_orders($where,$orderBy);
        
        $this->load->view('admin/sales/orders', $data);
    }
}
