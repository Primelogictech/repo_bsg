<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masters_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('vendor_model');	
	}

	public function attributes()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
	    $data['attributes'] = $this->vendor_model->get_data(array(), 'tbl_attributes');
		$this->load->view('admin/masters/attributes', $data);
	}
	public function upload_attribute()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$name = $this->input->post('name');

        if(!empty($name)){
        	$vendor_data = array('attribute_name'=>$name);
        	$data = $this->vendor_model->insert_data($vendor_data, 'tbl_attributes');
    		$this->session->set_flashdata('success','Attribute added Successfully');
        }else{
			$this->session->set_flashdata('danger', 'Please input all fields');
		}
		redirect('masters/attributes');
	}

	public function attribute_masters()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data['masters'] = $this->vendor_model->get_attribute_masters();
		$this->load->view('admin/masters/attribute_masters', $data);
	}
	public function add_attribute_masters($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
        $data['attributes'] = $this->vendor_model->get_data(array(), 'tbl_attributes');
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_attribute_masters');
		}
		$this->load->view('admin/masters/add_attribute_masters', $data);
	}
	public function upload_attribute_masters($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$main_category_id = $this->input->post('main_category_id', TRUE);
		$category_id = $this->input->post('category_id', TRUE);
		$attributes = implode(',', $this->input->post('attributes'));

		$vendor_data = array('main_category_id'=>$main_category_id, 'category_id'=>$category_id, 'attributes'=>$attributes, 'created_at'=>date('Y-m-d H:i:s'));

		if(!empty($main_category_id) && !empty($category_id)){
			if(!empty($id)){
        		$data= $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_attribute_masters');
        		$this->session->set_flashdata('success','Masters updated Successfully');
        	}else{
        		$check = $this->vendor_model->get_single_data(array('main_category_id'=>$main_category_id, 'category_id'=>$category_id), 'tbl_attribute_masters');
        		if(empty($check)){
        			$data= $this->vendor_model->insert_data($vendor_data,'tbl_attribute_masters');
        			$this->session->set_flashdata('success','Masters added Successfully');
        		}else{
        			$data= $this->vendor_model->update_data($vendor_data, array('id'=>$check['id']), 'tbl_attribute_masters');
        			$this->session->set_flashdata('success','Masters updated Successfully');
        		}
        	}
        	redirect('masters/attribute_masters');
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('masters/add_attribute_masters');
		}
	}
	public function delete_attribute_masters($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_attribute_masters');
		$this->session->set_flashdata('danger','Masters removed successfully');
		redirect('masters/attribute_masters');
	}

	public function our_advantages()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
	    $data['our_advantages_data'] = $this->vendor_model->get_single_data(array(), 'tbl_our_advantages');
		$this->load->view('admin/masters/our_advantages', $data);
	}

	public function upload_our_advantages()
	{
		$id = $this->input->post('id');

		if(isset($_FILES['images']['name']) && !empty($_FILES['images']['name'][0]))
		{
	        $files = $_FILES;
        	$count = count($_FILES['images']['name']);
        	for($i=0; $i<$count; $i++){
                $_FILES['images']['name']= $files['images']['name'][$i];
                $_FILES['images']['type']= $files['images']['type'][$i];
                $_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
                $_FILES['images']['error']= $files['images']['error'][$i];
                $_FILES['images']['size']= $files['images']['size'][$i];

                $config['upload_path'] = 'uploads/images/our_advantages_images/';
	            $config['allowed_types'] = '*';
	            $config['file_name'] = "our_advantages_images_".($i+1);
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);

	            if(!$this->upload->do_upload('images'))
		        {
		        	$error =  $this->upload->display_errors();
		        	$this->session->set_flashdata('danger', $error);
		        	redirect('masters/our_advantages');
		        }
		        else
		        {
		        	$upload_data = $this->upload->data();
                	$fileNames[] = $upload_data['file_name'];
		        }
            }
            if(!empty($id)){
				$get_images = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_our_advantages');
				if(!empty($get_images['images'])){
					array_push($fileNames, $get_images['images']);
				}
			}
			$vendor_data = ['images'=>implode(',', $fileNames)];
			
			if(!empty($vendor_data)){
				if(!empty($id)){
					$data= $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_our_advantages');
					$this->session->set_flashdata('success','Our Advantages updated Successfully');
				}else{
					$data= $this->vendor_model->insert_data($vendor_data,'tbl_our_advantages');
					$this->session->set_flashdata('success','Our Advantages added Successfully');
				}
				redirect('masters/our_advantages');
			}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('masters/our_advantages');
		}
	}
	public function delete_our_advantages_images($id='', $name='')
	{
		$delete_image = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_our_advantages');
		if(!empty($delete_image['images'])){
			$images = explode(',', $delete_image['images']);

	        if (($key = array_search($name, $images)) !== false) {
                unset($images[$key]);
                unlink("uploads/images/our_advantages_images/".$name);
            }

            $insert_data = $this->db->where('id',$id)->set('images', implode(',', $images))->update('tbl_our_advantages');
		}

		redirect('masters/our_advantages');
	}


	public function tags()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
	    $data['tags'] = $this->vendor_model->get_tags();
		$this->load->view('admin/masters/tags', $data);
	}
	public function add_tags($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_tags');
		}
		$this->load->view('admin/masters/add_tags', $data);
	}
	public function upload_tags($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$main_category_id = $this->input->post('main_category_id', TRUE);
		$category_id = $this->input->post('category_id', TRUE);
		$tags = strtolower(preg_replace('/\s*,\s*/', ',', $this->input->post('tags', TRUE)));

		$vendor_data = array('main_category_id'=>$main_category_id, 'category_id'=>$category_id, 'tags'=>$tags, 'created_at'=>date('Y-m-d H:i:s'));

		if(!empty($main_category_id) && !empty($category_id)){
			if(!empty($id)){
        		$data= $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_tags');
        		$this->session->set_flashdata('success','Tags updated Successfully');
        	}else{
        		$check = $this->vendor_model->get_single_data(array('main_category_id'=>$main_category_id, 'category_id'=>$category_id), 'tbl_tags');
        		if(empty($check)){
        			$data= $this->vendor_model->insert_data($vendor_data,'tbl_tags');
        			$this->session->set_flashdata('success','Tags added Successfully');
        		}else{
        			$data= $this->vendor_model->update_data($vendor_data, array('id'=>$check['id']), 'tbl_tags');
        			$this->session->set_flashdata('success','Tags updated Successfully');
        		}
        	}
        	redirect('masters/tags');
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('masters/add_tags');
		}
	}
	public function delete_tags($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_tags');
		$this->session->set_flashdata('danger','Tags removed successfully');
		redirect('masters/tags');
	}

	public function scrolling_message($value='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$this->load->view('admin/masters/scrolling_message', $data);
	}

	public function upload_scrolling_message($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$scrolling_message = $this->input->post('scrolling_message', TRUE);

		$vendor_data = array('scrolling_message'=>$scrolling_message, 'created_at'=>date('Y-m-d H:i:s'));

		if(!empty($scrolling_message)){
			if(!empty($id)){
        		$data= $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_scrolling_message');
        		$this->session->set_flashdata('success','Scrolling Message updated Successfully');
        	}else{
        		$data= $this->vendor_model->insert_data($vendor_data,'tbl_scrolling_message');
        		$this->session->set_flashdata('success','Scrolling Message added Successfully');
        	}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
		}
		redirect('masters/scrolling_message');
	}
}