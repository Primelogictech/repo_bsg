<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * This controller created by Anirudh
 * purpose : Tax Management
 * 
 */
class Tax_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('vendor_model');
    }

    /*
     * This is action index
     * purpose: Listing all Tax 
     */
    public function index($value = '') {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
        $data['tax'] = $this->vendor_model->get_data(array(), 'tbl_taxes');
        $this->load->view('admin/tax/index', $data);
    }
    
    /*
     * This is action add_tax
     * purpose: adding Tax
     */
    public function add_tax($id = '') {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
        $data = [];
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_taxes');
		}
        $this->load->view('admin/tax/add_tax',$data);
    }
    
    /*
     * This is action add_tax
     * purpose: upload Tax
     */
    public function upload_tax($value = '') {
        if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('tax', TRUE);
		$percentage = $this->input->post('percentage', TRUE);

		$tax_data = array('name'=>$name, 'percentage'=>$percentage);

		if(!empty($name) && !empty($percentage)){
			if(!empty($id)){
        		$data= $this->vendor_model->update_data($tax_data, array('id'=>$id), 'tbl_taxes');
        		$this->session->set_flashdata('success','Tax updated Successfully');
        	}else{
        		$check = $this->vendor_model->get_single_data(array('name'=>$name, 'percentage'=>$percentage), 'tbl_taxes');
        		if(empty($check)){
        			$data= $this->vendor_model->insert_data($tax_data,'tbl_taxes');
        			$this->session->set_flashdata('success','Tax added Successfully');
        		}else{
        			$data= $this->vendor_model->update_data($tax_data, array('id'=>$check['id']), 'tbl_taxes');
        			$this->session->set_flashdata('success','Tax updated Successfully');
        		}
        	}
        	redirect('tax');
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('add_tax');
		}
    }
    
    public function delete_tax($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_taxes');
		$this->session->set_flashdata('danger','Tax removed successfully');
		redirect('tax');
	}

}
