<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require('razorpay-php/Razorpay.php');
require('razorpay-php/config.php');
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;
class User_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('orders');
		$this->load->model('vendor_model');	
	}

	public function login()
	{
		if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User"))
		{
			redirect('home');
		}
		$this->load->view('user/login');
	}
	public function registration()
	{
		$this->load->view('user/register');
	}
	public function register_user()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$mobile_number = $this->input->post('mobile_number');
		$password = $this->input->post('re_password');

		if(!empty($name) && !empty($email) && !empty($mobile_number) && !empty($password)){
			$email_check = $this->vendor_model->get_single_data(array('email'=>$email), 'tbl_users');
			$mobile_number_check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_users');
			if(!empty($email_check)){
			    $this->session->set_flashdata('danger', 'Email already exists');
			    redirect('registration');
			}else if(!empty($mobile_number_check)){
				$this->session->set_flashdata('danger', 'Mobile Number already exists');
			    redirect('registration');
			}else{
				$vendor_data = array('name'=>$name, 'email'=>$email, 'mobile_number'=>$mobile_number, 'password'=>password_hash($password, PASSWORD_DEFAULT), 'role'=>'User', 'created_at'=>date('Y-m-d H:i:s'));
				$data = $this->vendor_model->insert_data($vendor_data,'tbl_users');
				if($data){
					$this->session->set_flashdata('success', 'Registration completed Successfully');
					redirect('login');
				}else{
					$this->session->set_flashdata('danger', 'Something went wrong. Please try again later');
    				redirect('registration');
				}
			}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
    		redirect('registration');
		}
	}
	public function verify_user()
	{
		$email_mobile_number = $this->input->post('email_mobile_number');
		$password = $this->input->post('password');

		if(!empty($email_mobile_number) && !empty($password)){
			$email_check = $this->vendor_model->get_single_data(array('email'=>$email_mobile_number), 'tbl_users');
			$mobile_number_check = $this->vendor_model->get_single_data(array('mobile_number'=>$email_mobile_number), 'tbl_users');
			if(empty($email_check) && empty($mobile_number_check)){
			    $this->session->set_flashdata('danger', 'User does not exists');
			    redirect('login');
			}else{
				if(!empty($email_check)){
					$hash = $email_check['password'];
				}else{
					$hash = $mobile_number_check['password'];
				}
				
				if(password_verify($password, $hash)){
					if(!empty($email_check)){
						$user_data = [
	    					"name" => $email_check['name'],
	    					"email" => $email_check['email'],
	    					"id" => $email_check['id'],
	    					"role" => $email_check['role']
	    				];
	    			}else{
	    				$user_data = [
	    					"name" => $mobile_number_check['name'],
	    					"email" => $mobile_number_check['email'],
	    					"id" => $mobile_number_check['id'],
	    					"role" => $mobile_number_check['role']
	    				];
	    			}
					$this->session->set_userdata($user_data);
					$url = $this->session->userdata("login_redirect");
					if(!empty($url)) {
						redirect($url);
					}else{
						redirect('home');
					}
				}else{
					$this->session->set_flashdata('danger','Invalid Password');
			    	redirect('login');
				}
			}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
    		redirect('login');
		}
	}
	public function user_logout()
	{
		$this->session->sess_destroy();
		redirect('home', 'refresh');
	}
	public function forgot_password()
	{
		$this->load->view('user/forgot_password');
	}
	public function send_otp()
	{
		$mobile_number = $this->input->post('mobile_number');
		$forgot_password = $this->input->post('forgot_password');

		if(!empty($mobile_number)){
			if(!empty($forgot_password) && $forgot_password == "yes"){
				$check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_users');
				if(!empty($check)){
					$otp = mt_rand(100000,999999);

					$message = urlencode("Your OTP for Forgot Password verification is BSG-".$otp);

		        	$send = file_get_contents("http://173.45.76.227/send.aspx?username=bsg2&pass=garments2&route=Trans1&senderid=INFOSM&numbers=$mobile_number&message=$message");
					
					if($send){
						$check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_otp');
						if(empty($check)){
							$this->vendor_model->insert_data(array('mobile_number'=>$mobile_number, 'otp'=>$otp, 'created_at'=>date('Y-m-d H:i:s')), 'tbl_otp');
						}else{
							$this->vendor_model->update_data(array('otp'=>$otp, 'created_at'=>date('Y-m-d H:i:s')), array('mobile_number'=>$mobile_number), 'tbl_otp');
						}
						echo "OTP Successfully Sent To Your Mobile Number";
					}else{
						echo "OTP not sent";
					}
				}else{
					echo "Mobile Number doesn't exist";
				}
			}else{
				$data = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_users');
				if(empty($data)){
					$otp = mt_rand(100000,999999);

					$message = urlencode("Your OTP for Mobile Number verification is BSG-".$otp);

		        	$send = file_get_contents("http://173.45.76.227/send.aspx?username=bsg2&pass=garments2&route=Trans1&senderid=INFOSM&numbers=$mobile_number&message=$message");
					
					if($send){
						$check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_otp');
						if(empty($check)){
							$this->vendor_model->insert_data(array('mobile_number'=>$mobile_number, 'otp'=>$otp, 'created_at'=>date('Y-m-d H:i:s')), 'tbl_otp');
						}else{
							$this->vendor_model->update_data(array('otp'=>$otp, 'created_at'=>date('Y-m-d H:i:s')), array('mobile_number'=>$mobile_number), 'tbl_otp');
						}
						echo "OTP Successfully Sent To Your Mobile Number";
					}else{
						echo "OTP not sent";
					}
				}else{
					echo "Mobile Number already exist's";
				}
			}
		}else{
			echo "Mobile Number is Required";
		}
	}
	public function verify_otp($value='')
	{
		$otp = $this->input->post('otp');
		$mobile_number = $this->input->post('mobile_number');

		if(!empty($mobile_number) && !empty($otp)){
			$check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number, 'otp'=>$otp), 'tbl_otp');
			if(!empty($check)){
				echo "success";
			}else{
				echo "failed";
			}
		}
	}
	public function forgot_password_update()
	{
		$mobile_number = $this->input->post('mobile_number');
		$password = $this->input->post('re_password');

		if(!empty($mobile_number) && !empty($password)){
			$mobile_number_check = $this->vendor_model->get_single_data(array('mobile_number'=>$mobile_number), 'tbl_users');
			if(!empty($mobile_number_check)){
				$data = $this->vendor_model->update_data(array('password'=>password_hash($password, PASSWORD_DEFAULT)), array('mobile_number'=>$mobile_number), 'tbl_users');
				if($data){
					$this->session->set_flashdata('success', 'Password changed Successfully');
					redirect('login');
				}else{
					$this->session->set_flashdata('danger', 'Something went wrong. Please try again later');
    				redirect('forgot_password');
				}
			}else{
				$this->session->set_flashdata('danger', "Mobile Number doesn't exist");
    			redirect('forgot_password');
			}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
    		redirect('forgot_password');
		}
	}
	
	public function index()
	{
		if($this->session->userdata('id') != '' && $this->session->userdata('role') == 'User'){
			$query = $this->vendor_model->get_single_data(array('user_id'=>$this->session->userdata('id')), 'tbl_recently_viewed');
			if(!empty($query)){
				$products = explode(',', $query['product_id']);
				$data['recently_viewed'] = $this->vendor_model->get_products_with_ids($products);
			}
			$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		}
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$data['top_banners'] = $this->vendor_model->get_data(array('main_category_id'=>1), 'tbl_home_top_banners');
		$data['categories'] = $this->vendor_model->get_data(array('main_category_id'=>1), 'tbl_categories');
		$data['newly_added_products'] = $this->vendor_model->get_products_limit(array('tbl_products.main_category_id'=>1), 'DESC', 6, 'tbl_products');
		$this->load->view('user/index', $data);
	}

	public function kids()
	{
		if($this->session->userdata('id') != '' && $this->session->userdata('role') == 'User'){
			$query = $this->vendor_model->get_single_data(array('user_id'=>$this->session->userdata('id')), 'tbl_recently_viewed');
			if(!empty($query)){
				$products = explode(',', $query['product_id']);
				$data['recently_viewed'] = $this->vendor_model->get_products_with_ids($products);
			}
			$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		}
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$data['top_banners'] = $this->vendor_model->get_data(array('main_category_id'=>2), 'tbl_home_top_banners');
		$data['categories'] = $this->vendor_model->get_data(array('main_category_id'=>2), 'tbl_categories');
		$data['newly_added_products'] = $this->vendor_model->get_products_limit(array('main_category_id'=>2), 'DESC', 6, 'tbl_products');

		$this->load->view('user/index', $data);
	}

	public function products()
	{
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$data['cid'] = $this->input->get_post('cid', TRUE);
		$data['sid'] = $this->input->get_post('sid', TRUE);
		$data['search_keyword'] = $this->input->get_post('tag', TRUE);
		$this->load->view('user/products', $data);
	}

	public function get_products_ajax($value='')
	{
		$cid = $this->input->get_post('cid', TRUE);
		$sid = $this->input->get_post('sid', TRUE);

		$search_keyword = $this->input->get_post('search_keyword', TRUE);

		$min_price_filter = $this->input->get_post('min_price_filter');
		$max_price_filter = $this->input->get_post('max_price_filter');

		$sleeves = $this->input->get_post('sleeves');
		$type = $this->input->get_post('type');
		$design = $this->input->get_post('design');

		$size_filter = $this->input->get_post('size_filter');

		$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		if(!empty($cid)){
			$data['products'] = $this->vendor_model->get_products_ajax(base64_decode($cid), $search_keyword, $min_price_filter, $max_price_filter, $sleeves, $type,$design, $size_filter);
		}elseif(!empty($search_keyword)){
			$data['products'] = $this->vendor_model->get_products_ajax(base64_decode($cid), $search_keyword, $min_price_filter, $max_price_filter, $sleeves, $type,$design, $size_filter);
		}elseif(!empty($sid)){
			$query = $this->vendor_model->get_single_data(array('id'=>base64_decode($sid)), 'tbl_create_sale_items');
			if (isset($query['products'])){
			$sale_products = explode(',', $query['products']);
			} else {
				$sale_products = '';
			}
			$data['products'] = $this->vendor_model->get_products_with_ids($sale_products, $search_keyword, $min_price_filter, $max_price_filter, $sleeves, $type,$design, $size_filter);
		}else{
		    $data['products'] = "";
		}
		$result = $this->load->view('user/products_ajax', $data);
		echo $result;
	}

	public function product_details()
	{
		$id = $this->input->get_post('pid', TRUE);
		if(!empty($id)){
			if($this->session->userdata('id') != '' && $this->session->userdata('role') == 'User'){
				$check = $this->vendor_model->get_single_data(array('user_id'=>$this->session->userdata('id')), 'tbl_recently_viewed');
				if(!empty($check)){
					$product_id = base64_decode($id).",".$check['product_id'];
					$add_to_recently_viewed = $this->vendor_model->update_data(array('product_id'=>$product_id, 'created_at'=>date('Y-m-d H:i:s')), array('user_id'=>$this->session->userdata('id')), 'tbl_recently_viewed');
				}else{
					$add_to_recently_viewed = $this->vendor_model->insert_data(array('user_id'=>$this->session->userdata('id'), 'product_id'=>base64_decode($id), 'created_at'=>date('Y-m-d H:i:s')), 'tbl_recently_viewed');
				}
				$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
			}
	        $data['product'] = $this->vendor_model->get_product_data(base64_decode($id));
	        /*var_dump($data['product']);exit;*/
			$data['attributes'] = $this->vendor_model->getAttributes(array('product_id'=>base64_decode($id)), 'tbl_product_stock');
			$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
			$data['items_you_may_like'] = $this->vendor_model->get_data_by_limit(array('main_category_id'=>isset($data['product']['main_category_id']) ?? '', 'category_id'=>isset($data['product']['category_id'])) ?? '', 'ASC', 6, 'tbl_products');
			//var_dump($data['attributes'][0]);die();
			$this->load->view('user/product_details', $data);
		}
	}

	public function checkout()
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		$userId = $this->session->userdata('id');
		$data['cart_data'] = $this->vendor_model->get_cart_details($userId);
		//var_dump($data['cart_data']); exit;
        if(empty($data['cart_data'])){
        	$this->load->view('user/empty_cart', $data);
        }else{
			$this->load->view('user/checkout', $data);
		}
	}

	public function add_wishlist()
    {
        if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
        $productId = $this->input->post('productId');
        $userId = $this->session->userdata('id');
        if (!empty($productId) && !empty($userId)){
            $data = [
              	'user_id' => $userId,
                'product_id' => $productId
            ];
            $p = $this->vendor_model->get_single_data($data,'tbl_wishlist');
            if (empty($p)){
                if ($this->vendor_model->insert_data($data,'tbl_wishlist')){
                    echo "Added To Wishlist";
                }
            } 
        }            
    }
    public function remove_wishlist(){
        if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$productId = $this->input->post('productId');
		$userId = $this->session->userdata('id');
		if (!empty($productId) && !empty($userId)){
			$data = [
				'user_id' => $userId,
				'product_id' => $productId
			];
			if($this->vendor_model->delete_data($data,'tbl_wishlist')){
				echo 'Removed From Wishlist';
			}
		}            
	}

	public function add_cart()
    {
        if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
        $productId = $this->input->post('productId');
        // $quantity = $this->input->post('quantity');
        $quantity = 1;
        $size = $this->input->post('size');
        $color = $this->input->post('color');
        $userId = $this->session->userdata('id');
        if (!empty($productId) && !empty($userId)){
            $data = [
              	'user_id' => $userId,
				'product_id' => $productId,
			];
			$dataIns = [
					'user_id' => $userId,
				  	'product_id' => $productId,
				  	'quantity' => $quantity,
				  	'size' => $size,
				  	'color' => $color
			  	];
            $p = $this->vendor_model->get_single_data($data,'tbl_cart');
            if(empty($p)){
                if ($this->vendor_model->insert_data($dataIns,'tbl_cart')){
                    echo "done";
                }
            }else{
                echo "added";
            }
        }            
    }
    public function remove_from_cart($productId=""){
        if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$userId = $this->session->userdata('id');
		if (!empty($productId) && !empty($userId)){
			$data = [
				'user_id' => $userId,
				'product_id' => $productId
			];
			$this->vendor_model->delete_data($data,'tbl_cart');
			$this->session->set_flashdata('danger','Removed from Cart successfully');
			redirect('checkout');
		}            
	}

    public function add_quantity(){
        if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$productId = $this->input->post('productId');
		$userId = $this->session->userdata('id');
		$quantity = $this->input->post('quantity');
		if (!empty($productId) && !empty($userId) && !empty($quantity)){
			$data = [
				'user_id' => $userId,
				'product_id' => $productId
			];
			$this->vendor_model->update_data(['quantity'=>$quantity], $data, 'tbl_cart');
		} 
	}

	public function my_address()
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$userId = $this->session->userdata('id');
		$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$userId), 'tbl_cart');
        $data['cart_data'] = $this->vendor_model->get_cart_details($userId);
		$data['addresses'] = $this->vendor_model->get_data(array('user_id'=>$userId), 'tbl_user_addresses');
		$data['payment'] = $_POST;
		$this->load->view('user/my_address',$data);
	}
	public function add_new_address()
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		if(!empty($_POST['payment'])){
	        $payment = $_POST['payment'];
		    $this->load->view('user/add_new_address',['payment'=>$payment]);
	    }else{
	        $this->load->view('user/add_new_address');
	    }
	}
	public function saveAddress(){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$user_id = $this->session->userdata('id');

		$address_id = $this->input->post('address_id');

		$full_name = $this->input->post('full_name');
		$mobile = $this->input->post('mobile');

		$zip = $this->input->post('zip');
		$address_lane = $this->input->post('address_lane');
		$landmark = $this->input->post('landmark');
		$city = $this->input->post('city');
		$state = $this->input->post('state');

		$address_type = $this->input->post('address_type');
		$make_defalut = $this->input->post('make_defalut');
		if($make_defalut){
			$make_defalut = 1;
			$this->vendor_model->update_data(array('primary_address'=>0), ['user_id'=>$user_id], 'tbl_user_addresses');
		}else{
			$make_defalut = 0;
		}

		$vendor_data = array('user_id'=>$user_id, 'primary_address'=>$make_defalut, 'full_name'=>$full_name, 'mobile'=>$mobile, 'zip'=>$zip, 'address_lane'=>$address_lane, 'landmark'=>$landmark, 'city'=>$city, 'state'=>$state, 'address_type'=>$address_type);

		$data = $_POST;
		$data['user_id'] = $this->session->userdata('id');
		
		$payment = $_POST['payment'];
		
		unset($data['payment']);
		
		if(!empty($full_name) && !empty($address_lane) && !empty($landmark) && !empty($mobile) && !empty($state) && !empty($city) && !empty($zip)){
			if(empty($address_id)){
				$success = $this->vendor_model->insert_data($vendor_data,'tbl_user_addresses');
			}else{
				$success = $this->vendor_model->update_data($vendor_data, ['id'=>$address_id], 'tbl_user_addresses');	
			}
			if($success){
				$this->session->set_flashdata('success', 'Address added Successfully');
			}else{
				$this->session->set_flashdata('danger', 'Unable to Address');
			}
		}else{
			$this->session->set_flashdata('danger', 'Please fill all fields');
		}
		if(empty($payment)){
			redirect('my_address');
		}else{
			$data['payment'] = json_decode(base64_decode($payment),true);
			$userId = $this->session->userdata('id');
			$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$userId), 'tbl_cart');
        	$data['cart_data'] = $this->vendor_model->get_cart_details($userId);
			$data['addresses'] = $this->vendor_model->get_data(array('user_id'=>$userId), 'tbl_user_addresses');
			$this->load->view('user/my_address',$data);
		}	
	}
	public function editAddress($id)
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$data = array();
		if (!empty($id)){
			$id = base64_decode($id);
			$data['address'] =  $this->vendor_model->get_single_data(
				array(
					'id' => $id,
				),
			 'tbl_user_addresses');
		}
		$this->load->view('user/add_new_address',$data);
	}
	public function getAddressData()
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$id = $this->input->post('id');
		if (!empty($id)){
			$data =  $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_user_addresses');
			echo json_encode($data);
			exit();
		}
	}

	public function deleteAddress($id='')
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_user_addresses');
		redirect('my_address');
	}

	public function preCheckout(){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		if (!empty($_POST['address']) && !empty($_POST['payment'])){
		$data['payment'] = $_POST;
		$where = array('id'=>$_POST['address']);
		$data['address'] = $this->vendor_model->get_single_data($where, 'tbl_user_addresses');
		$this->load->view('user/payment',$data);
		}else {
			redirect('checkout');
		}
	}

	public function payment(){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$post = json_decode(base64_decode($_POST['payment']),true);
		$post['paymentType'] = $_POST['paymentType'];
		$post['address'] = $_POST['address'];
		$post['delivery_charge'] = $_POST['deliveryamount'];

		if ($post['paymentType'] == "online"){
			//  TEST CREDENTIALS
			//$api = new Api("rzp_test_iB0k6Jnt813DDT", "AdAofG9q01poTeUsYwlT3caS");
			
			//  LIVE CREDENTIALS
			  $api = new Api("rzp_live_J0ie3qfuigQhgx", "tsVzZZEh3cZtCp1f6YHJf90c");

			$price = base64_decode(base64_decode($post['price']));
			if (isset($price) && !empty($price) && $price!="0"){
			$post = base64_encode(json_encode($post));
			$orderData = [
				'receipt'         => 3456,
				'amount'          => ($price * 100), // 2000 rupees in paise
				'currency'        => 'INR',
				'payment_capture' => 1 // auto capture
			];
			$displayCurrency = "INR";
			$razorpayOrder = $api->order->create($orderData);

			$razorpayOrderId = $razorpayOrder['id'];
			$this->session->set_userdata('razorpay_order_id', $razorpayOrderId);

			$displayAmount = $amount = $orderData['amount'];

			if ($displayCurrency !== 'INR')
			{
				$url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
				$exchange = json_decode(file_get_contents($url), true);

				$displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
			}

			$checkout = 'automatic';

			if (isset($_GET['checkout']) and in_array($_GET['checkout'], ['automatic', 'manual'], true))
			{
				$checkout = $_GET['checkout'];
			}

			$data = [
				"key"               => "rzp_live_J0ie3qfuigQhgx",
				"amount"            => $amount,
				"name"              => "BSG Garments",
				"description"       => "BSG Garments",
				"image"             => "https://s29.postimg.org/r6dj1g85z/daft_punk.jpg",
				"prefill"           => [
				"name"              => "BSG Garments",
				"email"             => "bsggarments@gmail.com",
				"contact"           => "9999999999",
				],
				"notes"             => [
				"address"           => "BSG Garments",
				"merchant_order_id" => "9999999999",
				],
				"theme"             => [
				"color"             => "#F37254"
				],
				"order_id"          => $razorpayOrderId,
			];

			if ($displayCurrency !== 'INR')
			{
				$data['display_currency']  = $displayCurrency;
				$data['display_amount']    = $displayAmount;
			}

			$json = json_encode($data);

			require("checkout/manual.php");
			}
		}else {
			$this->placeOrder(null,base64_encode(json_encode($post)),"cod");
		}
	}
	public function verifyPayment(){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}

		$success = true;

		$error = "Payment Failed";
		$post = $_POST['orderData'];
		if (empty($_POST['razorpay_payment_id']) === false)
		{
			//  TEST CREDENTIALS
			//$api = new Api("rzp_test_iB0k6Jnt813DDT", "AdAofG9q01poTeUsYwlT3caS");
			
			//  LIVE CREDENTIALS
			  $api = new Api("rzp_live_J0ie3qfuigQhgx", "tsVzZZEh3cZtCp1f6YHJf90c");

			try
			{
				// Please note that the razorpay order ID must
				// come from a trusted source (session here, but
				// could be database or something else)
				$attributes = array(
					'razorpay_order_id' => $this->session->userdata("razorpay_order_id"),
					'razorpay_payment_id' => $_POST['razorpay_payment_id'],
					'razorpay_signature' => $_POST['razorpay_signature']
				);

			$api->utility->verifyPaymentSignature($attributes);
			}
			catch(SignatureVerificationError $e)
			{
				$success = false;
				$error = 'Razorpay Error : ' . $e->getMessage();
			}
		}

		if ($success === true)
		{
			$this->placeOrder($_POST['razorpay_payment_id'],$post,"online");
		}
		else
		{
			$html = "<p>Your payment failed</p>
					<p>{$error}</p>";
		}
	}
	public function placeOrder($paymentId=null,$post,$paymentype){


	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		//  TEST CREDENTIALS
		//$api = new Api("rzp_test_iB0k6Jnt813DDT", "AdAofG9q01poTeUsYwlT3caS");
			
		//  LIVE CREDENTIALS
		  $api = new Api("rzp_live_J0ie3qfuigQhgx", "tsVzZZEh3cZtCp1f6YHJf90c");
		
		$post = json_decode(base64_decode($post),true);
		//var_dump($post);exit;
		$userId = $this->session->userdata('id');
		if ($paymentId!=null){
		$order = $api->payment->fetch($paymentId);
		$orderTransaction  = $order;
		foreach ($orderTransaction as $key=>$val){
			$orderTransactionData[$key] = $val;
		}
		}
		$orderData = array(
			'user_id' => $userId,
			'order_total' => base64_decode(base64_decode($post['price'])),
			'delivery_charge' => $post['delivery_charge'],
			'payment_method' => $paymentype,
			'payment_status' => isset($order['status']) ? $order['status']: null,
			'payment_id' => $paymentId,
			'payment_order_id' => isset($order['order_id']) ? $order['order_id']: null,
			'address_id' => $post['address'],
			'order_status' => Orders::$orderPlaced
		);
		$data = $this->vendor_model->get_single_data(
			array(
				'user_id' => $userId,
				'payment_id' => $paymentId,
				'payment_order_id' => isset($order['order_id']) ? $order['order_id']: null,
			),
		 'tbl_orders');
		  if ((empty($data) && $paymentype =="online") || $paymentype == "cod") {
		$payment_order_id = $this->vendor_model->insert_data($orderData,'tbl_orders');
		if(!empty($payment_order_id)) {
			$productData = json_decode(base64_decode(base64_decode($post['productData'])),true);
			
			if ($paymentId!=null){
			$transactionDataArray = array(
				'order_id' => $payment_order_id,
				'user_id' => $userId,
				'payment_id' => $paymentId,
				'payment_order_id' => $order['order_id'],
				'transaction_info' => json_encode($orderTransactionData),
			);
			$this->vendor_model->insert_data($transactionDataArray,'tbl_transactions');
			}
			$x=0;
			foreach ($productData as $productData) {
				$productInfo =  $this->vendor_model->get_single_data(
					array(
						'id' => $productData['productId'],
					),
				 'tbl_product_stock');
				 $statusInfo = [
					 Orders::$orderPlaced => date('d M Y h:i:s a'),
				 ];
				$productDataArray = array(
					'order_id' => $payment_order_id,
					'product_id' => $productData['productId'],
					'color' => $post['color'][$x],
					'size' => $post['size'][$x],
					'quantity' => $post['quantity'][$x],
					'product_info' => json_encode($productInfo),
					'order_product_status' => Orders::$orderPlaced,
					'status_info' => json_encode($statusInfo),
					'product_price' => $productInfo['price']
				);
				$this->vendor_model->insert_data($productDataArray,'tbl_order_products');
				$data = [
					'user_id' => $userId,
					'product_id' => $productData['productId']
				];
				$this->vendor_model->delete_data($data,'tbl_cart');
			$x++;}
		}
	 	}
	 	redirect('order_success');
	}

	public function order_success(){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$this->load->view('user/order_success');
	}

	public function my_orders()
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$data['cart'] = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		$data['scrolling_message'] = $this->vendor_model->get_single_data(array(), 'tbl_scrolling_message');
		$where = array('user_id'=>$this->session->userdata('id'));
		$data['orders'] = $this->vendor_model->get_data($where, 'tbl_orders');
		$this->load->view('user/my_orders',$data);
	}
	public function order_details($id)
	{
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$id = base64_decode($id);
		$where = array('tbl_orders.id'=>$id);
		$data['orders'] = $this->vendor_model->getOrderProducts($where);
		$this->load->view('user/order_details',$data);
	}

	public function trackOrder($id){
	    if($this->session->userdata("id")=="")
		{
			redirect('login');
		}
		$id = base64_decode($id);
		$where = array('id'=>$id);
		$data['orderData'] = $this->vendor_model->get_single_data($where,'tbl_order_products');
		$this->load->view('user/trackOrder',$data);
	}

	public function get_tags_suggestions($value='')
	{
		$keyword = strtolower($this->input->post('keyword'));
		if(!empty($keyword)){
			$query = $this->vendor_model->get_tags_suggestions(array('tags'=>$keyword));
			
			$output = '';
			$all_tags = '';
			if(!empty($query)) {
				foreach($query as $value){
					$all_tags .= $value['tags'].",";
				}
				$tags = explode(',', $all_tags);
				foreach(array_unique($tags) as $key) {
					if(strpos($key, $keyword) === 0){
						$output .= '<a class="pl-3 pr-3" onclick="myFunction(this);">'.$key.'</a><hr>';
					}
				}
			}
			echo $output;
		}
	}

	public function getColorImages()
	{
		$id = $this->input->post('id');
		$color = $this->input->post('color');
		if(!empty($id) && !empty($color)){
			$data['color'] = $this->vendor_model->get_data(array('product_id'=>$id,'color_name'=>$color), 'tbl_product_stock');
			$this->load->view('user/_getColors', $data);
		}
	}
	public function getColorSizes()
	{
		$id = $this->input->post('id');
		$color = $this->input->post('color');
		if(!empty($id) && !empty($color)){
			$data['size'] = $this->vendor_model->get_data(array('product_id'=>$id,'color_name'=>$color), 'tbl_product_stock');
			$this->load->view('user/_getColors', $data);
		}
	}
	public function getColorPrice()
	{
		$id = $this->input->post('id');
		if(!empty($id)){
			$data = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_product_stock');
			echo "₹ ".$data['price'];
		}
	}
	public function getCartItemsNum(){
		if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User"))
		{
			$count = $this->vendor_model->get_count(array('user_id'=>$this->session->userdata('id')), 'tbl_cart');
		} else {
			$count = 0;
		}
		echo $count;
	}
}