<?php include('header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Product Creation</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('add_product'); ?>" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="datatable table table-hover table-center mb-0">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Product Title</th>
										<th>Price Per Each item</th>
										<th>Category</th>
										<th class="text-right" style="width: 150px;">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $z=1; foreach ($products as $value) { ?>
									<tr>
										<td><?php echo $z; ?></td>
										<td><?php echo $value['product_title']; ?></td>
										<td><?php echo $value['product_price']; ?></td>
										<td><?php echo $value['category_name']; ?></td>
										<td class="text-right">
											<div class="actions">
												<a href="<?php echo base_url('edit_product'); ?>/<?php echo base64_encode($value['id']); ?>" class="btn btn-sm bg-success-light mr-2">
													<i class="fe fe-pencil"></i> Edit
												</a>
												<a onclick="loadColors(<?php echo $value['id']; ?>)" href="#" class="btn btn-sm bg-success-light mr-2">
													<i class="fe fe-pencil"></i> Edit Colors
												</a>
												<a onclick="addColors(<?php echo $value['id']; ?>)" href="#" class="btn btn-sm bg-success-light mr-2">
													<i class="fe fe-pencil"></i> Add Color
												</a>
												<a class="btn btn-sm bg-danger-light mr-2" href="<?php echo base_url('delete_product'); ?>/<?php echo base64_encode($value['id']); ?>">
													<i class="fe fe-trash"></i> Delete
												</a>
												<a class="btn btn-sm bg-success-light" href="<?php echo base_url('view_product'); ?>/<?php echo base64_encode($value['id']); ?>">
													 View More
												</a>
											</div>
										</td>
									</tr>
									<?php $z++;} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>

	</div>			
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Colors</h4>
      </div>
      <div class="modal-body" id="modelBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- /Page Wrapper -->

<?php include('footer.php'); ?>
<script>
function loadColors(id){
	$.ajax({
            url: "<?php echo base_url('listColors'); ?>",
            data: { 'id':id },
            type: 'POST',
            success: function (data) {
				$(".modal-title").html("Edit Color");
                $('#modelBody').html(data);
            }
        });
		$("#myModal").modal("show");
}
function addColors(id){
	$.ajax({
            url: "<?php echo base_url('listColors'); ?>",
            data: { 'id':id ,'type':'add'},
            type: 'POST',
            success: function (data) {
				$(".modal-title").html("Add Color");
                $('#modelBody').html(data);
            }
        });
		$("#myModal").modal("show");
}
</script>