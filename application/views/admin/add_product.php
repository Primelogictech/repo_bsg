<?php include('header.php'); ?>

<style type="text/css">
    .panel-default {
        border: 1px solid #dcdcdc;
        border-top: 1px solid #dcdcdc;
    }
    .panel {
        border-radius: 0px;
    }
    .panel {
        margin-bottom: 18px;
        background-color: #fff;
        /* border: 1px solid transparent;*/
        border-radius: 3px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
    }
    .panel-body {
        padding: 15px;
    }
    .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 15px;
        color: inherit;
    }
    .panel-heading {
        padding: 12px 15px;
        border-bottom: 1px solid transparent;
        border-top-right-radius: 2px;
        border-top-left-radius: 2px;
    }
    .nav-tabs {
        margin-bottom: 25px;
    }
    .nav-tabs {
        border-bottom: 1px solid #ddd;
    }
    .nav {
        margin-bottom: 0;
        padding-left: 0;
        list-style: none;
    }
    .nav-tabs > li {
        float: left;
        margin-bottom: -1px;
    }
    .nav > li {
        position: relative;
        display: block;
    }
    .nav-tabs > li.active > a,
    .nav-tabs > li.active > a:hover,
    .nav-tabs > li.active > a:focus {
        font-weight: bold;
        color: #333;
    }
    .nav-tabs > li.active > a,
    .nav-tabs > li.active > a:hover,
    .nav-tabs > li.active > a:focus {
        color: #555;
        background-color: #fff;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
        cursor: default;
    }
    .nav-tabs > li > a {
        color: #a5a5a5;
        border-radius: 2px 2px 0 0;
    }
    .nav-tabs > li > a {
        margin-right: 2px;
        line-height: 1.42857;
        border: 1px solid transparent;
        border-radius: 3px 3px 0 0;
    }
    .nav > li > a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }
    .panel-default .panel-heading {
        color: #4c4d5a;
        border-color: #dcdcdc;
        background: #f6f6f6;
        text-shadow: 0 -1px 0 rgba(50, 50, 50, 0);
    }
    .nav:before,
    .nav:after {
        content: " ";
        display: table;
    }
    .tabss {
        cursor: pointer;
    }
    .cke_bottom {
        padding: 0px !important;
        position: relative;
        border: 0px !important;
        background: none !important;
    }
    .cke_resizer {
        border: 0px !important;
    }
</style>


<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-3">
                    <h4 class="page-title">Product Creation</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-3">
                    <div class="float-right">
                       <a href="<?php echo base_url('all_products'); ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                   </div>
               </div>
           </div>
       </div>
       <!-- /Page Header -->

       <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Product</h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li class="active tabss" data-id="general"><a>General</a></li>
                        <li class="tabss" data-id="pricing"><a>Pricing</a></li>
                        <li class="tabss" data-id="links"><a>Links</a></li>
                        <li class="tabss" data-id="images"><a>Images and Videos</a></li>
                        <li class="tabss" data-id="miscellaneous"><a>Miscellaneous</a></li>
                    </ul>
                </div>
                
                <form action="<?php echo base_url('upload_product') ?>" method="post" enctype="multipart/form-data">
                    <div id="general" class="tab">
                        <div class="card-body">
                            <input type="number" name="id" value="<?php if(isset($q)){ echo $q['id']; } ?>" hidden>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Product Title<span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="product_title" id="product_title" value="<?php if(isset($q)){ echo $q['product_title']; } ?>" class="form-control" placeholder=" Enter Name of the Product" required>
                                </div>
                            </div>
                            <div class="form-group row" id="product_title_error" style="display: none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-8 text-danger">Required</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Short Description (Which comes under product Title) <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <textarea name="short_description" id="short_description" rows="5" cols="5" class="form-control" placeholder="Not more than 80 charactors" required><?php if(isset($q)){ echo $q['short_description']; }else{ echo $this->session->userdata('short_description'); } ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row" id="short_description_error" style="display: none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-8 text-danger">Required</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Description <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <textarea name="description" id="description" rows="5" cols="5" class="form-control" required><?php if(isset($q)){ echo $q['description']; }else{ echo $this->session->userdata('description'); } ?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Sleeve Length <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                <select class="form-control" name="sleeves" id="sleeves" class="filters-styles">
                                    <?php $sleeves = Orders::getSleeves(); ?>
                                    <?php foreach($sleeves as $key=>$val){ ?>
                                        <option <?php if(isset($q)){ if($key==$q['sleeve_length']){echo "selected";} } ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Style / Type <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                <select class="form-control" name="type" id="type" class="filters-styles">
                                <?php $sleeves = Orders::getType(); ?>
                                    <?php foreach($sleeves as $key=>$val){ ?>
                                        <option <?php if(isset($q)){ if($key==$q['type']){echo "selected";} } ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Design <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                <select class="form-control" name="design" id="design" class="filters-styles">
                                <?php $sleeves = Orders::getDesign(); ?>
                                    <?php foreach($sleeves as $key=>$val){ ?>
                                        <option <?php if(isset($q)){ if($key==$q['design']){echo "selected";} } ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row" id="description_error" style="display: none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-8 text-danger">Required</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Choose Main Category<span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" id="main_category_id" name="main_category_id" required>
                                        <option value="">-- Select --</option>
                                        <?php foreach ($main_categories as $key) { ?>
                                        <option value="<?php echo $key['id']; ?>" <?php if(isset($q)){  if($q['main_category_id'] == $key['id']){ echo "selected"; }}else{ if($this->session->userdata('main_category_id') == $key['id']){ echo "selected"; }} ?>><?php echo $key['main_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="main_category_id_error" style="display: none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-8 text-danger">Required</div>
                            </div>
                            <?php if(!isset($q)){ ?>
                            <div class="form-group row">
                                <label class="col-form-label col-md-3">Select Category<span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control" id="category_id" name="category_id" required>
    									<option value="">-- Select --</option>
    								</select>
                                </div>
                            </div>
                            <div class="form-group row" id="category_id_error" style="display: none;">
                                <div class="col-md-3"></div>
                                <div class="col-md-8 text-danger">Required</div>
                            </div>
							<!-- <div class="form-group row">
                                <?php if(isset($product_attributes)){ foreach ($product_attributes as  $key) { ?>
                                <div class="removable row col-md-11">
                                    <label class="col-form-label col-md-1"> <spanclass="mandatory">*</span></label>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Size:</label><input type="text" name="size[]" value="<?php echo $key['size']; ?>" class="form-control" placeholder="Size">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Color:</label><input type="text" name="color[]" value="<?php echo $key['color']; ?>" class="form-control" placeholder="Color">
                                    </div>
                                                            </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Quantity:</label><input type="text" name="quantity[]" value="<?php echo $key['quantity']; ?>" class="form-control" placeholder="Quantity">
                                        </div>
                                    </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                            <label style="opacity: 0">add</label><input type="button" class="remove btn form-control" value="X">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="opacity: 0">add</label>
                                        <input type="button" class="add-new-btn btn form-control" value="+">
                                    </div>
                                </div>          
                                <?php }else{ ?>
                            
                                <label class="col-form-label col-md-3"> <span class="mandatory">*</span></label>
                                <div class="col-md-8">
                                    <div class="row" id="attributes">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div id="appendAttr"></div> -->
                        <?php if(isset($product_attributes) && !empty($product_attributes)){ ?>
                        <?php foreach ($product_attributes as $key) { ?>
                        <div class="form-group row remove_attributes_block">
                            <label class="col-form-label col-md-3"></label>
                            <div class="col-md-9 row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Size</label>
                                        <select class="form-control" id="size" name="size[]">
                                            <option value="">-- Select --</option>
                                            <option value="S" <?php if($key['size']=='S'){ echo "selected"; } ?>>S</option>
                                            <option value="M" <?php if($key['size']=='M'){ echo "selected"; } ?>>M</option>
                                            <option value="L" <?php if($key['size']=='L'){ echo "selected"; } ?>>L</option>
                                            <option value="XL" <?php if($key['size']=='XL'){ echo "selected"; } ?>>XL</option>
                                            <option value="XXL" <?php if($key['size']=='XXL'){ echo "selected"; } ?>>XXL</option>
                                            <option value="3XL" <?php if($key['size']=='3XL'){ echo "selected"; } ?>>3XL</option>
                                            <option value="4XL" <?php if($key['size']=='4XL'){ echo "selected"; } ?>>4XL</option>
                                            <option value="5XL" <?php if($key['size']=='5XL'){ echo "selected"; } ?>>5XL</option>
                                            <option value="28" <?php if($key['size']=='28'){ echo "selected"; } ?>>28</option>
                                            <option value="30" <?php if($key['size']=='30'){ echo "selected"; } ?>>30</option>
                                            <option value="32" <?php if($key['size']=='32'){ echo "selected"; } ?>>32</option>
                                            <option value="34" <?php if($key['size']=='34'){ echo "selected"; } ?>>34</option>
                                            <option value="36" <?php if($key['size']=='36'){ echo "selected"; } ?>>36</option>
                                            <option value="38" <?php if($key['size']=='38'){ echo "selected"; } ?>>38</option>
                                            <option value="40" <?php if($key['size']=='40'){ echo "selected"; } ?>>40</option>
                                            <option value="42" <?php if($key['size']=='42'){ echo "selected"; } ?>>42</option>
                                            <option value="44" <?php if($key['size']=='44'){ echo "selected"; } ?>>44</option>
                                            <option value="46" <?php if($key['size']=='46'){ echo "selected"; } ?>>46</option>
                                            <option value="48" <?php if($key['size']=='48'){ echo "selected"; } ?>>48</option>
                                            <option value="50" <?php if($key['size']=='50'){ echo "selected"; } ?>>50</option>
                                            <option value="70" <?php if($key['size']=='70'){ echo "selected"; } ?>>70</option>
                                            <option value="75" <?php if($key['size']=='75'){ echo "selected"; } ?>>75</option>
                                            <option value="80" <?php if($key['size']=='80'){ echo "selected"; } ?>>80</option>
                                            <option value="85" <?php if($key['size']=='85'){ echo "selected"; } ?>>85</option>
                                            <option value="90" <?php if($key['size']=='90'){ echo "selected"; } ?>>90</option>
                                            <option value="95" <?php if($key['size']=='95'){ echo "selected"; } ?>>95</option>
                                            <option value="100" <?php if($key['size']=='100'){ echo "selected"; } ?>>100</option>
                                            <option value="105" <?php if($key['size']=='105'){ echo "selected"; } ?>>105</option>
                                            <option value="110" <?php if($key['size']=='110'){ echo "selected"; } ?>>110</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Color Name</label>
                                        <input type="text" name="colorName[]" class="form-control" value="<?php echo $key['color_name']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Color</label>
                                        <input type="file" name="color[]" class="form-control">
                                        <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key['color']; ?>" alt="color" width="50px" height="50px">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <input type="number" name="quantity[]" value="<?php echo $key['quantity']; ?>" min="1" class="form-control" placeholder="Quantity">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input type="number" value="<?php echo $key['price']; ?>" name="productPrice[]" min="1" class="form-control" placeholder="Price">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                <input id="rowCountinput" type="hidden" value="1">
                                    <div class="form-group">
                                        <label>Images</label>
                                        <input multiple class="form-control" type="file" accept="image/*" name="images1[]" <?php if(isset($q) && !empty($q['id'])){}else{ echo "required"; } ?>>
                                        <input name="rowCount[]" type="hidden" value="1">
                                    </div>
                                </div>
                                
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="opacity: 0">add</label>
                                        <input type="button" class="remove_attributes btn btn-danger form-control" value="X">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group row">
                            <div class="col-md-10"></div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label style="opacity: 0">add</label>
                                    <input type="button" class="add_attributes btn btn-primary form-control" value="+">
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Attributes <span class="mandatory">*</span></label>
                            <div class="col-md-9 row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Size</label>
                                        <select class="form-control" id="size" name="size[]">
                                            <option value="">-- Select --</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="XL">XL</option>
                                            <option value="XXL">XXL</option>
                                            <option value="3XL">3XL</option>
                                            <option value="4XL">4XL</option>
                                            <option value="5XL">5XL</option>
                                            <option value="28">28</option>
                                            <option value="30">30</option>
                                            <option value="32">32</option>
                                            <option value="34">34</option>
                                            <option value="36">36</option>
                                            <option value="38">38</option>
                                            <option value="40">40</option>
                                            <option value="42">42</option>
                                            <option value="44">44</option>
                                            <option value="46">46</option>
                                            <option value="48">48</option>
                                            <option value="50">50</option>
                                            <option value="70">70</option>
                                            <option value="75">75</option>
                                            <option value="80">80</option>
                                            <option value="85">85</option>
                                            <option value="90">90</option>
                                            <option value="95">95</option>
                                            <option value="100">100</option>
                                            <option value="105">105</option>
                                            <option value="110">110</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Color Name</label>
                                        <input type="text" name="colorName[]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Color</label>
                                        <input type="file" name="color[]" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Quantity</label>
                                        <input type="number" name="quantity[]" min="1" class="form-control" placeholder="Quantity">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input type="number" name="productPrice[]" min="1" class="form-control" placeholder="Price">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                <input id="rowCountinput" type="hidden" value="1">
                                    <div class="form-group">
                                        <label>Images</label>
                                        <input multiple class="form-control" type="file" accept="image/*" name="images1[]" <?php if(isset($q) && !empty($q['id'])){}else{ echo "required"; } ?>>
                                        <input name="rowCount[]" type="hidden" value="1">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="opacity: 0">add</label>
                                        <input type="button" class="add_attributes btn btn-primary form-control" value="+">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div id="appendAttributes"></div>
                            <?php } ?>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Estimated Delivery Days <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="number" name="estimated_delivery_days" id="estimated_delivery_days" min="1" value="<?php if(isset($q)){ echo $q['estimated_delivery_days']; }else{ echo $this->session->userdata('estimated_delivery_days'); } ?>" class="form-control" placeholder="Enter Number of days" required>
                            </div>
                        </div>
                        <div class="form-group row" id="estimated_delivery_days_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Returnable<span class="mandatory">*</span></label>
                            <div class="col-md-8 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="returnable" value="1" <?php if(isset($q)){ if($q['returnable']=='1'){ echo "checked"; }}else{ if($this->session->userdata('returnable') != ""){ if($this->session->userdata('returnable') == 1){ echo "checked"; }}} ?> required> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="returnable" value="0" <?php if(isset($q)){ if($q['returnable']=='0'){ echo "checked"; }}else{ if($this->session->userdata('returnable') != ""){ if($this->session->userdata('returnable') == 0){ echo "checked"; }}} ?> required> No </label>
                                </div>
                                <div class="form-group col-md-8 row" id="returnable_days_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If Yes, How many Days <span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" name="returnable_days" min="1" <?php if((isset($q) && !empty($q['returnable_days'])) || !empty($this->session->userdata('returnable_days'))) { ?>value="<?php if(isset($q)){ echo $q['returnable_days']; }else{ echo $this->session->userdata('returnable_days'); } ?>"<?php } ?> class="form-control" placeholder="Enter Number of days">
                                    </div>
                                </div>
                                <div class="form-group col-md-8 row" id="not_returnable_reason_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If No, Reason <span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" name="not_returnable_reason" value="<?php if(isset($q)){ echo $q['not_returnable_reason']; }else{ echo $this->session->userdata('not_returnable_reason'); } ?>" class="form-control" placeholder="Reason">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="returnable_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <label class="col-form-label col-md-3">Payment Methods<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" name="payment_methods" value="<?php if(isset($q)){ echo $q['payment_methods']; }elseif(!empty($this->session->userdata('payment_methods'))){ echo $this->session->userdata('payment_methods'); }else{ echo "All"; } ?>" class="form-control" placeholder="Credit/Debit card, Net Banking, UPI, EMI Etc" required>
                            </div>
                        </div>
                        <div class="form-group row" id="payment_methods_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">COD Available<span class="mandatory">*</span></label>
                            <div class="col-md-8 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="cod" value="1" <?php if(isset($q)){ if($q['cod']=='1'){ echo "checked"; }}else{ if($this->session->userdata('cod') != ""){ if($this->session->userdata('cod') == 1){ echo "checked"; }}} ?> required> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="cod" value="0" <?php if(isset($q)){ if($q['cod']=='0'){ echo "checked"; }}else{ if($this->session->userdata('cod') != ""){ if($this->session->userdata('cod') == 0){ echo "checked"; }}} ?> required> No </label>
                                </div>
                                <div class="form-group col-md-8 row" id="no_cod_reason_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If No, Reason <span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" name="no_cod_reason" id="no_cod_reason" value="<?php if(isset($q)){ echo $q['no_cod_reason']; }else{ echo $this->session->userdata('no_cod_reason'); } ?>" class="form-control" placeholder="Reason">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="cod_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">International Shipping Available<span class="mandatory">*</span></label>
                            <div class="col-md-8 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="international_shipping" value="1" <?php if(isset($q)){ if($q['international_shipping']=='1'){ echo "checked"; }}else{ if($this->session->userdata('international_shipping') != ""){ if($this->session->userdata('international_shipping') == 1){ echo "checked"; }}} ?> required> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="international_shipping" value="0" <?php if(isset($q)){ if($q['international_shipping']=='0'){ echo "checked"; }}else{ if($this->session->userdata('international_shipping') != ""){ if($this->session->userdata('international_shipping') == 0){ echo "checked"; }}} ?> required> No </label>
                                </div>
                                <div class="form-group col-md-8 row" id="international_shipping_countries_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If Yes, Select Countries<span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="international_shipping_countries" id="international_shipping_countries">
                                            <option value="">-- Select --</option>
                                            <option value="India" <?php if(isset($q)){ if($q['international_shipping_countries']=='India'){ echo "selected"; }}else{ if($this->session->userdata('international_shipping_countries') == 'India'){ echo "selected"; }} ?>>India</option>
                                            <option value="USA" <?php if(isset($q)){ if($q['international_shipping_countries']=='USA'){ echo "selected"; }}else{ if($this->session->userdata('international_shipping_countries') == 'USA'){ echo "selected"; }} ?>>USA</option>
                                            <option value="Australia" <?php if(isset($q)){ if($q['international_shipping_countries']=='Australia'){ echo "selected"; }}else{ if($this->session->userdata('international_shipping_countries') == 'Australia'){ echo "selected"; }} ?>>Australia</option>
                                            <option value="Africa" <?php if(isset($q)){ if($q['international_shipping_countries']=='Africa'){ echo "selected"; }}else{ if($this->session->userdata('international_shipping_countries') == 'Africa'){ echo "selected"; }} ?>>Africa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-8 row" id="no_international_shipping_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If No, Reason <span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" name="no_international_shipping_reason" id="no_international_shipping_reason" value="<?php if(isset($q)){ echo $q['no_international_shipping_reason']; }else{ echo $this->session->userdata('no_cod_reason'); } ?>" class="form-control" placeholder="Reason">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="international_shipping_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div> -->
                        
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-4 text-center">
                                <a class="btn btn-primary" data-id="pricing" id="general_next">Next</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="pricing" class="tab" style="display: none;">
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Price Per each Item <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="number" name="product_price" id="product_price" min="0" max="1000000" value="<?php if(isset($q)){ echo $q['product_price']; } ?>" class="form-control" placeholder="Enter Price" required>
                            </div>
                        </div>
                        <div class="form-group row" id="product_price_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Price Including Tax?<span class="mandatory">*</span></label>
                            <div class="col-md-9 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="price_including_tax" value="1" <?php if(isset($q)){ if($q['price_including_tax']=='1'){ echo "checked"; }}else{ if($this->session->userdata('price_including_tax') != ""){ if($this->session->userdata('price_including_tax') == 1){ echo "checked"; }}} ?>> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="price_including_tax" value="0" <?php if(isset($q)){ if($q['price_including_tax']=='0'){ echo "checked"; }}else{ if($this->session->userdata('price_including_tax') != ""){ if($this->session->userdata('price_including_tax') == 0){ echo "checked"; }}} ?>> No </label>
                                </div>
                                <div class="form-group col-md-8 row" id="tax_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If No, Select Tax<span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="tax" id="tax">
                                            <option value="">-- Select --</option>
                                            <?php foreach($tax as $key){ ?>
                                            <option value="<?php echo $key['percentage']; ?>" <?php if(isset($q)){  if($q['tax'] == $key['percentage']){ echo "selected"; }}else{ if($this->session->userdata('tax') == $key['percentage']){ echo "selected"; }} ?>><?php echo $key['name']; ?> (<?php echo $key['percentage']; ?>%)</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="tax_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Discount Applicable<span class="mandatory">*</span></label>
                            <div class="col-md-9 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="discount_applicable" value="1" <?php if(isset($q)){ if($q['discount_applicable']=='1'){ echo "checked"; }}else{ if($this->session->userdata('discount_applicable') != ""){ if($this->session->userdata('discount_applicable') == 1){ echo "checked"; }}} ?>> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="discount_applicable" value="0" <?php if(isset($q)){ if($q['discount_applicable']=='0'){ echo "checked"; }}else{ if($this->session->userdata('discount_applicable') != ""){ if($this->session->userdata('discount_applicable') == 0){ echo "checked"; }}} ?>> No </label>
                                </div>
                                <div class="form-group col-md-8 row" id="discount_applicable_if_products_block" style="display: none;">
                                    <label class="col-form-label col-md-6">If Yes, On purchase of how many items discount to be applied <span class="mandatory">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" name="discount_applicable_if_products" min="1" id="discount_applicable_if_products" <?php if((isset($q) && !empty($q['discount_applicable_if_products'])) || !empty($this->session->userdata('discount_applicable_if_products'))) { ?>value="<?php if(isset($q)){ echo $q['discount_applicable_if_products']; }else{ echo $this->session->userdata('discount_applicable_if_products'); } ?>"<?php } ?> class="form-control" placeholder="Enter Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="discount_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row" id="discount_percent_block" style="display: none;">
                            <label class="col-form-label col-md-3">Enter Discount Percentage <span class="mandatory">* (minimum value is 1)</span></label>
                            <div class="col-md-9">
                                <input type="number" name="discount_percent" id="discount_percent" min="1" max="100" <?php if((isset($q) && !empty($q['discount_percent'])) || !empty($this->session->userdata('discount_percent'))) { ?>value="<?php if(isset($q)){ echo $q['discount_percent']; }else{ echo $this->session->userdata('discount_percent'); } ?>"<?php } ?> class="form-control" placeholder="Enter percentage" />
                            </div>
                        </div>
                        <div class="form-group row" id="discount_percent_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger" id="discount_percent_error_msg"></div>
                        </div>
                        <div class="form-group row" id="discount_validity_block" style="display: none;">
                            <label class="col-form-label col-md-3">Discount Validity<span class="mandatory">(Optional)</span></label>
                            <div class="col-md-9">
                                <input type="text" id="discount_validity" name="discount_validity" <?php if(isset($q) && ($q['discount_validity'] != '0000-00-00')) { ?>value="<?php if(isset($q)){ echo date("d/m/Y", strtotime($q['discount_validity'])); }else{ echo date("d/m/Y", strtotime($this->session->userdata('discount_validity'))); } ?>"<?php } ?> class="form-control" placeholder="dd/mm/yyyy" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row" id="discount_validity_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Delivery Charge<span class="mandatory">*</span></label>
                            <div class="col-md-9 row">
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="delivery_charge" value="1" <?php if(isset($q)){ if($q['delivery_charge']=='1'){ echo "checked"; }}else{ if($this->session->userdata('delivery_charge') != ""){ if($this->session->userdata('delivery_charge') == 1){ echo "checked"; }}} ?>> Yes </label>
                                </div>
                                <div class="radio col-md-2">
                                    <label> <input type="radio" name="delivery_charge" value="0" <?php if(isset($q)){ if($q['delivery_charge']=='0'){ echo "checked"; }}else{ if($this->session->userdata('delivery_charge') != ""){ if($this->session->userdata('delivery_charge') == 0){ echo "checked"; }}} ?>> No </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="delivery_charge_error" style="display: none;">
                            <div class="col-md-3"></div>
                            <div class="col-md-8 text-danger">Required</div>
                        </div>
                    
                   
                    <div class="form-group row" id="local_delivery_charge_block" style="display: none;">
                        <label class="col-form-label col-md-3">Delivery Charge <span class="mandatory">*</span></label>
                        <div class="col-md-9">
                            <input type="number" id="local_delivery_charge_amount" name="local_delivery_charge_amount" min="0" <?php if((isset($q) && !empty($q['local_delivery_charge_amount'])) || !empty($this->session->userdata('local_delivery_charge_amount'))) { ?>value="<?php if(isset($q)){ echo $q['local_delivery_charge_amount']; }else{ echo $this->session->userdata('local_delivery_charge_amount'); } ?>"<?php } ?> class="form-control" placeholder="Enter Amount">
                        </div>
                    </div>
                   
                    <!-- <div class="form-group row">
                        <label class="col-form-label col-md-3">Reward points per purchase of each item</label>
                        <div class="col-md-9">
                            <input type="text" name="reward_points_per_item" value="<?php if(isset($q)){ echo $q['reward_points_per_item']; }else{ echo $this->session->userdata('reward_points_per_item'); } ?>" class="form-control" placeholder="Enter Reward points" />
                        </div>
                    </div> -->
                    <div class="form-group row" id="local_delivery_charge_error" style="display: none;">
                        <div class="col-md-3"></div>
                        <div class="col-md-8 text-danger">Required</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <a class="btn btn-primary" data-id="general" onclick="myFunction(this);">Previous</a>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <a class="btn btn-primary float-right" data-id="links" id="pricing_next">Next</a>
                        </div>
                    </div>
                </div>
            </div>


            <div id="links" class="tab" style="display: none;">
                <div class="card-body">
                    <!-- <div class="form-group row">
                        <label class="col-form-label col-md-3">Related Products</label>
                        <div class="col-md-9">
                            <select class="form-control" name="related_products">
                                <option value="">-- Select --</option>
                                <option value="Option 1" <?php if(isset($q)){ if($q['related_products']=='Option 1'){ echo "selected"; }} ?>>Option 1</option>
                                <option value="Option 2" <?php if(isset($q)){ if($q['related_products']=='Option 2'){ echo "selected"; }} ?>>Option 2</option>
                                <option value="Option 3" <?php if(isset($q)){ if($q['related_products']=='Option 3'){ echo "selected"; }} ?>>Option 3</option>
                                <option value="Option 4" <?php if(isset($q)){ if($q['related_products']=='Option 4'){ echo "selected"; }} ?>>Option 4</option>
                                <option value="Option 5" <?php if(isset($q)){ if($q['related_products']=='Option 5'){ echo "selected"; }} ?>>Option 5</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Banner Image<span class="mandatory">* (upload image with 333x493 pixels only. Image is shown in Product list page)</span></label>
                        <div class="col-md-9">
                            <input class="form-control" type="file" accept="image/*" name="banner_image" <?php if(isset($q) && !empty($q['id'])){}else{ echo "required"; } ?>>
                        </div>
                        <?php if(isset($q) && !empty($q['banner_image'])){ ?>
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <img src='<?php echo base_url(); ?>uploads/images/product_images/<?php echo $q['banner_image']; ?>' width="50px">
                        </div>
                        <?php } ?>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <a class="btn btn-primary" data-id="pricing" onclick="myFunction(this);">Previous</a>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <a class="btn btn-primary float-right" data-id="images" onclick="myFunction(this);">Next</a>
                        </div>
                    </div>
                </div>
            </div>


            <div id="images" class="tab" style="display: none;">
                <div class="card-body">
                    <!-- <div class="form-group row images_div">
                        <label class="col-form-label col-md-3">Upload Images</label>
                        <div class="col-md-8">
                            <input class="form-control" type="file" accept="image/*" name="images[]">
                        </div>
                        <div class="col-md-1"><a class="btn btn-primary" id="add_another_image">+</a></div>
                    </div>
                    <div id="add_another_image_div"></div>
                    <?php if(isset($q) && !empty($q['images'])){ ?>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <?php $array = explode(',', $q['images']); foreach ($array as $key) { if(!empty($key)){ ?>
                                <img src='<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>' width="50px">
                                <a href="<?php echo base_url('delete_product_images'); ?>/<?php echo $q['id']; ?>/<?php echo $key; ?>" style="margin-right: 25px;"><i class="fe fe-minus text-danger" style="position: absolute;"></i></a>
                            <?php }} ?>
                        </div>
                    </div>
                    <?php } ?> -->
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Upload Video <span class="mandatory">(Youtube embed link only)</span></label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="videos" value="<?php if(isset($q)){ echo $q['videos']; } ?>" placeholder="Enter Youtube Embed Link">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <a class="btn btn-primary" data-id="links" onclick="myFunction(this);">Previous</a>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <a class="btn btn-primary float-right" data-id="miscellaneous" onclick="myFunction(this);">Next</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="miscellaneous" class="tab" style="display: none;">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Our Advantages</label>
                        <div class="col-md-9">
                            <?php if(isset($our_advantages_data) && !empty($our_advantages_data['images'])){
                            $array = explode(',', $our_advantages_data['images']); foreach ($array as $key) { if(!empty($key)){ ?>
                                <input type="checkbox" name="our_advantages[]" value="<?php echo $key; ?>" <?php if(isset($q['our_advantages']) && !empty($q['our_advantages'])){ $check_array = explode(',', $q['our_advantages']); if(in_array($key, $check_array)){ echo "checked"; }} ?>><img width="100" src="<?php echo base_url(); ?>uploads/images/our_advantages_images/<?php echo $key; ?>">
                            <?php }}} ?>
                        </div>
                    </div>
                    <div class="form-group row bank_offers_div">
                        <label class="col-form-label col-md-3">Bank Offers</label>
                        <div class="col-md-8">
                            <input class="form-control" type="file" accept="image/*" name="bank_offers[]">
                        </div>
                        <div class="col-md-1"><a class="btn btn-primary" id="add_another_bank_offers">+</a></div>
                    </div>
                    <div id="add_another_bank_offers_div"></div>
                    <?php if(isset($q) && !empty($q['bank_offers'])){ ?>
                    <div class="form-group row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <?php $array = explode(',', $q['bank_offers']); foreach ($array as $key) { if(!empty($key)){ ?>
                                <img src='<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>' width="50px">
                                <a href="<?php echo base_url('delete_product_bank_offers'); ?>/<?php echo $q['id']; ?>/<?php echo $key; ?>" style="margin-right: 25px;"><i class="fe fe-minus text-danger" style="position: absolute;"></i></a>
                            <?php }} ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Tags<span class="mandatory">* (keywords for searching product)</span></label>
                        <div class="col-md-9" id="tags">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <a class="btn btn-primary" data-id="images" onclick="myFunction(this);">Previous</a>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
</div>
</div>
</div>
<!-- /Page Wrapper -->
</div>
<!-- /Main Wrapper -->

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- VALIDATION SCRIPTS START -->
<script type="text/javascript">
    $(document).ready(function () {
        $("input[name=returnable]").on("change", function () {
            var id = $(this).val();
            if(id == 1){
                $('#returnable_days_block').show();
                $('#not_returnable_reason_block').hide();

                $("input[name=returnable_days]").prop('required', true);
                $("input[name=not_returnable_reason]").prop('required', false);

                $("input[name=not_returnable_reason]").val('');
            }else{
                $('#returnable_days_block').hide();
                $('#not_returnable_reason_block').show();

                $("input[name=returnable_days]").prop('required', false);
                $("input[name=not_returnable_reason]").prop('required', true);

                $("input[name=returnable_days]").val('');
            }
        });

        /*$("input[name=cod]").on("change", function () {
            var id = $(this).val();
            if(id == 0){
                $('#no_cod_reason_block').show();
                $("#no_cod_reason").prop('required', true);
            }else{
                $('#no_cod_reason_block').hide();
                $("#no_cod_reason").prop('required', false);

                $("#no_cod_reason").val('');
            }
        });*/

        /*$("input[name=international_shipping]").on("change", function () {
            var id = $(this).val();
            if(id == 1){
                $('#international_shipping_countries_block').show();
                $('#no_international_shipping_block').hide();

                $("#international_shipping_countries").prop('required', true);
                $("#no_international_shipping_reason").prop('required', false);

                $("#no_international_shipping_reason").val('');
            }else{
                $('#international_shipping_countries_block').hide();
                $('#no_international_shipping_block').show();

                $("#international_shipping_countries").prop('required', false);
                $("#no_international_shipping_reason").prop('required', true);

                $("#international_shipping_countries").val('');
            }
        });*/

        $("input[name=price_including_tax]").on("change", function () {
            var id = $(this).val();
            if(id == 0){
                $('#tax_block').show();
                $("#tax").prop('required', true);
            }else{
                $('#tax_block').hide();
                $("#tax").prop('required', false);

                $("#tax").val('');
            }
        });

        $("input[name=discount_applicable]").on("change", function () {
            var id = $(this).val();
            if(id == 1){
                $('#discount_applicable_if_products_block').show();
                $('#discount_percent_block').show();
                $('#discount_validity_block').show();

                $("#discount_applicable_if_products").prop('required', true);
                $("#discount_percent").prop('required', true);
                // $("#discount_validity").prop('required', true);
            }else{
                $('#discount_applicable_if_products_block').hide();
                $('#discount_percent_block').hide();
                $('#discount_validity_block').hide();
                
                $("#discount_applicable_if_products").prop('required', false);
                $("#discount_percent").prop('required', false);
                // $("#discount_validity").prop('required', false);

                $("#discount_applicable_if_products").val('');
                $("#discount_percent").val('');
                $("#discount_validity").val('');
            }
        });

        $("input[name=delivery_charge]").on("change", function () {
            var id = $(this).val();
            if(id == 1){
                
                $('#local_delivery_charge_block').show();
                

               
                $("#local_delivery_charge_amount").prop('required', true);
                
            }else{
                
                $('#local_delivery_charge_block').hide();
               

                
                $("#local_delivery_charge_amount").prop('required', false);
               
                $("#local_delivery_charge_amount").val('');
                
            }
        });

        $("#general_next").on("click", function () {
            if(!$('#product_title').val()){
                $('#product_title_error').show();
                $('#product_title').focus();
            }else{
                $('#product_title_error').hide();
            }
            if(!$('#short_description').val()){
                $('#short_description_error').show();
                $('#short_description').focus();
            }else{
                $('#short_description_error').hide();
            }
            if(!CKEDITOR.instances['description'].getData()){
                $('#description_error').show();
            }else{
                $('#description_error').hide();
            }
            if(!$('#main_category_id').val()){
                $('#main_category_id_error').show();
                $('#main_category_id').focus();
            }else{
                $('#main_category_id_error').hide();
            }
            if(!$('#category_id').val()){
                $('#category_id_error').show();
                $('#category_id').focus();
            }else{
                $('#category_id_error').hide();
            }
            if(!$('#estimated_delivery_days').val()){
                $('#estimated_delivery_days_error').show();
                $('#estimated_delivery_days').focus();
            }else{
                $('#estimated_delivery_days_error').hide();
            }
            if(!$("input[name=returnable]:checked").val()){
                $('#returnable_error').show();
            }else{
                $('#returnable_error').hide();
            }
            if($("input[name=returnable]:checked").val() == "1"){
                if(!$("input[name=returnable_days]").val()){
                    $('#returnable_error').show();
                    $("input[name=returnable_days]").focus();
                }else{
                    $('#returnable_error').hide();
                }
            }
            if($("input[name=returnable]:checked").val() == "0"){
                if(!$("input[name=not_returnable_reason]").val()){
                    $('#returnable_error').show();
                    $("input[name=not_returnable_reason]").focus();
                }else{
                    $('#returnable_error').hide();
                }
            }
            if(!$("input[name=cod]:checked").val()){
                $('#cod_error').show();
            }else{
                $('#cod_error').hide();
            }
            /*if($("input[name=cod]:checked").val() == "0"){
                if(!$("#no_cod_reason").val()){
                    $('#cod_error').show();
                    $("#no_cod_reason").focus();
                }else{
                    $('#cod_error').hide();
                }
            }*/
            /*if(!$("input[name=international_shipping]:checked").val()){
                $('#international_shipping_error').show();
            }else{
                $('#international_shipping_error').hide();
            }
            if($("input[name=international_shipping]:checked").val() == "1"){
                if(!$("#international_shipping_countries").val()){
                    $('#international_shipping_error').show();
                    $("#international_shipping_countries").focus();
                }else{
                    $('#international_shipping_error').hide();
                }
            }else{
                if(!$("#no_international_shipping_reason").val()){
                    $('#international_shipping_error').show();
                    $("#no_international_shipping_reason").focus();
                }else{
                    $('#international_shipping_error').hide();
                }
            }*/

            if(!$('#product_title').val()){
                $('#product_title').focus();
            }else if(!$('#short_description').val()){
                $('#short_description').focus();
            }else if(!CKEDITOR.instances['description'].getData()){
                $('#description').focus();
            }else if(!$('#main_category_id').val()){
                $('#main_category_id').focus();
            }else if(!$('#estimated_delivery_days').val()){
                $('#estimated_delivery_days').focus();
            }else if(!($('#estimated_delivery_days').val() > 0)){
                $('#estimated_delivery_days').focus();
            }else if(!$("input[name=returnable]:checked").val()){
                $("input[name=returnable]").focus();
            }else if($("input[name=returnable]:checked").val() == "1" && !$("input[name=returnable_days]").val()){
                $("input[name=returnable_days]").focus();
            }else if($("input[name=returnable]:checked").val() == "0" && !$("input[name=not_returnable_reason]").val()){
                $("input[name=not_returnable_reason]").focus();
            }else{
                var id = $(this).data('id');
                $('li[data-id='+id+']').click();
            }
        });

        $("#pricing_next").on("click", function () {
            if(!$('#product_price').val()){
                $('#product_price_error').show();
                $("#product_price").focus();
            }else{
                $('#product_price_error').hide();
            }
            if(!$("input[name=price_including_tax]:checked").val()){
                $('#tax_error').show();
                $("input[name=price_including_tax]").focus();
            }else{
                $('#tax_error').hide();
            }
            if($("input[name=price_including_tax]:checked").val() == "0"){
                if(!$("#tax").val()){
                    $('#tax_error').show();
                    $("#tax").focus();
                }else{
                    $('#tax_error').hide();
                }
            }
            if(!$("input[name=discount_applicable]:checked").val()){
                $('#discount_error').show();
            }else{
                $('#discount_error').hide();
            }
            if($("input[name=discount_applicable]:checked").val() == "1"){
                if(!$("#discount_applicable_if_products").val()){
                    $('#discount_error').show();
                    $("#discount_applicable_if_products").focus();
                }else{
                    $('#discount_error').hide();
                }
                if(!$("#discount_percent").val()){
                    $('#discount_percent_error_msg').html('Required');
                    $('#discount_percent_error').show();
                    $("#discount_percent").focus();
                }else{
                    $('#discount_percent_error').hide();
                }
                if($("#discount_percent").val() < 1){
                    $('#discount_percent_error_msg').html('Discount should be greater than or equal to 1');
                    $('#discount_percent_error').show();
                    $("#discount_percent").focus();
                }else{
                    $('#discount_percent_error').hide();
                }
                // if(!$("#discount_validity").val()){
                //     $('#discount_validity_error').show();
                //     $("#discount_validity").focus();
                // }else{
                //     $('#discount_validity_error').hide();
                // }
            }
            if(!$("input[name=delivery_charge]:checked").val()){
                $('#delivery_charge_error').show();
            }else{
                $('#delivery_charge_error').hide();
            }
            if($("input[name=delivery_charge]:checked").val() == "1"){
                
                if(!$("#local_delivery_charge_amount").val()){
                    $('#local_delivery_charge_error').show();
                    $("#local_delivery_charge_amount").focus();
                }else{
                    $('#local_delivery_charge_error').hide();
                }
               
            }


            if(!$('#product_price').val()){
                $("#product_price").focus();
            }else if(!$("input[name=price_including_tax]:checked").val()){
                $("input[name=price_including_tax]").focus();
            }else if($("input[name=price_including_tax]:checked").val() == "0" && !$("#tax").val()){
                $("#tax").focus();
            }else if(!$("input[name=discount_applicable]:checked").val()){
                $("input[name=discount_applicable]").focus();
            }else if($("input[name=discount_applicable]:checked").val() == "1" && !$("#discount_applicable_if_products").val() && !$("#discount_percent").val()){
                $("#discount_applicable_if_products").focus();
                $("#discount_percent").focus();
                // $("#discount_validity").focus();
            }else if($("input[name=discount_applicable]:checked").val() == "1" && !($("#discount_percent").val() > 0)){
                $("#discount_percent").focus();
            }else if(!$("input[name=delivery_charge]:checked").val()){
                $("input[name=delivery_charge]").focus();
            }else if($("input[name=delivery_charge]:checked").val() == "1"  && !$("#local_delivery_charge_amount").val()){
                
                $("#local_delivery_charge_amount").focus();
               
            }else{
                var id = $(this).data('id');
                $('li[data-id='+id+']').click();
            }
        });
    });
</script>

<script type="text/javascript">
    function myFunction(i){
        var id = $(i).data('id');
        $('li[data-id='+id+']').click();
    }
</script>
<!-- VALIDATION SCRIPTS END -->

<!-- SCRIPTS FOR LOADING DATA IN EDIT PRODUCT -->
<?php if(isset($q) || ($this->session->userdata('returnable') != '')){ ?>
<script>
    $(window).on('load', function(){
        var returnable = "<?php if(isset($q)){ echo $q['returnable']; }else{ if($this->session->userdata('returnable') != ''){ echo $this->session->userdata('returnable'); }} ?>";
        if(returnable == 1){
            $('#returnable_days_block').show();
        }else{
            $('#not_returnable_reason_block').show();
        }

        var cod = "<?php if(isset($q)){ echo $q['cod']; }else{ if($this->session->userdata('cod') != ''){ echo $this->session->userdata('cod'); }} ?>";
        if(cod == 0){
            $('#no_cod_reason_block').show();
        }

        var international_shipping = "<?php if(isset($q)){ echo $q['international_shipping']; }else{ if($this->session->userdata('international_shipping') != ''){ echo $this->session->userdata('international_shipping'); }} ?>";
        if(international_shipping == 1){
            $('#international_shipping_countries_block').show();
        }else{
            $('#no_international_shipping_block').show();
        }

        var price_including_tax = "<?php if(isset($q)){ echo $q['price_including_tax']; }else{ if($this->session->userdata('price_including_tax') != ''){ echo $this->session->userdata('price_including_tax'); }} ?>";
        if(price_including_tax == 0){
            $('#tax_block').show();
        }

        var discount_applicable = "<?php if(isset($q)){ echo $q['discount_applicable']; }else{ if($this->session->userdata('discount_applicable') != ''){ echo $this->session->userdata('discount_applicable'); }} ?>";
        if(discount_applicable == 1){
            $('#discount_applicable_if_products_block').show();
            $('#discount_percent_block').show();
            $('#discount_validity_block').show();
        }

        var delivery_charge = "<?php if(isset($q)){ echo $q['delivery_charge']; }else{ if($this->session->userdata('delivery_charge') != ''){ echo $this->session->userdata('delivery_charge'); }} ?>";
        if(delivery_charge == 1){
           
            $('#local_delivery_charge_block').show();
           
        }
    });
</script>
<?php } ?>

<!-- FUNCTIONALITY RELATED SCRIPTS -->
<script type="text/javascript">     
    $('body').on('change','#main_category_id',function(){ //alert();
        var main_category_id =$(this).val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
        var main_category_id = $('#main_category_id').val(); //alert(main_category_id);
        var category_id = "<?php if(isset($q)){ echo $q['category_id']; }else{ if($this->session->userdata('category_id') != ''){ echo $this->session->userdata('category_id'); }} ?>";
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $('body').on('change','#category_id',function(){ //alert();
        var main_category_id = $('#main_category_id').val();
        var category_id =$('#category_id').val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_attributes_list'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#attributes').html(data);
            }
        });
    });
</script>

<!-- TAGS SCRIPT START -->
<script type="text/javascript">
    $('body').on('change','#category_id',function(){
        var main_category_id = $('#main_category_id').val();
        var category_id = $('#category_id').val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_tags'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#tags').html(data);
            }
        });
    });
</script>
<script type="text/javascript">
    $(window).on('load', function(){
        var main_category_id = $('#main_category_id').val();
        var category_id = "<?php if(isset($q)){ echo $q['category_id']; }else{ if($this->session->userdata('category_id') != ''){ echo $this->session->userdata('category_id'); }} ?>";
        var tags = "<?php if(isset($q)){ echo $q['tags']; }else{ if($this->session->userdata('tags') != ''){ echo $this->session->userdata('tags'); }} ?>";
        $.ajax({
            url: "<?php echo base_url('product_controller/get_tags'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id, tags: tags },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#tags').html(data);
            }
        });
    });
</script>
<!-- TAGS SCRIPT END -->

<script>
    $(document).ready(function () {
        $("body").on("click", ".tabss", function () {
            var id = $(this).data("id");
            $(".tabss").removeClass("active");
            $(this).addClass("active");
            $(".tab").hide();
            $("#" + id).show();
        });
    });
</script>
<script type="text/javascript">
    $(document).on('click', '.add-new-btn', function(){
        var main_category_id = $('#main_category_id').val();
        var category_id = $('#category_id').val();
        var is_add = "yes";
        $.ajax({
            url: "<?php echo base_url('product_controller/get_attributes_list'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id, is_add:is_add },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                // $('#attributes').html(data);
                $('#appendAttr').append('<div class="removable form-group row"><label class="col-form-label col-md-3"> <spanclass="mandatory"></span></label><div class="col-md-8"><div class="row">'+ data +'<div class="col-md-1"><div class="form-group"><label style="opacity: 0">add</label><input type="button" class="remove btn form-control" value="x"></div></div></div></div></div>');
            }
        })
    });

    $(document).on('click', '.remove', function(){ 
        $(this).closest('.removable').remove();
    });
</script>

<!-- ADD ANOTHER IMAGE SCRIPT -->
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#add_another_image', function(){
            // if($('.images_div').length < 5){
                $('#add_another_image_div').append('<div class="form-group row images_div"><label class="col-form-label col-md-3"></label><div class="col-md-8"><input class="form-control" type="file" accept="image/png,image/jpg,image/jpeg" name="images[]"></div></div>');
            // }else{
            //     alert('You can add maximum of 5 images')
            // }
        });
    })
</script>

<!-- ADD ANOTHER BANK OFFER SCRIPT -->
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#add_another_bank_offers', function(){
            $('#add_another_bank_offers_div').append('<div class="form-group row bank_offers_div"><label class="col-form-label col-md-3"></label><div class="col-md-8"><input class="form-control" type="file" accept="image/png,image/jpg,image/jpeg" name="bank_offers[]"></div></div>');
        });
    })
</script>

<script type="text/javascript">
    $(document).on('click', '.add_attributes', function(){
        var rowCount = $("#rowCountinput").val();
        rowCount = +rowCount+1;
        $("#rowCountinput").val(rowCount);
        $('#appendAttributes').append('<div class="form-group row remove_attributes_block"><div class="col-md-2"></div><div class="col-md-9 row"><div class="col-md-2"><div class="form-group"><label>Size</label><select class="form-control" id="size" name="size[]"><option value="">-- Select --</option><option value="S">S</option><option value="M">M</option><option value="L">L</option><option value="XL">XL</option><option value="XXL">XXL</option><option value="3XL">3XL</option><option value="4XL">4XL</option><option value="5XL">5XL</option><option value="28">28</option><option value="30">30</option><option value="32">32</option><option value="34">34</option><option value="36">36</option><option value="38">38</option><option value="40">40</option><option value="42">42</option><option value="44">44</option><option value="46">46</option><option value="48">48</option><option value="50">50</option><option value="70">70</option><option value="75">75</option><option value="80">80</option><option value="85">85</option><option value="90">90</option><option value="95">95</option><option value="100">100</option><option value="105">105</option><option value="110">110</option></select></div></div><div class="col-md-2"><div class="form-group"><label>Color Name</label><input type="text" name="colorName[]" class="form-control"></div></div><div class="col-md-2"><div class="form-group"><label>Color</label><input type="file" name="color[]" class="form-control"></div></div><div class="col-md-2"><div class="form-group"><label>Quantity</label><input type="number" name="quantity[]" min="1" class="form-control" placeholder="Quantity"></div></div><div class="col-md-2"><div class="form-group"><label>Price</label><input type="number" name="productPrice[]" min="1" class="form-control" placeholder="Price"></div></div><div class="col-md-3"><div class="form-group"><label>Images</label><input multiple class="form-control" type="file" accept="image/*" name="images'+rowCount+'[]"><input name="rowCount[]" type="hidden" value="'+rowCount+'"></div></div><div class="col-md-1"><div class="form-group"><label style="opacity: 0">add</label><input type="button" class="remove_attributes btn btn-danger form-control" value="X"></div></div></div></div>');
    });

    $(document).on('click', '.remove_attributes', function(){ 
        $(this).closest('.remove_attributes_block ').remove();
    });
</script>

<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    var editor = CKEDITOR.replace("description");
    CKEDITOR.on('instanceReady', function(ev) {
        ev.editor.on('paste', function(evt) {
    
        var filter = new CKEDITOR.filter(evt.editor);
        
        var allowed = filter.allow({
            '$1': {
                elements: CKEDITOR.dtd,
                attributes: true,
                styles: false,
                classes: true
            }
        });
        if (allowed === false) {
            console.warn('An error occured setting the custom rules.');
            return;
        }
        
        var fragment = CKEDITOR.htmlParser.fragment.fromHtml(evt.data.dataValue);
        var writer = new CKEDITOR.htmlParser.basicWriter();
        filter.applyTo(fragment);
        fragment.writeHtml(writer);
        var processed_html = writer.getHtml();
        
        evt.data.dataValue = processed_html;
        console.log('Content filtered.');
        
        filter.destroy();
        });
    });
</script>

<?php include('footer.php'); ?>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#discount_validity" ).datepicker({ dateFormat: 'yy-mm-dd' });
    } );
</script>