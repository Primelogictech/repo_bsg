<?php $this->load->view('admin/header.php'); ?>
            
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-4">
                    <h4 class="page-title"> Attributes</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-2">
                    <div class="float-right">
                        <a class="add-new-btn btn" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach ($attributes as $value) { ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value['attribute_name']; ?></td>
                                    </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>          
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Attribute</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('masters/upload_attribute'); ?>" enctype="multipart/form-data">
                            
                            <div class="form-group">
                                <label> Attribute Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" required>
                
                            <div class="form-group mb15"> 
                                <div class="col-sm-12 text-center">
                                    <input type="submit" class="submit_btn" value="Submit">
                                </div>
                            </div>
                
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>          
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>