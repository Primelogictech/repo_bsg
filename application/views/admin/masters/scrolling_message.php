<?php $this->load->view('admin/header.php'); ?>
            
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-4">
                    <h4 class="page-title">Scrolling Message</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-2">
                    <div class="float-right">
                        <?php if(isset($scrolling_message) && empty($scrolling_message['id'])){ ?>
                        <a class="add-new-btn btn" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <td>Scrolling Message</td>
                                        <th class="text-right" style="width: 123px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if(isset($scrolling_message) && !empty($scrolling_message['scrolling_message'])){ echo $scrolling_message['scrolling_message']; } ?>
                                        </td>
                                        <td class="text-right">
                                            <div class="actions">
                                                <?php if(isset($scrolling_message) && !empty($scrolling_message['id']) && !empty($scrolling_message['scrolling_message'])){ ?>
                                                    <a class="btn btn-sm bg-success-light mr-2" id="scrolling_message" data-id="<?php echo $scrolling_message['id']; ?>" data-msg="<?php echo $scrolling_message['scrolling_message']; ?>">
                                                    <i class="fe fe-pencil"></i> Edit</a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>          
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Scrolling Message</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('masters_controller/upload_scrolling_message'); ?>" enctype="multipart/form-data">

                            <input type="number" name="id" id="id" hidden>
                            
                            <div class="form-group row images_div">
                                <label class="col-form-label col-md-3">Message</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" id="scrolling_message_input" name="scrolling_message" placeholder="Enter Scrolling Message" required>
                                </div>
                            </div>
                
                            <div class="form-group mb15"> 
                                <div class="col-sm-12 text-center">
                                    <input type="submit" class="submit_btn" value="Submit">
                                </div>
                            </div>
                
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>          
</div>
<!-- /Page Wrapper -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('body').on('click', '#scrolling_message', function(){
            var id = $(this).data('id');
            var msg = $(this).data('msg');
            $('#exampleModal').modal('show');
            $('#id').val(id);
            $('#scrolling_message_input').val(msg);
        })
    })
</script>

<?php $this->load->view('admin/footer.php'); ?>