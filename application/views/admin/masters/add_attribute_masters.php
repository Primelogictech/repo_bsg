<?php $this->load->view('admin/header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Add Attribute</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('masters/attribute_masters'); ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="<?php echo base_url('masters_controller/upload_attribute_masters') ?>" method="post" enctype="multipart/form-data">
                            <input type="number" name="id" value="<?php if(isset($q)){ echo $q['id']; } ?>" hidden>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Main Category<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="main_category_id" name="main_category_id" required>
                                        <option value="">-- Select --</option>
                                        <?php foreach ($main_categories as $key) { ?>
                                            <option value="<?php echo $key['id']; ?>" <?php if(isset($q)){  if($q['main_category_id'] == $key['id']){ echo "selected"; }} ?>><?php echo $key['main_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Select Category<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="category_id" name="category_id" required>
                                        <option value="">-- Select --</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-md-2">Attributes<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <div class="removeAttr form-group row">
                                        <div class="col-md-12">
                                            <select class="form-control" name="attributes[]" multiple>
                                                <?php foreach($attributes as $val){ ?>
                                                <option value="<?php echo $val['id']; ?>"><?php echo $val['attribute_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                                    
                                    </div>
                                </div>
                            </div>
                            <div id="appendAttr"></div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">     
    $('body').on('change','#main_category_id',function(){ //alert();
        var main_category_id =$(this).val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
        var main_category_id = $('#main_category_id').val(); //alert(main_category_id);
        var category_id = <?php if(isset($q)){ echo $q['category_id']; } ?>;
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>