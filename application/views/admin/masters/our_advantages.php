<?php $this->load->view('admin/header.php'); ?>
            
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-4">
                    <h4 class="page-title"> Our Adavantages</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-2">
                    <div class="float-right">
                        <?php if(isset($our_advantages_data) && empty($our_advantages_data['id'])){ ?>
                        <a class="add-new-btn btn" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <td>Images</td>
                                        <th class="text-right" style="width: 123px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php if(isset($our_advantages_data) && !empty($our_advantages_data['images'])){
                                                $array = explode(',', $our_advantages_data['images']);
                                                foreach ($array as $key) { if(!empty($key)){ ?>
                                                <img width="100" src="<?php echo base_url(); ?>uploads/images/our_advantages_images/<?php echo $key; ?>">
                                                <a href="<?php echo base_url('masters/delete_our_advantages_images'); ?>/<?php echo $our_advantages_data['id']; ?>/<?php echo $key; ?>" style="margin-right: 25px;"><i class="fe fe-minus text-danger" style="position: absolute;"></i></a>
                                            <?php }}} ?>
                                        </td>
                                        <td class="text-right">
                                            <div class="actions">
                                                <?php if(isset($our_advantages_data) && !empty($our_advantages_data['id'])){ ?>
                                                    <a class="btn btn-sm bg-success-light mr-2" id="edit_logo" data-id="<?php echo $our_advantages_data['id']; ?>">
                                                    <i class="fe fe-pencil"></i> Edit</a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>          
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Our Advantages</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="<?php echo base_url('masters/upload_our_advantages'); ?>" enctype="multipart/form-data">

                            <input type="number" name="id" id="id" hidden>
                            
                            <div class="form-group row images_div">
                                <label class="col-form-label col-md-3">Upload Images</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="file" accept="image/*" name="images[]">
                                </div>
                                <div class="col-md-1"><a class="btn btn-primary" id="add_another_image">+</a></div>
                            </div>
                            <div id="add_another_image_div"></div>
                            <?php if(isset($our_advantages_data) && !empty($our_advantages_data['images'])){ ?>
                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-9">
                                    <?php $array = explode(',', $our_advantages_data['images']); foreach ($array as $key) { if(!empty($key)){ ?>
                                        <img src='<?php echo base_url(); ?>uploads/images/our_advantages_images/<?php echo $key; ?>' width="50px">
                                    <?php }} ?>
                                </div>
                            </div>
                            <?php } ?>
                
                            <div class="form-group mb15"> 
                                <div class="col-sm-12 text-center">
                                    <input type="submit" class="submit_btn" value="Submit">
                                </div>
                            </div>
                
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>          
</div>
<!-- /Page Wrapper -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('body').on('click', '#edit_logo', function(){
            var id = $(this).data('id');
            $('#exampleModal').modal('show');
            $('#id').val(id);
        })
    })
</script>

<!-- ADD ANOTHER IMAGE SCRIPT -->
<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('click', '#add_another_image', function(){
            $('#add_another_image_div').append('<div class="form-group row images_div"><label class="col-form-label col-md-3"></label><div class="col-md-8"><input class="form-control" type="file" accept="image/png,image/jpg,image/jpeg" name="images[]"></div></div>');
        });
    })
</script>

<?php $this->load->view('admin/footer.php'); ?>