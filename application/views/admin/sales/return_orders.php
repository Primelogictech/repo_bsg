<?php $this->load->view('admin/header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h4 class="page-title">Return Orders</h4>
                </div>
                <div class="float-right">
                </div>
            </div>
        </div>
        <!-- /Page Header -->
        
        <div class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo base_url(); ?>orderFilter/new"><input type="button" value="New" class="btn"></a>
                </div>
                <div class="col-md-2">
                <a href="<?php echo base_url(); ?>orderFilter/inProgress"><input type="button" value="In Progress" class="btn"></a>
                </div>
                <div class="col-md-2">
                <a href="<?php echo base_url(); ?>orderFilter/complted"><input type="button" value="Completed" class="btn"></a>
                </div>
                <div class="col-md-2">
                <a href="<?php echo base_url(); ?>orderFilter/rejected"><input type="button" value="Rejected" class="btn"></a>
                </div>
                <div class="col-md-2">
                <a href="<?php echo base_url(); ?>orderFilter/returned"><input type="button" value="Returned" class="btn"></a>
                </div>
                <div class="col-md-2">
                <a href="<?php echo base_url(); ?>orderFilter/all"><input type="button" value="All" class="btn"></a>
                </div>
                <div class="col-md-2">
                    <select id="filterDate">
                        <option value="">Date</option>
                        <option value="new">Latest</option>
                        <option value="old">Oldest</option>
                    </select>
                    <form action="<?php echo base_url('orderFilter/newAll'); ?>" id="filerDateFormNew">
                    </form>
                    <form action="<?php echo base_url('orderFilter/old'); ?>" id="filerDateFormOld">
                    </form>
                </div>    
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Order ID</th>
                                        <?php if($this->session->userdata('role') == "admin"){ ?>
                                        <th>Order Status</th>
                                        <?php  } ?>
                                        <th>Payments</th>
                                        <th>Customer Name</th>
                                        <th>Order Credited On</th>
                                        <th>Order Total</th>
                                        <th class="text-right" style="width: 123px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($orders) && !empty($orders)){ $i=1; foreach($orders as $value){ ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo "ORDR-".$value['id']; ?></td>
                                        <?php if($this->session->userdata('role') == "admin"){ ?>
                                        <td><?php echo Orders::checkOrderStatus($value['order_status']); ?></td>
                                        <?php  } ?>
                                        <td>
                                            <div>Payment Method: <?php echo $value['payment_method']; ?></div>
                                            <div>Payment Status : <?php echo $value['payment_status']; ?></div>
                                        </td>
                                        <td><?php echo $value['name']; ?></td>
                                        <td><?php echo $value['created_on']; ?></td>
                                        <td><?php echo "₹ ".$value['order_total']; ?></td>
                                        
                                        <td class="text-right">
                                            <div class="actions">
                                                <a href="<?php echo base_url('sales/order_details') ?>/<?php echo base64_encode($value['id']); ?>" class="btn btn-sm bg-success-light mr-2">
                                                    <i class="far fa-eye"></i> View
                                                </a>
                                            </div>
                                            <?php
                                    if(isset($value['return_status'])){ ?>
                                            <div class="actions">
                                                <a href="<?php echo base_url('sales/order_details') ?>/<?php echo base64_encode($value['id']); ?>" class="btn btn-sm bg-danger-light mr-2">
                                                    <i class="far fa-eye"></i> View Return Requests
                                                </a>
                                            </div>
                                        <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $i++; }} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>			
        </div>
        
    </div>			
</div>
<!-- /Page Wrapper -->

<?php $this->load->view('admin/footer.php'); ?>
<script>
    $(document).ready(function(){
        $("#filterDate").change(function(){
            var val = $(this).val();
            if (val != '' && val =='new'){
                $("#filerDateFormNew").submit();
            } else if (val != '' && val =='old'){
                $("#filerDateFormOld").submit();
            }
            
        });
    });
</script>