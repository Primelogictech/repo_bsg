<?php include('header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Sale Items List</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('add_create_sale_item'); ?>" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-hover table-center mb-0" id="myTable">
								<thead>
									<tr>
										<th>Name</th>
										<th>Discount (%)</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>

	</div>			
</div>
<!-- /Page Wrapper -->

<?php include('footer.php'); ?>

<script>
    $(document).ready(function(){
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax:{
                url: "<?php echo base_url('ajax_sale_items'); ?>",
            },
            columns:[
            	{ data: 'name', name: 'name' },
                { data: 'discount', name: 'discount' },
                { data: 'action', name: 'action' }
            ]
        });
    })
</script>
