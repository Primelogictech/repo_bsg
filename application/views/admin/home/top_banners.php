<?php $this->load->view('admin/header.php'); ?>

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-3">
                    <h4 class="page-title">Top Banner</h4>
                </div>
                <div class="col-6">
                    <p <?php if($this->session->flashdata('success') != ''){ ?>style="color: green;"<?php }else{ ?>style="color: red;"<?php } ?> class="text-center"><?php if($this->session->flashdata('success') != ''){ echo $this->session->flashdata('success'); }else{ echo $this->session->flashdata('danger'); } ?></p>
                </div>
                <div class="col-3">
                    <div class="float-right">
                        <a href="<?php echo base_url('home/add_top_banners'); ?>" data-toggle="tooltip" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i> Add New Banner</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Banner Image</th>
                                        <th>Main Category</th>
                                        <th style="width: 250px;">Linked to</th>
                                        <th>View</th>
                                        <th class="text-right" style="width: 123px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach($top_banners as $value){ ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td>
                                            <img src="<?php echo base_url(); ?>uploads/images/top_banners/<?php echo $value['banner_image']; ?>" alt="banner" width="100px" />
                                        </td>
                                        <td><?php if($value['main_category_id']==1){ echo "Mens"; }else{ echo "Kids"; } ?></td>
                                        <td>
                                            <select id="linked_to" data-id="<?php echo ($value['id']); ?>" name="linked_to" class="form-control">
                                                <option value="">-- Select --</option>
                                                <?php foreach ($sales as $key) { ?>
                                                    <option value="<?php echo $key['id']; ?>" <?php if($value['linked_to'] == $key['id']){ echo "selected"; } ?>><?php echo $key['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php if(!empty($value['linked_to'])){ ?>
                                            <a href="<?php echo base_url('view_sale_products'); ?>/<?php echo base64_encode($value['linked_to']); ?>">View Products</a>
                                            <?php } ?>
                                        </td>
                                        <td class="text-right">
                                            <div class="actions">
                                                <?php if($value['status']==1){ ?>
                                                    <a href="<?php echo base_url('home/top_banner_status'); ?>/<?php echo base64_encode($value['id']); ?>/0" class="btn btn-sm bg-danger-light mr-2"> Make In-Active</a>
                                                <?php }else{ ?>
                                                <a href="<?php echo base_url('home/top_banner_status'); ?>/<?php echo base64_encode($value['id']); ?>/1" class="btn btn-sm bg-success-light mr-2"> Make Active</a>
                                                <?php } ?>
                                                <a class="btn btn-sm bg-danger-light" href="<?php echo base_url('home/delete_top_banner'); ?>/<?php echo base64_encode($value['id']); ?>"> <i class="fe fe-trash"></i> Delete </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/footer.php'); ?>

<script type="text/javascript">     
    $('body').on('change','#linked_to',function(){ //alert();
        var id =$(this).data('id');
        var linked_to =$(this).val();
        var table = "tbl_home_top_banners";
        $.ajax({
            url: "<?php echo base_url('home_controller/banner_linked_to'); ?>",
            data: { id:id, linked_to:linked_to, table:table },
            type: 'POST',
            dataType: 'html',
            success: function (msg) {
                alert(msg);
            }
        });
    });
</script>