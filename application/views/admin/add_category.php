<?php include('header.php'); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Category Creation</h4>
				</div>
				<div class="col-8">
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-1">
					<div class="float-right">
						<a href="<?php echo base_url('categories'); ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form action="<?php echo base_url('admin_controller/upload_category') ?>" method="post" enctype="multipart/form-data">
							<input type="number" name="id" value="<?php if(isset($q)){ echo $q['id']; } ?>" hidden>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Category Name<span class="mandatory">*</span></label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="category_name" placeholder="Category Name" value="<?php if(isset($q)){ echo $q['category_name']; } ?>" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Category Image <span class="mandatory">(please upload an image with 384x384 pixels)</span></label>
								<div class="col-md-9">
									<input class="form-control" type="file" name="category_image" accept="image/png,image/jpg,image/jpeg" <?php if(isset($q) && !empty($q['id'])){}else{ echo "required"; } ?>>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Choose Main Category <span class="mandatory">*</span></label>
								<div class="col-md-9">
									<select class="form-control" name="main_category_id" required>
										<option value="">-- Select --</option>
										<?php foreach ($main_categories as $key) { ?>
										<option value="<?php echo $key['id']; ?>" <?php if(isset($q)){  if($q['main_category_id'] == $key['id']){ echo "selected"; }} ?>><?php echo $key['main_category_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="text-right">
								<input class="btn btn-primary" type="submit" name="submit" value="Submit">
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>

	</div>			
</div>
<!-- /Page Wrapper -->

<?php include('footer.php'); ?>