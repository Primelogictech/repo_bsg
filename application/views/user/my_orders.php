<?php include('header.php'); ?>

<div class="page-wrapper ml-0 pt-0">
    <div class="content container-fluid pt-0">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 pt-5">
                <a href="<?php echo base_url(); ?>" class="headingg">
                    <div class="backbtn">
                        <i class="fas fa-chevron-left"></i>
                        <span>My Orders</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-md-6 offset-md-3">
            <div class="row">
    			<?php foreach($orders as $order){ ?>
    			<div class="order-block">
    	            <div class="col-12">
    	                <div class="row">
    	                    <div class="col-6 px-0">
    	                        <div class="orders-title">Order Number</div>
    	                        <div class="order-content">BSG-<?php echo $order['id']; ?></div>
    	                    </div>
    	                    <div class="col-6 px-0">
    	                        <div class="orders-title text-right">Order Total</div>
    	                        <div class="order-content text-right">Rs. <?php echo $order['order_total']; ?></div>
    	                    </div>
    	                    <div class="col-12 px-0">
    	                    	<div class="orders-title">Placed on <?php echo date('M d Y',strtotime($order['created_on']));  ?></div>
    	                    </div>
    	                </div>
    	            </div>
    	            <div class="col-12 px-0">
    	                <div>
    					<div class="orders-title">Items in your order</div>
    		            </div>
    		        </div>
    		        <div class="col-12">
    			        <div class="row">
    					<?php 
    					$whereCondition = array('tbl_order_products.order_id'=>$order['id']);
    					$products = $this->vendor_model->getUserProducts($whereCondition); ?>
    					<?php foreach($products as $product){ ?>
    			        	<div class="col-3 px-1">
    			        		<div class="item-image-block">
    			        			<img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $product['banner_image']; ?>" class="img-fluid" alt="">
    			        		</div>
    				        </div>
    					<?php } ?>
    			        </div>
    			    </div>
    			    <div class="col-12 mt-2">
    			    	<a href="<?php echo base_url('order_details'); ?>/<?php echo base64_encode($order['id']) ?>" class="view-more-details-btn">View More Details</a>
    			    </div>
    			</div>
    			<?php } ?>
    		</div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>