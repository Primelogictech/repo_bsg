<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/user_other/css_user/progress-bar.css" />

<?php include('header.php'); ?>


<div class="page-wrapper ml-0 pt-0 mb-0">
	<div class="content container-fluid pt-0">
		<div class="row">
			<div class="col-12">
				<a href="<?php echo base_url(); ?>">
					<div class="backbtn">
						<i class="fas fa-chevron-left"></i>
						<span>Track Order Shipment</span>
					</div>
				</a>
			</div>

			<div class="col-12">
				<div class="order__id">ORD-<?php echo $orderData['order_id']; ?></div>
				<div class="order__date">Purchase Day - <?php echo date("d M Y", strtotime($orderData['placed_on'])); ?></div>
			</div>

			<hr class="w-100" />
	<?php
	$statusInfo = json_decode($orderData['status_info'],true);
	?>
			<div class="col-12">
				<ul class="progress-tracker mt10 progress-tracker--vertical">
					<li class="progress-step <?php if(is_array($statusInfo)){ if (array_key_exists(Orders::$orderPlaced,$statusInfo)) { echo "is-complete"; } }?>" <?php if(is_array($statusInfo)){ if (array_key_exists(Orders::$orderPlaced,$statusInfo)) { ?> aria-current="step" <?php } }?>>
						<div class="progress-marker">
							<span class="order__steps">1</span>
						</div>
						<div class="progress-text">
							<h4 class="progress-title">Order Placed</h4>
							<p>Order Successfully placed<?php if(is_array($statusInfo)){  echo " ( ".date("d M Y", strtotime($statusInfo[Orders::$orderPlaced]))." )"; } ?></p>
						</div>
					</li>

					<li class="progress-step <?php if(is_array($statusInfo)){ if (array_key_exists(Orders::$handOverToCourier,$statusInfo)) { echo "is-complete"; } }?>" <?php if(is_array($statusInfo)){ if (array_key_exists(Orders::$handOverToCourier,$statusInfo)) { ?> aria-current="step" <?php } }?>>
						<div class="progress-marker">
							<span class="order__steps">2</span>
						</div>
						<div class="progress-text">
							<h4 class="progress-title">Order Shipped</h4>
							<p>Your order is Shipped<?php if(is_array($statusInfo)){ if (array_key_exists(Orders::$handOverToCourier,$statusInfo)) { echo " ( ".date("d M Y", strtotime($statusInfo[Orders::$handOverToCourier]))." )"; }} ?></p>
						</div>
					</li>

					<li class="progress-step" >
						<div class="progress-marker">
							<span class="order__steps">3</span>
						</div>
						<div class="progress-text">
							<h4 class="progress-title">Out Of Delivery</h4>
							<p>Your oreder is ready to Delivery</p>
						</div>
					</li>

					<li class="progress-step">
						<div class="progress-marker">
							<span class="order__steps">4</span>
						</div>
						<div class="progress-text">
							<h4 class="progress-title">Delivered</h4>
							<p>Order Delivered</p>
						</div>
					</li>
				</ul>
			</div>

		</div>
	</div>
</div>

<?php include('footer.php'); ?>