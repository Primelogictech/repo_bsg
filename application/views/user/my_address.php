<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8" />
        <meta name="description" content="BSG Garments" />
        <meta name="keywords" content="BSG Garments, unica, creative, html" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>BSG Garments</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet" />

        <!-- Css Styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/elegant-icons.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/magnific-popup.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/owl.carousel.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/slicknav.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/custom.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/ecomm.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a><?php }else{ ?>
                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                    <?php } ?>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <a class="text-dark" href="<?php echo base_url('my_orders'); ?>">My Orders</a>
                <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a>
                <?php }  ?>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a>
                                    <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a><?php }else{ ?>
                                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="<?php echo base_url(); ?>"><h3>BSG Garments</h3></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="header__nav__option">
                            <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                            <a href="#"><img src="<?php echo base_url(); ?>uploads/images/icon/heart.png" alt="" /></a>
                            <a href="<?php echo base_url('checkout'); ?>"><img src="<?php echo base_url(); ?>uploads/images/icon/cart.png" alt="" /> <span><?php if(isset($cart)){ echo $cart; } ?></span></a>
                            <div class="price">₹0.00</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->

<section class="checkout-section">
    <div class="container-fluid">
        <div class="col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1">
            <div class="row">
                <div class="col-12 col-md-7 col-lg-7 sm-px0 order-sm-1 order-2">
                    <div class="addressList-base-titleContainer ">
                        <div class="addressList-base-title">Select Delivery Address</div>
                        <!-- <?php if(!empty($payment)){ ?>
                            <?php echo form_open('add_new_address'); ?>
                            <input type="hidden" name="payment" value="<?php echo base64_encode(json_encode($payment)); ?>">
                            <button type="submit" class="addressList-base-addAddressButton">ADD NEW ADDRESS</button>
                            <?php echo form_close(); ?>
                        <?php } else{ ?>
                            <?php echo form_open('add_new_address'); ?>
                            <button type="submit" class="addressList-base-addAddressButton">ADD NEW ADDRESS</button>
                            <?php echo form_close(); ?>
                        <?php } ?> -->

                        <button class="addressList-base-addAddressButton" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#exampleModal">ADD NEW ADDRESS</button>

                        <?php if(!empty($payment)){ ?>
                            <?php echo form_open('preCheckout',['id'=>'from']); ?>
                            <input type="hidden" name="address" id="address">
                            <input type="hidden" name="payment" value="<?php echo base64_encode(json_encode($payment)); ?>">
                            <button type="submit" class="addressList-base-addAddressButton mr-2">CONTINUE</button>
                            <?php echo form_close(); ?>
                        <?php } ?>
                    </div>
                    <div class="addressList-base-defaultTitle">DEFAULT ADDRESS</div>

                    <?php foreach($addresses as $val){ ?>
                        <div class="addressBlocks-base-block">
                            <div>
                                <input type="radio" name="useAddr" class="useAddr addressbase-radio-btn" value="<?php echo $val['id']; ?>" <?php if($val['primary_address']==1){ echo "checked"; } ?>>
                            </div>
                            <div class="addressDetails-base-desktopAddressTitle">
                                <div class="addressDetails-base-name"><?php echo $val['full_name']; ?></div>
                                <button class="addressDetails-base-addressType"><?php echo $val['address_type']; ?></button>
                            </div>
                            <div class="addressDetails-base-addressField"><?php echo $val['address_lane']; ?></div>
                            <div><?php echo $val['landmark']; ?></div>
                            <div>
                                <span><?php echo $val['city']; ?>, <?php echo $val['state']; ?> - </span>
                                <span><?php echo $val['zip']; ?></span>
                            </div>
                            <div class="addressDetails-base-mobile">
                                <span>Mobile: </span>
                                <span><?php echo $val['mobile']; ?></span>
                            </div>
                            <div class="addressServiceability-base-container">
                                <div>
                                    <span class="addressServiceability-base-bullet">•</span>
                                    <span>Pay on Delivery not available</span>
                                </div>
                            </div>
                            <div class="addressBlocks-base-btns">
                                <button class="addressBlocks-base-remove"><a href="<?php echo base_url('deleteAddress'); ?>/<?php echo base64_encode($val['id']); ?>" >Remove</a></button>
                                <!-- <button class="addressBlocks-base-edit"><a href="<?php echo base_url('editAddress'); ?>/<?php echo base64_encode($val['id']); ?>" >Edit</a></button> -->
                                <button class="addressBlocks-base-edit editAddress" data-id="<?php echo $val['id']; ?>">Edit</button>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-12 col-md-5 col-lg-5 order-1 sm-px0">
                    <div class="checkout-box summary">
                        <div class="heading">
                            <strong id="summary-title">Your order summary</strong>
                        </div>
                        <?php $price = 0; $discount = 0; 
                        $productsData = array();
                        $i=0;
                        $deliveryCharges = 0;
                        foreach($cart_data as $data){ ?>
                        <?php
                        $productsData[$i] = array(
                            'productId' => $data['id'],
                            'price' => $data['product_price'] - ($data['product_price']*($data['discount_percent']/100)),
                        );
                        $deliveryCharges = $deliveryCharges + $data['local_delivery_charge_amount'];
                        ?>
                        <?php $price = $price +($data['product_price'] * (isset($data['quantity'])? $data['quantity']:1)); ?>
                        <?php 
                            if ($data['discount_percent'] != 0) {  
                                $discount = $discount+($data['product_price'] - ($data['product_price'] - ($data['product_price']*($data['discount_percent']/100))));
                            }
                        ?>
                        <div class="shopping-cart-item-section">
                            <div class="row">
                                <div class="col-3 col-md-3 pr-0">
                                    <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $data['color']; ?>" />
                                </div>
                                <div class="col-9 col-md-9 px8">
                                    <div><?php echo $data['product_title']; ?></div>
                                    <p class="checkout-product-name"><?php echo substr(html_entity_decode($data['short_description']), 0, 100)."..."; ?></p>
                                    <div class="shopping-cart-item-group">
                                        <div class="d-inline-block"><span>Quantity : </span><span><?php  echo $data['quantity']; ?></span></div>
                                        <div class="float-right">
                                            <span>Price:</span>
                                            <span><strong>₹ <?php echo $data['product_price']; ?></strong></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $i++;} ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="item-subtotal">
                                    <strong>Sub Total : </strong>
                                    <strong class="float-right">₹ <?php echo $price; ?></strong>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="item-subtotal">
                                    <strong>Delivery Charges : </strong>
                                    <strong class="float-right">₹ <?php echo $deliveryCharges;; ?></strong>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="standinfo">
                                    <div class="row">
                                        <div class="col-7">
                                            <strong>Discount : </strong>
                                        </div>
                                        <div class="col-5">
                                            <strong class="float-right">₹ <?php echo $discount; ?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="item-total">
                                    <strong>Total : </strong>
                                    <strong class="float-right">₹ <?php echo ($price - $discount + $deliveryCharges); ?></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="payment-info">
                        <p><strong>Ways you can pay:</strong></p>
                        <div class="shoppinglogos"></div>
                    </div> -->
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="addressList-base-addBlock">
                                    <div class="address__block">
                                        <form class="form-horizontal" method="post" action="<?php echo base_url('saveAddress'); ?>" enctype="multipart/form-data">
                                            <input type="hidden" name="address_id" id="address_id" value="">
                                            <?php if(!empty($payment)){ ?>
                                            <input type="hidden" name="payment" value="<?php echo base64_encode(json_encode($payment)); ?>">
                                            <?php } ?>
                                            <div class="form-group">
                                                <input type="text" class="in" name="full_name" id="full_name" value="" required="required"/>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Name *</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="tel" class="in" name="mobile" id="mobile" value="" required="required"/>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Mobile Number *</label>
                                            </div>
                                            <div class="addressForm-base-formHeader">ADDRESS</div>
                                            <div class="form-group">
                                                <input type="tel" class="in" name="zip" id="zip" value="" required="required"/>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Pin Code *</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="in" name="address_lane" id="address_lane" value="" required="required"/>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Address (House No, Building, Street, Area) *</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="in" name="landmark" id="landmark" value="" required="required"/>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Land Mark *</label>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-12 col-sm-6">
                                                    <select class="in" name="city" id="city" required>
                                                        <option value="">City / District *</option>
                                                        <option value="Hyderabad">Hyderabad</option>
                                                        <option value="Karimnagar">Karimnagar</option>
                                                        <option value="Vijayawada">Vijayawada</option>
                                                        <option value="Amaravathi">Amaravathi</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-12 col-sm-6">
                                                    <select class="in" name="state" id="state" required>
                                                        <option value="">State *</option>
                                                        <option value="Telangana">Telangana</option>
                                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="addressForm-base-formHeader">SAVE ADDRESS AS</div>
                                            <div class="addressForm-base-addressTypes">
                                                <input type="radio" name="address_type" class="address_type_input" value="HOME"> Home 
                                                <input type="radio" name="address_type" class="address_type_input" value="WORK"> Work
                                            </div>
                                            <div class="addressbase__text">
                                                <input type="checkbox" name="make_defalut" id="make_defalut">
                                                <span class="addressForm-base-defaultAddress">Make this my default address</span>
                                            </div>
                                            <div class="addressbase__button">
                                                <button type="submit" class="addressbase_addaddress_button">SAVE</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function(){
        $(".useAddr").click(function(){
            var addr = $(this).val();
            $("#address").val(addr);
        });
        $("#from").submit(function(){
           var addr = $("#address").val();
           if (addr == ""){
               alert("Please select Address");
               return false;
           }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
        var addr = $("input:radio[name=useAddr]:checked").val();
        $("#address").val(addr);
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".editAddress").click(function(){
            var id = $(this).data('id');
            $.ajax({
                url: "<?php echo base_url('user_controller/getAddressData') ?>",
                data: { id:id },
                type: 'POST',
                dataType: 'html',
                success: function (resp) {
                    data = JSON.parse(resp);
                    $('#address_id').val(data['id']);
                    $('#full_name').val(data['full_name']);
                    $('#mobile').val(data['mobile']);
                    $('#zip').val(data['zip']);
                    $('#address_lane').val(data['address_lane']);
                    $('#landmark').val(data['landmark']);
                    $('#city').val(data['city']);
                    $('#state').val(data['state']);
                    if(data['address_type'] != ""){
                        $('input[class=address_type_input][value='+ data['address_type'] +']').prop('checked', true);
                    }
                    if(data['primary_address'] == 1){
                        $('#make_defalut').prop('checked', true);
                    }
                    $('#exampleModal').modal('show');
                }
            })
        });
    });
</script>
<script type="text/javascript">
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        $(this)
        .find("input,textarea,select")
        .val('')
        .end()
        .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
    })
</script>