<?php include('header.php'); ?>
            
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-9">
                    <h4 class="page-title">Product View</h4>
                </div>
                <div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('products'); ?>" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-reply"></i></a>
					</div>
				</div>
            </div>
        </div>
        <!-- /Page Header -->
        <?php foreach($products as $value){ ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5>General</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Product Title : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['product_title'])){ echo $value['product_title']; } ?> </div>
                            </div>
                            <div class="col-md-3">
                                <label> Available Quantity : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['product_title'])){ echo $value['product_title']; } ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Short Description (Which comes under product Title) : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['short_description'])){ echo $value['short_description']; } ?> </div>
                            </div>
                            <div class="col-md-3">
                                <label>Description : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['description'])){ echo htmlspecialchars_decode($value['description']); } ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Estimated Delivery Days : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['estimated_delivery_days'])){ echo $value['estimated_delivery_days']; } ?> </div>
                            </div>
                            <div class="col-md-3">
                                <label>Returnable : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['returnable']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                        </div>
                         <div class="row">
                            <?php if($value['returnable']==1){ ?>
                            <div class="col-md-3">
                                <label>If Yes, How many Days : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['returnable_days'])){ echo $value['returnable_days']; } ?> </div>
                            </div>
                            <?php }else{ ?>
                            <div class="col-md-3">
                                <label>If No, Reason : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['not_returnable_reason'])){ echo $value['not_returnable_reason']; } ?> </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-3">
                                <label>Payment Menthods : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['payment_methods'])){ echo $value['payment_methods']; } ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>COD Available : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['cod']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                            <?php if($value['cod']==0){ ?>
                            <div class="col-md-3">
                                <label>If No, Reason : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['no_cod_reason'])){ echo $value['no_cod_reason']; } ?> </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>International Shipping Available : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['international_shipping']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                            <?php if($value['international_shipping']==1){ ?>
                            <div class="col-md-3">
                                <label>If Yes, Available Countries : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['international_shipping_countries'])){ echo $value['international_shipping_countries']; } ?> </div>
                            </div>
                            <?php }else{ ?>
                            <div class="col-md-3">
                                <label>If No, Reason : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['no_international_shipping_reason'])){ echo $value['no_international_shipping_reason']; } ?> </div>
                            </div>
                            <?php } ?>
                        </div>
                        <h5>Pricing</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Price Per each Item : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['product_price'])){ echo "Rs. ".$value['product_price']; } ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Price Including Tax : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['price_including_tax']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                            <?php if($value['price_including_tax']==0){ ?>
                            <div class="col-md-3">
                                <label>If No, Select Tax : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['tax'])){ echo $value['tax']." %"; } ?> </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Discount Applicable : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['discount_applicable']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                            <?php if($value['discount_applicable']==1){ ?>
                            <div class="col-md-3">
                                <label>If Yes, On purchase of how many items discount to be applied : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['discount_applicable_if_products'])){ echo $value['discount_applicable_if_products']; } ?> </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if($value['discount_applicable']==1){ ?>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Discount Percentage : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['discount_percent'])){ echo $value['discount_percent']; } ?> </div>
                            </div>
                            <div class="col-md-3">
                                <label>Discount Validity : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!($value['discount_validity'] == "0000-00-00")){ echo $value['discount_validity']; } ?> </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Delivery Charge : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if($value['delivery_charge']==1){ echo "Yes"; }else{ echo "No"; } ?> </div>
                            </div>
                            <?php if($value['delivery_charge']==1){ ?>
                            <div class="col-md-3">
                                <label>Iy Yes, Local Delivery Charge Amount : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['local_delivery_charge_amount'])){ echo "Rs. ".$value['local_delivery_charge_amount']; } ?> </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <?php if($value['delivery_charge']==1){ ?>
                            <div class="col-md-3">
                                <label> Non Local Delivery Charge Amount : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php if(!empty($value['non_local_delivery_charge_amount'])){ echo "Rs. ".$value['non_local_delivery_charge_amount']; } ?> </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-3">
                                <label>Reward points per purchase of each item : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php echo $value['reward_points_per_item']; ?> </div>
                            </div>
                        </div>
                        <h5>Links</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <label> Category : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php echo $value['category_name']; ?> </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Attributes (Depending on the selected category and sub category) : </label>
                            </div>
                            <div class="col-md-3">
                                <div> Attributes </div>
                            </div>
                            <div class="col-md-3">
                                <label>Related Products : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <?php echo $value['related_products']; ?> </div>
                            </div>
                        </div>
                        <?php if(!empty($value['banner_image'])){ ?>
                        <div class="row">
                            <div class="col-md-3">
                                <label> Banner : </label>
                            </div>
                            <div class="col-md-3">
                                <div> <img src='<?php echo base_url(); ?>uploads/images/product_images/<?php echo $value['banner_image']; ?>' width="100px"> </div>
                            </div>
                        </div>
                        <?php } ?>
                        <h5>Images and Videos</h5>
                         <div class="row">
                            <?php if(!empty($value['images'])){ ?>
                            <div class="col-md-3">
                                <label>Uploaded Images : </label>
                            </div>
                            <div class="col-md-3">
                                <?php $array = explode(',', $value['images']); foreach ($array as $key) { if(!empty($key)){ ?>
                                    <img src='<?php echo base_url(); ?>uploads/images/product_images/<?php echo $key; ?>' width="100px"><br>
                                <?php }} ?>
                            </div>
                            <?php } ?>
                            <?php if(!empty($value['videos'])){ ?>
                            <div class="col-md-3">
                                <label>Uploaded Video : </label>
                            </div>
                            <div class="col-md-3">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="<?php echo $value['videos']; ?>" allowfullscreen></iframe>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
        <?php } ?>
    </div>          
</div>
<!-- /Page Wrapper -->
        
<?php include('footer.php');  ?>