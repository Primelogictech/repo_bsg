<?php include('header.php'); ?>
			
<!-- Page Wrapper -->
<div class="page-wrapper">
	<div class="content container-fluid">

		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Category Creation</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('add_category'); ?>" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table class="datatable table table-hover table-center mb-0">
								<thead>
									<tr>
										<th>Category Name</th>
										<th>Main Category</th>
										<th>Category Image</th>
										<th class="text-right" style="width: 123px;">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($categories as $value) { ?>
									<tr>
										<td><?php echo $value['category_name']; ?></td>
										<td><?php if($value['main_category_id']==1){ echo "Mens"; }else{ echo "Kids"; } ?></td>
										<td><?php if(!empty($value['category_image'])){ ?><img width="50" src="<?php echo base_url(); ?>uploads/images/category_images/<?php echo $value['category_image']; ?>"><?php } ?></td>
										<td class="text-right">
											<div class="actions">
												<a href="<?php echo base_url('edit_category'); ?>/<?php echo base64_encode($value['id']); ?>" class="btn btn-sm bg-success-light mr-2">
													<i class="fe fe-pencil"></i> Edit
												</a>
												<a class="btn btn-sm bg-danger-light" href="<?php echo base_url('delete_category'); ?>/<?php echo base64_encode($value['id']); ?>">
													<i class="fe fe-trash"></i> Delete
												</a>
											</div>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>			
		</div>

	</div>			
</div>
			<!-- /Page Wrapper -->

<?php include('footer.php'); ?>