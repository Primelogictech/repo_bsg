<?php include(__DIR__."/../header.php"); ?>

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-3">
                    <h4 class="page-title">Add Items You May Like</h4>
                </div>
                <div class="col-6">
                    <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                    <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                </div>
                <div class="col-3">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 form-group row"> 
                                <label class="col-form-label col-md-5">Category</label>
                                <div class="col-md-7">
                                    <select class="form-control" name="category" id="category">
                                        <option value="">All</option>
                                        <?php foreach ($categories as $key) { ?>
                                        <option value="<?php echo $key['id']; ?>"><?php echo $key['category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 form-group row"> 
                                <label class="col-form-label col-md-5">Sub Category</label>
                                <div class="col-md-7">
                                    <select class="form-control" name="sub_category" id="sub_category">
                                        <option value="">All</option>
                                        <?php foreach ($sub_categories as $key) { ?>
                                        <option value="<?php echo $key['id']; ?>" ><?php echo $key['sub_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 form-group row">
                                <div class="col-md-2">
                                    <input type="checkbox" name="show_only_checked" id="show_only_checked">
                                </div> Show Checked
                            </div>
                            <div class="col-md-1 form-group row">
                                <button class="btn btn-primary" id="filter">Filter</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0" id="myTable">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="all_checkbox" id="all_checkbox"></th>
                                        <th>Product Title</th>
                                        <th>Price (₹)</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="btn btn-primary" id="add_multiple_products">Submit</div>
                    </div>
                </div>
            </div>          
        </div>

    </div>          
</div>
<!-- /Page Wrapper -->

<?php include(__DIR__."/../footer.php"); ?>

<script>
    $(document).ready(function(){
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax:{
                url: "<?php echo base_url('kids/get_items_you_may_like'); ?>",
                data: function (d) {
                    // d.keyword = $('input[name=keyword]').val();
                    d.show_only_checked = $('input[name=show_only_checked]').val();
                    d.category = $('select[name=category]').val();
                    d.sub_category = $('select[name=sub_category]').val();
                }
            },
            columns:[
                { data: 'id', name: 'id' },
                { data: 'product_title', name: 'product_title' },
                { data: 'product_price', name: 'product_price' },
                { data: 'category_name', name: 'category_name' },
                { data: 'sub_category_name', name: 'sub_category_name' }
            ]
        });

        $('#filter').on('click', function () {
            $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                searchable: true,
                "bDestroy": true,
                ajax:{
                    url: "<?php echo base_url('kids/get_items_you_may_like'); ?>",
                    data: function (d) {
                        // d.keyword = $('input[name=keyword]').val();
                        d.show_only_checked = $('input[name=show_only_checked]').val();
                        d.category = $('select[name=category]').val();
                        d.sub_category = $('select[name=sub_category]').val();
                    }
                },
                columns:[
                    { data: 'id', name: 'id', searchable: false, orderable: false },
                    { data: 'product_title', name: 'product_title' },
                    { data: 'product_price', name: 'product_price' },
                    { data: 'category_name', name: 'category_name' },
                    { data: 'sub_category_name', name: 'sub_category_name' }
                ]
            });
        });
    })
</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
$(document).ready(function(){
    $('body').on('change', '#all_checkbox', function(){
        if(this.checked){
            // $("input:checkbox[name=add_item]").prop('checked', true);
            Swal.fire({
                text: "Do you want to add all products?",
                icon: 'info',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Select all pages',
                cancelButtonText: 'Select this page'
            }).then((result) => {
                if (result.value) {
                    var add_all = "yes";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('kids/upload_items_you_may_like'); ?>",
                        data: { add_all: add_all },
                        success: function(response)
                        {
                            alert(response);
                            location.reload();
                        }
                    });
                }
            })
        }else{
            $("input:checkbox[name=add_item]").prop('checked', false);
        }
    })
})
</script>

<script>
    $(document).ready(function(){
        $('body').on('click', '#add_multiple_products', function(){
			var add_products_id = [];
            $("input:checkbox[name=add_item]:checked").each(function(){
				add_products_id.push($(this).val());
			});

			var remove_products_id = [];
            $("input:checkbox[name=add_item]:not(:checked)").each(function(){
				remove_products_id.push($(this).val());
			});
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('kids_controller/upload_items_you_may_like'); ?>",
				data: { add_products_id: add_products_id, remove_products_id: remove_products_id },
				success: function(response)
				{
                    alert(response);
				}
			});
        })
    })
</script>

<script>
    $(document).ready(function(){
        $('body').on('change', '#show_only_checked', function(){
            if(this.checked){
                $('#show_only_checked').val('1');
            }else{
                $('#show_only_checked').val('0');
            }
        })
    })
</script>