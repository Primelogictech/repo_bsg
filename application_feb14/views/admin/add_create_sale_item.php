<?php include('header.php'); ?>
			
<!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

    	<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-3">
					<h4 class="page-title">Create Sale Items List</h4>
				</div>
				<div class="col-6">
					<p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
        			<p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
				</div>
				<div class="col-3">
					<div class="float-right">
						<a href="<?php echo base_url('create_sale_item'); ?>" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /Page Header -->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form method="post" enctype="multipart/form-data">
							<input type="number" name="create_sale_id" id="create_sale_id" value="<?php if(isset($q)){ echo $q['id']; } ?>" hidden>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Name of the Sale Item List 
								<span class="mandatory"></span></label>

								<div class="col-md-9">
									<input type="text" class="form-control" id="name" name="name" value="<?php if(isset($q)){ echo $q['name']; } ?>" placeholder="Name of the Sale Item List">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Select Type</label>
								<div class="col-md-9 d-flex">
									<div class="radio" style="margin-right:30px">
										<label>
											<input type="radio" name="select_type" id="onCategory" value="category" <?php if(isset($q)){ if($q['type']=="category"){ echo "checked"; }} ?>> On Categories
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="select_type" id="onProducts" value="products" <?php if(isset($q)){ if($q['type']=="products"){ echo "checked"; }}else{ echo "checked"; } ?>> On Products
										</label>
									</div>
								</div>
							</div>
							<div class="form-group row select-main-category">
                                <label class="col-form-label col-md-3">Choose Main Category<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="main_category_id" name="main_category_id">
                                        <option value="">-- Select --</option>
                                        <?php foreach ($main_categories as $key) { ?>
                                        <option value="<?php echo $key['id']; ?>" <?php if(isset($q)){  if($q['main_category_id'] == $key['id']){ echo "selected"; }} ?>><?php echo $key['main_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row select-category">
                                <label class="col-form-label col-md-3">Select Category<span class="mandatory">*</span></label>
                                <div class="col-md-9">
                                    <select class="form-control" id="category_id" name="category_id">
    									<option value="">-- Select --</option>
    								</select>
                                </div>
                            </div>
							<div class="form-group row">
								<label class="col-form-label col-md-3">Discount Percentage <span class="mandatory"></span></label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="discount" name="discount" value="<?php if(isset($q)){ echo $q['discount']; } ?>" placeholder="Discount Percentage">
								</div>
							</div>
							
							<div class="text-right">
								<button type="submit" class="btn btn-primary" id="category-submit">Submit</button>
							</div>
						</form>
					</div>
				</div>
		
			</div>
		</div>
		
		<div class="col-sm-12 text-center text-success" id="alert_msg"></div>
		<!-- table -->
		<div class="row category-tbl" style="display: none;">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body">
                        
                        <div class="row col-12">
                            <div class="col-3">
                                Main Category
                                <div>
                                    <select id="main_cat_filter" name="main_cat_filter">
                                        <option value="">-- Select --</option>
                                        <?php foreach ($main_categories as $key) { ?>
                                        <option value="<?php echo $key['id']; ?>" ><?php echo $key['main_category_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-2">
                                Category
                                <div>
                                    <select id="cat_filter" name="cat_filter">
                                        <option value="">-- Select --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-1">
                                <button class="btn btn-primary" id="filter_products">Apply</button>
                            </div>
                        </div>

						<div class="table-responsive">
							<table class="table table-hover table-center mb-0" id="myTable">
								<thead>
									<tr>
										<th>Product ID</th>
										<th>Product Name</th>
										<th>Price</th>
										<th>Category</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="btn btn-primary" id="add_multiple_products">Submit</div>
					</div>
				</div>
			</div>			
		</div>
	</div>	
	<!-- End of Table -->
</div>
<!-- /Page Wrapper -->

		</div>
		<!-- /Main Wrapper -->

		<!-- jQuery -->
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery-3.2.1.min.js"></script>
	
		<!-- Bootstrap Core JS -->
        <script src="<?php echo base_url(); ?>assets/admin/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="<?php echo base_url(); ?>assets/admin/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="<?php echo base_url(); ?>assets/admin/js/script.js"></script>

		<script type="text/javascript">
		$(function(){
				var url = window.location.href;
				$('body').find('a[href="'+url+'"]').parents('.br-menu-item').addClass('active');
				$('body').find('a[href="'+url+'"]').parents('.br-menu-show').show();
			});
		</script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $("input[name=select_type]").change(function() {
		    if ($(this).val() == "category") {
		      	$('.select-main-category, .select-category').show();
		      	$('.category-tbl').css('display','none');
		    }						    
		    else {
		    	$('#main_category_id').val("");
    			$('#category_id').val("");
		      	$('.select-main-category, .select-category').hide();
		      	$('.category-tbl').css('display','none');
		    }
		});

	    $('#category-submit').click(function(e){
	    	e.preventDefault();
	    	var id =$('#create_sale_id').val();
	    	var name =$('#name').val();
	    	var type =$("input[name=select_type]:checked").val();
	    	var main_category_id =$('#main_category_id').val();
	    	var category_id = $('#category_id').val();
	    	var discount =$('#discount').val();
	        $.ajax({
	            url: "<?php echo base_url('upload_create_sale_item'); ?>",
	            data: { id:id, name:name, type:type, main_category_id:main_category_id, category_id:category_id, discount:discount },
	            type: 'POST',
	            dataType: 'html',
	            success: function(response) {
	            	$('#create_sale_id').val(response);
	            	$('#category-submit').attr('disabled', true);
	            }
	        });
	        $('#myTable').DataTable({
	            processing: true,
	            serverSide: true,
	            "bDestroy": true,
	            ajax:{
	                url: "<?php echo base_url('admin_controller/products_for_sale'); ?>",
	                data: { id:id, main_category_id:main_category_id, category_id:category_id }
	            },
	            columns:[
	            	{ data: 'id', name: 'id' },
	                { data: 'product_title', name: 'product_title' },
	                { data: 'product_price', name: 'product_price' },
	                { data: 'category_name', name: 'category_name' }
	            ]
	        });
	    	$('.category-tbl').css('display','block');
	    });
	});
</script>

<script>
    $(document).ready(function(){
        $('body').on('click', '#filter_products', function(){
            var id =$('#create_sale_id').val();
	    	var main_category_id =$('#main_category_id').val();
	    	var category_id = $('#category_id').val();
            var main_cat_filter =$('#main_cat_filter').val();
	    	var cat_filter = $('#cat_filter').val();
			$('#myTable').DataTable({
	            processing: true,
	            serverSide: true,
	            "bDestroy": true,
	            ajax:{
	                url: "<?php echo base_url('admin_controller/products_for_sale'); ?>",
	                data: { id:id, main_category_id:main_category_id, category_id:category_id,  main_cat_filter:main_cat_filter, cat_filter:cat_filter }
	            },
	            columns:[
	            	{ data: 'id', name: 'id' },
	                { data: 'product_title', name: 'product_title' },
	                { data: 'product_price', name: 'product_price' },
	                { data: 'category_name', name: 'category_name' }
	            ]
	        });
        })
    })
</script>

<script>
    $(document).ready(function(){
        $('body').on('click', '#add_multiple_products', function(){
			var add_products_id = [];
            $("input:checkbox[name=add_item]:checked").each(function(){
				add_products_id.push($(this).val());
			});

			var remove_products_id = [];
            $("input:checkbox[name=add_item]:not(:checked)").each(function(){
				remove_products_id.push($(this).val());
			});
			
			var id =$('#create_sale_id').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('admin_controller/upload_create_sale_items_products'); ?>",
				data: { add_products_id: add_products_id, remove_products_id: remove_products_id, id:id },
				success: function(response)
				{
					$('#alert_msg').html(response);
				}
			});
        })
    })
</script>

<script type="text/javascript">     
    $('body').on('change','#main_category_id',function(){ //alert();
        var main_category_id =$(this).val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
        var main_category_id = $('#main_category_id').val(); //alert(main_category_id);
        var category_id = "<?php if(isset($q)){ echo $q['category_id']; } ?>";
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id:main_category_id, category_id:category_id },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#category_id').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
	    if($('#onCategory').is(':checked')) {
	    	$('.select-main-category, .select-category').show();
	    	$('.category-tbl').css('display','none');
	    }
	    if($('#onProducts').is(':checked')) {
	    	$('.select-main-category, .select-category').hide();
	    	$('.category-tbl').css('display','none');
	    }
    });
</script>

<!-- FOR FILTERS -->
<script type="text/javascript">     
    $('body').on('change','#main_cat_filter',function(){ //alert();
        var main_cat_filter = $(this).val();
        $.ajax({
            url: "<?php echo base_url('product_controller/get_category_list'); ?>",
            data: { main_category_id: main_cat_filter },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#cat_filter').html(data);
            }
        });
    });
</script>

	</body>
</html>