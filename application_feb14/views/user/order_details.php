<?php include('header_no_menu.php'); ?>

<div class="page-wrapper ml-0 pt-0">
    <div class="content container-fluid pt-0">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <a onclick="history.go(-1);">
                    <div class="backbtn">
                        <i class="fas fa-chevron-left"></i>
                        <span>Order Details</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-md-10 offset-md-1">
            <div class="row">
                <div class="col-12 col-md-8 px-0">
                    <div class="order-block">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6 px-0">
                                    <div class="orders-title">Order Number</div>
                                    <div class="order-content">ORD-<?php echo isset($orders[0]['orderId']) ? $orders[0]['orderId'] : ""; ?></div>
                                </div>
                                <div class="col-6 px-0">
                                    <div class="orders-title text-right">Order Total</div>
                                    <div class="order-content text-right">Rs.<?php echo isset($orders[0]['order_total']) ? $orders[0]['order_total'] : ""; ?></div>
                                </div>
                                <div class="col-12 px-0">
                                    <div class="orders-title">Placed on <?php echo isset($orders[0]['placed_on']) ? date('M d Y', strtotime($orders[0]['placed_on'])) : ""; ?></div>
                                </div>
                            </div>
                        </div>
                        <hr class="px-0 my-2" />
                        <div class="order-items-block">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6 px-0">
                                        <div class="items-count">Items: <?php echo count($orders); ?></div>
                                    </div>
                                    <div class="col-6 px-0">
                                        <a href="<?php echo base_url('order_details'); ?>" class="cancel-order-btn">Cancel Order</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach($orders as $order){
                        $productInfo = json_decode($order['product_info'],true);
                        $productInfoData =  $this->vendor_model->get_single_data(array('id'=>$productInfo['product_id']), 'tbl_products'); ?>
                        <div class="order-details">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-3 col-md-2 px-0">
                                        <div class="item-image-block">
                                            <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $productInfo['color']; ?>" class="img-fluid mx-auto d-block" alt="">
                                        </div>
                                    </div>
                                    <div class="col-9 col-md-10 px8">
                                        <div class="my-1">
                                            <span>Jeans</span>
                                            <span class="float-right">Rs. <?php echo $order['product_price']; ?></span>
                                        </div>
                                        <div class="my-1">Size: <?php echo $order['size']; ?></div>
                                        <div class="my-1">Color: <img class="product_color_img color" src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $order['color']; ?>" class="img-fluid" alt="clr" /></div>
                                        <div class="my-1">Deliverd By: <?php echo date('M d Y', strtotime($order['placed_on'] . " + ".$productInfoData['estimated_delivery_days']. " days")); ?></div>
                                        <div><a href="<?php echo base_url('trackOrder'); ?>/<?php echo base64_encode($order['id']); ?>" class="track-order-btn btn">Track Order</a></div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <?php } ?>               
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="payment-section">
                                <div class="payment-title">Payment details</div>
            
                                <div class="payment-details">
                                    <div>
                                        <span>Sub Total</span>
                                        <span class="float-right">Rs.<?php echo isset($orders[0]['order_total']) ? $orders[0]['order_total'] : ""; ?></span>
                                    </div>
                                    <!-- <div>
                                        <span>Shipping Charges</span>
                                        <span class="float-right">Rs.70</span>
                                    </div>
                                    <div>
                                        <span>Taxes</span>
                                        <span class="float-right">0</span>
                                    </div>
                                    <div>
                                        <span>Discount</span>
                                        <span class="float-right">Rs.20</span>
                                    </div> -->
                                    <div>
                                        <span>Grand Total</span>
                                        <span class="float-right">Rs.<?php echo isset($orders[0]['order_total']) ? $orders[0]['order_total'] : ""; ?></span>
                                    </div>
                                    <div>
                                        <span>Payment Method</span>
                                        <span class="float-right"><?php echo isset($orders[0]['payment_method']) ? $orders[0]['payment_method'] : ""; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
