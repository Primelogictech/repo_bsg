<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8" />
        <meta name="description" content="BSG Garments" />
        <meta name="keywords" content="BSG Garments, unica, creative, html" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>BSG Garments</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@300;400;600;700;800;900&display=swap" rel="stylesheet" />

        <!-- Css Styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/elegant-icons.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/magnific-popup.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/owl.carousel.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/slicknav.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/custom.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/user/css/ecomm.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" />
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Offcanvas Menu Begin -->
        <div class="offcanvas-menu-overlay"></div>
        <div class="offcanvas-menu-wrapper">
            <div class="offcanvas__option">
                <div class="offcanvas__links">
                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a><?php }else{ ?>
                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                    <?php } ?>
                </div>
            </div>
            <div class="offcanvas__nav__option">
                <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                <a class="text-dark" href="<?php echo base_url('my_orders'); ?>">My Orders</a>
                <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a>
                <?php }  ?>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
        <!-- Offcanvas Menu End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-7">
                            <div class="header__top__left">
                                <p>Free shipping, 30-day return or refund guarantee.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-5">
                            <div class="header__top__right">
                                <div class="header__top__links">
                                    <?php if(($this->session->userdata("id") != "") && ($this->session->userdata("role") == "User")){ ?>
                                    <a href="#">Hello, <?php echo $this->session->userdata('name'); ?></a>
                                    <a class="text-danger" href="<?php echo base_url('user_logout'); ?>">Log Out</a><?php }else{ ?>
                                    <a href="<?php echo base_url('login'); ?>">Sign in</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="header__logo">
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/images/logo.png" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                    </div>
                    <div class="col-lg-3 col-md-3">
                    </div>
                </div>
                <div class="canvas__open"><i class="fa fa-bars"></i></div>
            </div>
        </header>
        <!-- Header Section End -->
                <div class="main__wrapper">