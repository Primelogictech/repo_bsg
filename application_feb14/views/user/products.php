<?php include('header.php'); ?>

<style>
    ::-webkit-input-placeholder { 
        color: #5d5e5c !important;
    }
</style>

<div class="product_filters">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-12 col-sm-6 col-md-3 col-lg-2 my-1 mob-px15">
                <div><label>Price</label></div>
                <div class="row">
                    <div class="col-6 px5 mob-px15">
                        <div><input type="number" placeholder="Min-Price" name="min_price_filter" id="min_price_filter" class="filters-styles" min="0"></div>
                    </div>
                    <div class="col-6 px5 mob-px15">
                        <div><input type="number" placeholder="Max-Price" name="max_price_filter" id="max_price_filter" class="filters-styles" min="0"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-2 my-1 mob-px15">
                <div><label class="pl7">Size</label></div>
                <select name="size_filter" id="size_filter" class="filters-styles">
                    <option value="">All</option>
                    <option value="S">S</option>
                    <option value="M">M</option>
                    <option value="L">L</option>
                    <option value="XL">XL</option>
                    <option value="XXL">XXL</option>
                </select>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-2 my-1 mob-px15">
            <div><label class="pl7">Sleeve Length</label></div>
                <select name="sleeves" id="sleeves" class="filters-styles">
                    <?php $sleeves = Orders::getSleeves(); ?>
                    <option value="">All</option>
                    <?php foreach($sleeves as $key=>$val){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-2 my-1 mob-px15">
               <div><label class="pl7">Style / Type</label></div>
                <select name="type" id="type" class="filters-styles">
                <?php $sleeves = Orders::getType(); ?>
                    <option value="">All</option>
                    <?php foreach($sleeves as $key=>$val){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-2 px5 my-1 mob-px15">
               <div><label class="pl7">Design</label></div>
                <select name="design" id="design" class="filters-styles">
                <?php $sleeves = Orders::getDesign(); ?>
                    <option value="">All</option>
                    <?php foreach($sleeves as $key=>$val){ ?>
                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-sm-6 col-md-2 col-lg-1 px5 my-auto mob-px15">
                <button class="btn btn-primary mt25 pot-mt10" id="filter_products">Apply</button>
            </div>
        </div>
        <input type="hidden" name="cid" id="cid" value="<?php if(isset($cid) && !empty($cid)){ echo $cid; } ?>">
        <input type="hidden" name="sid" id="sid" value="<?php if(isset($sid) && !empty($sid)){ echo $sid; } ?>">
        <input type="hidden" name="search_keyword" id="search_keyword" value="<?php if(isset($search_keyword) && !empty($search_keyword)){ echo $search_keyword; } ?>">
        <div class="row" id="products_block">
        </div>
    </div>
</div>

<?php include('footer.php'); ?>

<script>
    $(document).ready(function(){
        $(".wishlist").click(function(){
            var productId = $(this).attr('data-id');
            var wish = $(this).attr('data-wishlist');
            var user_id = '<?php echo $this->session->userdata('id'); ?>';
                if (user_id != "") {
                    if (wish == 0){
                        $.ajax({
                            type:'post',
                            url:'<?php echo base_url('add_wishlist'); ?>',
                            data:{'productId':productId},
                            success: function(data){
                                alert(data);
                            }
                        });
                        $(this).css("color", "red");
                        $(this).attr('data-wishlist',"1");
                    }else{
                        $.ajax({
                            type:'post',
                            url:'<?php echo base_url('remove_wishlist'); ?>',
                            data:{'productId':productId},
                            success: function(data){
                                alert(data);
                            }
                        });
                        $(this).css("color", "black");
                        $(this).attr('data-wishlist',"0");
                    }
                }else {
                    alert("Please Login to add to Wishlist");
                }
        });
    });
</script>

<script type="text/javascript">     
    $('body').on('click','#filter_products', function(){
        var cid = $('#cid').val();
        var sid = $('#sid').val();
        var search_keyword = $('#search_keyword').val();
        var min_price_filter =$('#min_price_filter').val();
        var max_price_filter =$('#max_price_filter').val();
        var sleeves =$('#sleeves').val();
        var type =$('#type').val();
        var design =$('#design').val();
        var size_filter =$('#size_filter').val();
        $.ajax({
            url: "<?php echo base_url('user_controller/get_products_ajax'); ?>",
            data: { cid:cid, sid:sid, search_keyword:search_keyword, min_price_filter:min_price_filter, max_price_filter:max_price_filter, sleeves:sleeves, type:type,design:design, size_filter:size_filter },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#products_block').html(data);
            }
        });
    });
</script>

<script type="text/javascript">
    $(window).on('load', function(){
        var cid = $('#cid').val();
        var sid = $('#sid').val();
        var search_keyword = $('#search_keyword').val();
        $.ajax({
            url: "<?php echo base_url('user_controller/get_products_ajax'); ?>",
            data: { cid: cid, sid: sid, search_keyword:search_keyword },
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#products_block').html(data);
            }
        });
    });
</script>