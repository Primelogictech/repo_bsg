<?php include('header.php'); ?>

<div class="page-wrapper ml-0 pt-0 mb-0">
	<div class="content container-fluid pt-0">
		<div class="row">
			<div class="col-12">
				<div class="mt-5 pt-5">
					 <img src="<?php echo base_url(); ?>uploads/images/my-cart.png" class="img-fluid mx-auto d-block" alt="" />
				</div>
				<div class="text-center mt-5 mb-5">
					<div class="cart_mpty">Your Cart is Empty</div>
					<p class="text-secondary cart_mpty pt-1">But It doesn't have to be</p>
				</div>
				<div class="text-center mb-5">
					<a href="<?php echo base_url(); ?>"><input type="button" class="btn btn-primary" value="SHOP NOW"></a>
				</div>
			</div>
		</div>

	</div>
</div>

<?php include('footer.php'); ?>