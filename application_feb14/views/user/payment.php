<?php include('header_no_menu.php'); ?>

<style type="text/css">
    button.float-right {
        background-color: transparent;
        border: 0px;
        padding: 0px;
    }
</style>
<?php echo form_open('payment'); ?>
<div class="page-wrapper pt-0 ml-0 pb-0">
    <div class="content container-fluid pt-0">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <a onclick="history.go(-1);">
                    <div class="backbtn">
                        <i class="fas fa-chevron-left"></i>
                        <span class="fs20">Payment</span>
                    </div>
                </a>
            </div>
        </div>

        <!-- Categories -->

        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 my-3">
                <div class="">
                    <div class="block-main-title">Select Payment Method</div>
                    <div class="payment-method-section">
                        <div class="payment-category-block">
                            <div class="pm-title">
                                <img src="<?php echo base_url(); ?>uploads/images/icons/debit-card.png" class="img-fluid" alt="debit-card" width="32px" /><span class="payment-method-name">ONLINE (Credit/Debit/UPI)</span>
                                <input required type="radio" name="paymentType" value="online">
                            </div>
                        </div>
                        <div class="payment-category-block">
                            <div class="pm-title">
                                <img src="<?php echo base_url(); ?>uploads/images/icons/bhim.png" class="img-fluid" alt="debit-card" width="32px" /><span class="payment-method-name"><span>CASH ON DELIVERY</span>
                                <input required type="radio" name="paymentType" value="cod">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo $payment['payment']; ?>" name="payment">
                    <input type="hidden" value="deliveryCharges" name="address">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="shipping-details">
                    <div class="head">
                        <div class="title">SHIPPING ADDRESS</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 offset-md-2">
                <div class="user_shipping_details">
                        <p class="mb-0"><?php echo $address['full_name']; ?></p>
                        <p class="shippig-address mb-0">
                        <?php echo $address['landmark']; ?>, <?php echo $address['address_lane']; ?>, <?php echo $address['city']; ?>, <?php echo $address['state']; ?> - <?php echo $address['zip']; ?>
                    </p>
                    <p class="shipping_mobile_number mb-0">
                        <span><?php echo $address['mobile']; ?></span>
                    </p>
                    </div>
                    
            </div>
        </div>
        <?php 
        $payment = json_decode(base64_decode($payment['payment']),true); ?>
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2">
                <div class="price-summary-block">
                    <div class="head">
                        <div class="title">PRICE SUMMARY</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 offset-md-2 mt10">
                <div class="price-summary-details">
                    <div class="row mb5">
                        <div class="col-7">
                            <div>Bag Total</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right">₹ <?php echo base64_decode(base64_decode($payment['price'])); ?></div>
                        </div>
                    </div>
                    <div class="row mb5">
                        <div class="col-7">
                            <div>Bag Discount</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right disc">-₹ <?php echo base64_decode(base64_decode($payment['discount'])); ?></div>
                        </div>
                    </div>
                    <div class="row mb5">
                        <div class="col-7">
                            <div>Sub Total</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right disc">₹ <?php echo base64_decode(base64_decode($payment['price'])); ?></div>
                        </div>
                    </div>
                    <!-- <div class="row mb5">
                        <div class="col-7">
                            <div>Offer discount</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right apply-coupon">Apply Coupon</div>
                        </div>
                    </div> -->
                    <div class="row mb5">
                        <div class="col-7">
                            <div>Shipping Charges</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right">₹ <?php echo base64_decode(base64_decode($payment['deliveryCharges'])); ?></div>
                        </div>
                    </div>
                    <hr>
                    <div class="row mb5">
                        <div class="col-7">
                            <div class="font-weight-bold">Grand total</div>
                        </div>
                        <div class="col-5">
                            <div class="text-right font-weight-bold">₹ <?php echo base64_decode(base64_decode($payment['price'])); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 py-4">
                <div class="payment-secure">
                    <div>Safe Secure Payments | 100% Authentic Products</div>
                </div>
            </div>
        </div>
        <div class="payment-continue-btn mb-5">
                <div class="col-12 col-md-8 offset-md-2 px-0">
                    <div class="cart-continue-btn">
                    <input type="submit" class="cart-continue-btn" value="Checkout">
                    </div>
                </div>
            </div>
    </div>
</div>
<?php echo form_close(); ?>

<?php include('footer.php'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#debit_card_hide").click(function () {
            $(".debit_card_cont").hide(1000);
            $("#debit_card_hide").hide();
            $("#debit_card_show").show();
        });
        $("#debit_card_show").click(function () {
            $(".debit_card_cont").show(1000);
            $(".upi_bhim_cont").hide(1000);
            $(".google_pay_cont").hide(1000);
            $(".net_banking_cont").hide(1000);
            $("#debit_card_show").hide();
            $("#debit_card_hide").show();
        });
    });
    $(document).ready(function () {
        $("#upi_bhim_hide").click(function () {
            $(".upi_bhim_cont").hide(1000);
            $("#upi_bhim_hide").hide();
            $("#upi_bhim_show").show();
        });
        $("#upi_bhim_show").click(function () {
            $(".debit_card_cont").hide(1000);
            $(".upi_bhim_cont").show(1000);
            $(".google_pay_cont").hide(1000);
            $(".net_banking_cont").hide(1000);
            $("#upi_bhim_show").hide();
            $("#upi_bhim_hide").show();
        });
    });
    $(document).ready(function () {
        $("#google_pay_hide").click(function () {
            $(".google_pay_cont").hide(1000);
            $("#google_pay_hide").hide();
            $("#google_pay_show").show();
        });
        $("#google_pay_show").click(function () {
            $(".google_pay_cont").show(1000);
            $(".debit_card_cont").hide(1000);
            $(".upi_bhim_cont").show(1000);
            $(".net_banking_cont").hide(1000);
            $("#google_pay_show").hide();
            $("#google_pay_hide").show();
        });
    });
    $(document).ready(function () {
        $("#net_banking_hide").click(function () {
            $(".net_banking_cont").hide(1000);
            $("#net_banking_hide").hide();
            $("#net_banking_show").show();
        });
        $("#net_banking_show").click(function () {
            $(".net_banking_cont").show(1000);
            $(".debit_card_cont").hide(1000);
            $(".upi_bhim_cont").show(1000);
            $(".google_pay_cont").hide(1000);
            $(".net_banking_cont").hide(1000);
            $("#net_banking_show").hide();
            $("#net_banking_hide").show();
        });
    });
</script>
