<?php include('header.php'); ?>

<!-- Top Carousel Begin -->
<?php if(isset($top_banners) && !empty($top_banners)){ ?>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php $i=0; foreach($top_banners as $value){ ?>
        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0){ echo 'active'; } ?>"></li>
        <?php $i++; } ?>
    </ol>
    <div class="carousel-inner">
        <?php $i=0; foreach($top_banners as $value){ ?>
        <div class="carousel-item main-banner-carousel-item <?php if($i==0){ echo 'active'; } ?>">
            <a href="<?php echo base_url('products'); ?>?sid=<?php echo base64_encode($value['linked_to']); ?>">
                <img class="d-block img-fluid w-100" src="<?php echo base_url(); ?>uploads/images/top_banners/<?php echo $value['banner_image']; ?>" alt="First slide" />
            </a>
        </div>
        <?php $i++; } ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" style="background-image: url('<?php echo base_url(); ?>uploads/images/icon/left-arrow.png');" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" style="background-image: url('<?php echo base_url(); ?>uploads/images/icon/right-arrow.png');" aria-hidden="true"> </span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php } ?>
<!-- Top Carousel End -->

<!-- Categories Section Begin -->
<?php if(isset($categories) && !empty($categories)){ ?>
<section>
    <div class="container-fluid">
        <h3 class="tittle">Shop By Category</h3>
        <div class="row">
            <?php foreach($categories as $value){ ?>
            <div class="col-6 col-sm-4 col-md-3 px-0">
                <a href="<?php echo base_url('products'); ?>?cid=<?php echo base64_encode($value['id']); ?>" class="text-dark">
                    <div class="category-block">
                        <div class="product__image">
                            <img src="<?php echo base_url(); ?>uploads/images/category_images/<?php echo $value['category_image']; ?>" class="img-fluid" alt="" />
                        </div>
                        <div class="category-name"><?php echo $value['category_name']; ?></div>
                        <!--<p class="text-center">Starts from Rs.199/-</p>-->
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<!-- Categories Section End -->

<!-- Recently Viewed Section Begin -->
<?php if($this->session->userdata('id') != '' && $this->session->userdata('role') == 'User'){ if(isset($recently_viewed) && !empty($recently_viewed)){ ?>
<section>
    <div class="container-fluid">
        <h3 class="tittle">Recently Viewed</h3>
        <div class="row horizontal-scroll">
            <?php foreach($recently_viewed as $value){ ?>
            <div class="col-6 col-sm-4 col-md-2 px-0">
                <a href="<?php echo base_url('product_details'); ?>?pid=<?php echo base64_encode($value['id']); ?>" class="text-dark">
                    <div class="direct-item-block">
                        <div class="product__image">
                            <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $value['banner_image']; ?>" class="img-fluid" alt="" />
                        </div>
                        <div class="direct-image_product-prize">₹ <?php echo $value['product_price']; ?></div>
                        <div class="direct-image_product-name"><?php echo $value['product_title']; ?></div>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php }} ?>
<!-- Recently Viewed Section End -->

<!-- Newly Added Begin -->
<?php if(isset($newly_added_products) && !empty($newly_added_products)){ ?>
<section>
    <div class="container-fluid">
       <h3 class="tittle">New Collection</h3>
        <div class="row horizontal-scroll">
            <?php foreach($newly_added_products as $value){ ?>
            <div class="col-6 col-sm-4 col-md-2 px-0">
                <a href="<?php echo base_url('product_details'); ?>?pid=<?php echo base64_encode($value['id']); ?>" class="text-dark">
                    <div class="direct-item-block">
                        <div class="product__image">
                            <img src="<?php echo base_url(); ?>uploads/images/product_images/<?php echo $value['banner_image']; ?>" class="img-fluid" alt="" />
                        </div>
                        <div class="direct-image_product-prize">₹ <?php echo $value['product_price']; ?></div>
                        <div class="direct-image_product-name"><?php echo $value['product_title']; ?></div>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<!-- Newly Added End -->

<?php include('footer.php'); ?>