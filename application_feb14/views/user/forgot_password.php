<?php include('header_no_menu.php'); ?>

    <div class="main-wrapper">
        <div class="page-wrapper loggin-wrapper">
            <div class="content container-fluid px-0">
                <div class="row">
                    <div class="login-bg col-12 col-md-6 offset-md-3 mb-5">
                        <div class="col-12">
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/images/logo.jpg" class="img-fluid mx-auto d-block" alt="" width="60px" /></a>
                            <div class="text-center pt20 font-weight-bold">Please enter below fields to continue</div>
                        </div>
                        <div class="col-12">
                            <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                            <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                        </div>
                        <div class="col-12 col-md-12 py-3">
                            <div class="login">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('forgot_password_update'); ?>">
                                    <div class="form-group">
                                        <input type="number" min="0" class="in" name="mobile_number" id="mobile_number" placeholder="Mobile Number *" />
                                        <div id="mobile_error" style="color: red; display: none; font-size: 12px;">Mobile Number is required</div>
                                    </div>
        
                                    <div class="form-group">
                                        <div class="text-center"><a class="btn btn-sm btn-primary" id="send_otp">Get OTP <img id="otp_loader" src="<?php echo base_url() ?>uploads/images/loader.gif" width="6%" style="display: none;"></a></div>
                                        <div class="text-center text-danger" id="send_otp_error"></div>
                                    </div>
        
                                    <div class="form-group">
                                        <input type="text" placeholder="OTP *" class="in" name="otp" id="otp" autocomplete="off" />
                                        <div id="otp_error" style="color: red; display: none; font-size: 12px;">OTP is required</div>
                                    </div>
            
                                    <div class="form-group">
                                        <input type="password" class="in" name="password" id="password" placeholder="Enter New Password *" />
                                        <div id="password_error" style="color: red; display: none; font-size: 12px;"></div>
                                    </div>
            
                                    <div class="form-group">
                                        <input type="password" class="in" name="re_password" id="re_password" placeholder="Re Enter New Password *" />
                                        <div id="re_password_error" style="color: red; display: none; font-size: 12px;"></div>
                                    </div>
            
                                    <div class="form-group">
                                        <div class="col-sm-12 text-center px-0">
                                            <button type="submit" id="submit_btn" class="btn btn-white login-btn">Submit <img id="loader" src="<?php echo base_url() ?>uploads/images/loader.gif" width="6%" style="display: none;"></button>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-group">
                                    <div class="col-sm-12 text-center px-0"><span class="text-dark">Don't have an account?</span> <a href="<?php echo base_url('registration'); ?>" class="text-violet">Sign Up</a></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-center px-0"><a href="<?php echo base_url('login'); ?>" class="text-violet">Go back to Login</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $("form").on('submit', function(e){
        e.preventDefault();
        var mobile_number = $('#mobile_number').val();
        var otp = $('#otp').val();
        var password = $('#password').val();
        var re_password = $('#re_password').val();

        if(mobile_number == ""){
            $('#mobile_error').show();
            return false;
        }else{
            $('#mobile_error').hide();
        }
        if(otp == ""){
            $('#otp_error').show();
            return false;
        }else{
            $('#otp_error').hide();
        }
        if(password == ""){
            $('#password_error').html('Password is required');
            $('#password_error').show();
            return false;
        }else{
            $('#password_error').hide();
        }
        if(re_password == ""){
            $('#re_password_error').html('Re Enter Password is required');
            $('#re_password_error').show();
            return false;
        }else{
             $('#re_password_error').hide();
        }
        if(password != re_password){
            $('#re_password_error').html('Password you re-entered is incorrect');
            $('#re_password_error').show();
            return false;
        }else{
            $('#re_password_error').hide();
        }
        if(mobile_number != "" && otp != "" && password != "" && re_password != ""){
            $('#loader').show();
            $(this).unbind('submit').submit();
        }
    });
</script>

<script type="text/javascript">     
    $('body').on('click', '#send_otp', function(){
        var forgot_password = "yes";
        var mobile_number = $('#mobile_number').val();
        if(mobile_number != ''){
            $('#otp_loader').show();
            $('#mobile_error').hide();
            $.ajax({
                url: "<?php echo base_url('send_otp'); ?>",
                data: { forgot_password: forgot_password, mobile_number: mobile_number },
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    $('#otp_loader').hide();
                    $('#send_otp_error').html(data);
                }
            });
        }else{
            $('#mobile_error').show();
        }
    });
</script>

<script type="text/javascript">     
    $('body').on('keyup', '#otp', function(){
        var mobile_number = $('#mobile_number').val();
        var otp = $('#otp').val();
        if(mobile_number != '' && otp != ''){
            $.ajax({
                url: "<?php echo base_url('user_controller/verify_otp'); ?>",
                data: { mobile_number: mobile_number, otp: otp },
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                    if(data == 'success'){
                        $('#otp_error').hide();
                        $('#submit_btn').prop('disabled', false);
                    }else if(data == 'failed'){
                        $('#otp_error').html('Incorrect OTP');
                        $('#otp_error').show();
                        $('#otp').focus();
                        $('#submit_btn').prop('disabled', true);
                    }
                }
            });
        }
    });
</script>