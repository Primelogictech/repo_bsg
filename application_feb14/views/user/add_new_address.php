<?php include('header.php'); ?>

<section class="checkout-section">
	<div class="container-fluid">
		<div class="col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1">
			<div class="row">
				<div class="col-12 col-md-7 col-lg-7 sm-px0 order-sm-1 order-2">
					<div class="addressList-base-addBlock">
						<div class="addressList-base-label">Add New Address</div>
						<div class="address__block">
							<form class="form-horizontal" method="post" action="<?php echo base_url('saveAddress'); ?>">
								<input type="hidden" name="id" value="<?php if(!empty($address['id'])){ echo $address['id'];} ?>">
                    			<input type="hidden" name="payment" value="<?php if(!empty($payment)){ echo $payment;} ?>">
								<div class="form-group">
									<input type="text" class="in" name="full_name" value="<?php if(!empty($address['full_name'])){ echo $address['full_name'];} ?>" required="required"/>
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>Name *</label>
								</div>
								<div class="form-group">
									<input type="tel" class="in" name="mobile" value="<?php if(!empty($address['mobile'])){ echo $address['mobile'];} ?>" required="required"/>
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>Mobile Number *</label>
								</div>
								<div class="addressForm-base-formHeader">ADDRESS</div>
								<div class="form-group">
									<input type="tel" class="in" name="zip" value="<?php if(!empty($address['zip'])){ echo $address['zip'];} ?>" required="required"/>
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>Pin Code *</label>
								</div>
								<div class="form-group">
									<input type="text" class="in" name="address_lane" value="<?php if(!empty($address['address_lane'])){ echo $address['address_lane'];} ?>" required="required"/>
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>Address (House No, Building, Street, Area) *</label>
								</div>
								<div class="form-group">
									<input type="text" class="in" name="landmark" value="<?php if(!empty($address['landmark'])){ echo $address['landmark'];} ?>" required="required"/>
									<span class="highlight"></span>
									<span class="bar"></span>
									<label>Land Mark *</label>
								</div>
								<div class="row">
									<div class="form-group col-12 col-sm-6">
										<select class="in" name="city" required>
											<option value="">City / District *</option>
											<option value="Hyderabad" <?php if(!empty($address['city'])){ if($address['city'] == "Hyderabad"){ echo "selected"; }} ?>>Hyderabad</option>
											<option value="Karimnagar" <?php if(!empty($address['city'])){ if($address['city'] == "Karimnagar"){ echo "selected"; }} ?>>Karimnagar</option>
											<option value="Vijayawada" <?php if(!empty($address['city'])){ if($address['city'] == "Vijayawada"){ echo "selected"; }} ?>>Vijayawada</option>
											<option value="Amaravathi" <?php if(!empty($address['city'])){ if($address['city'] == "Amaravathi"){ echo "selected"; }} ?>>Amaravathi</option>
										</select>
									</div>
									<div class="form-group col-12 col-sm-6">
										<select class="in" name="state" required>
											<option value="">State *</option>
											<option value="Telangana" <?php if(!empty($address['state'])){ if($address['state'] == "Telangana"){ echo "selected"; }} ?>>Telangana</option>
											<option value="Andhra Pradesh" <?php if(!empty($address['state'])){ if($address['state'] == "Andhra Pradesh"){ echo "selected"; }} ?>>Andhra Pradesh</option>
										</select>
									</div>
								</div>
								<div class="addressForm-base-formHeader">SAVE ADDRESS AS</div>
								<div class="addressForm-base-addressTypes">
									<input type="radio" name="address_type" id="address_type_input" value="HOME" <?php if(!empty($address['address_type'])){ if($address['address_type'] == "HOME"){ echo "checked"; }} ?>>Home 
									<input type="radio" name="address_type" id="address_type_input" value="WORK" <?php if(!empty($address['address_type'])){ if($address['address_type'] == "WORK"){ echo "checked"; }} ?>>Work
									<!-- <button class="addressForm-base-addressTypeIcon addressForm-base-selectedAddressType address_type" data-name="HOME">Home</button>
									<button class="addressForm-base-addressTypeIcon address_type" data-name="WORK">Work</button> -->
								</div>
								<div class="addressbase__text">
									<input type="checkbox" name="make_defalut" <?php if(!empty($address['primary_address'])){ if($address['primary_address'] == 1){ echo "checked"; }} ?>>
									<span class="addressForm-base-defaultAddress">Make this my default address</span>
								</div>
								<div class="addressbase__button">
									<button type="submit" class="addressbase_addaddress_button">ADD ADDRESS</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>

<!-- <script>
    $(document).ready(function(){
        $(".address_type").click(function(){
            var name = $(this).data('name');
            $("#address_type_input").val(name);
        });
    });
</script> -->