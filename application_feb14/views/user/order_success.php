<?php include('header.php'); ?>

<style type="text/css">
    i.fa.fa-check{
        color: #fff;
        line-height: 100px;
        font-size: 45px;
        position: absolute;
        left: 43%;
        top: 0px;
    }
   
</style>

<div class="page-wrapper ml-0 pt-0 mb-0">
    <div class="content container-fluid pt-0">
        <div class="row">
            <div class="order-success-styles">
                <div class="order-success-section">
                    <div class="success-img">
                        <div class="checkmark-block"><i class="fa fa-check"></i></div>
                    </div>
                    <div class="success-msg">Your Order is Successfully Completed.</div>
                    <div class="track-msg">You Can track the delivery in the <strong>"My Orders"</strong> section.</div>
                    <div class="continue-shop-btn"><a href="<?php echo base_url('home'); ?>">Continue Shopping</a></div>
                    <div  class="go-orders"><a href="<?php echo base_url('my_orders'); ?>">Go To Orders</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
