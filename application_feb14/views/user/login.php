<?php include('header_no_menu.php'); ?>

    <div class="main-wrapper">
        <div class="page-wrapper loggin-wrapper">
            <div class="content container-fluid px-0">
                <div class="row">
                    <div class="login-bg col-12 col-md-4 offset-md-4 mb-5">
                        <div class="col-12">
                            <div class="text-center pt20 font-weight-bold">Please SignIn to Continue</div>
                        </div>
                        <div class="col-12">
                            <p style="color: green;" class="text-center"><?php echo $this->session->flashdata('success'); ?></p>
                            <p style="color: red;" class="text-center"><?php echo $this->session->flashdata('danger'); ?></p>
                        </div>
                        <div class="col-12 py-3">
                            <div class="login">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('verify_user'); ?>">
                                    <div class="form-group">
                                        <input type="text" placeholder="Email / Mobile Number *" class="in" name="email_mobile_number" id="email_mobile_number" />
                                        <div id="email_mobile_number_error" style="color: red; display: none; font-size: 12px;">Email / Mobile number is required</div>
                                    </div>
            
                                    <div class="form-group">
                                        <input type="password" placeholder="Password *" class="in" name="password" id="password" />
                                        <div id="password_error" style="color: red; display: none; font-size: 12px;">Password is required</div>
                                    </div>
            
                                    <div class="col-sm-12 mb35 px-0">
                                        <div class="text-right">
                                            <a href="<?php echo base_url('forgot_password'); ?>" class="text-violet">Forgot Password ?</a>
                                        </div>
                                    </div>
            
                                    <div class="form-group">
                                        <div class="col-sm-12 text-center px-0">
                                            <button type="submit" class="btn btn-white login-btn">Sign In <img id="loader" src="<?php echo base_url() ?>uploads/images/loader.gif" width="4%" style="display: none;"></button>
                                        </div>
                                    </div>
            
                                    <div class="form-group">
                                        <div class="col-sm-12 text-center px-0"><span class="text-dark">Don't have an account?</span> <a href="<?php echo base_url('registration'); ?>" class="text-violet">Sign Up</a></div>
                                    </div>
                                </form>
                                <div class="form-group">
                                    <div class="col-sm-12 text-center px-0"><a href="<?php echo base_url('home'); ?>" class="text-violet">Go back to Home</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $("form").on('submit', function(e){
        e.preventDefault();
        var email_mobile_number = $('#email_mobile_number').val();
        var password = $('#password').val();

        if(email_mobile_number == ""){
            $('#email_mobile_number_error').show();
            return false;
        }else{
            $('#email_mobile_number_error').hide();
        }
        if(password == ''){
            $('#password_error').show();
            return false;
        }else{
            $('#password_error').hide();
        }
        if(email_mobile_number != "" && password != ""){
            $('#loader').show();
            $(this).unbind('submit').submit();
        }
    });
</script>