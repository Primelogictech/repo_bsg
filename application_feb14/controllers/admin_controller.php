<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('orders');
		$this->load->model('vendor_model');	
	}
	
	public function index($value='')
	{
		if($this->session->userdata("id")!="" && $this->session->userdata("role")=="Admin")
		{
			redirect('dashboard');
		}
		$this->load->view('admin/login');
	}

	public function verify_login()
	{
		$email_mobile_number = $this->input->post('email');
		$password = $this->input->post('password');
		
		if(!empty($email_mobile_number) && !empty($password)){
			$email_check = $this->vendor_model->get_single_data(array('email'=>$email_mobile_number), 'tbl_users');
			$mobile_number_check = $this->vendor_model->get_single_data(array('mobile_number'=>$email_mobile_number), 'tbl_users');
			if(empty($email_check) && empty($mobile_number_check)){
			    $this->session->set_flashdata('danger', 'Invalid Details');
			    redirect('admin');
			}else{
				if(!empty($email_check)){
					$hash = $email_check['password'];
				}else{
					$hash = $mobile_number_check['password'];
				}
				if($email_check['role'] == "Admin" || $mobile_number_check['role'] == "Admin"){
					if(password_verify($password, $hash)){
						if(!empty($email_check)){
							$user_data = [
		    					"id" => $email_check['id'],
		    					"name" => $email_check['name'],
		    					"email" => $email_check['email'],
		    					"role" => $email_check['role']
		    				];
		    			}else{
		    				$user_data = [
		    					"id" => $mobile_number_check['id'],
		    					"name" => $mobile_number_check['name'],
		    					"email" => $mobile_number_check['email'],
		    					"role" => $mobile_number_check['role']
		    				];
		    			}
						$this->session->set_userdata($user_data);
						redirect('dashboard');
					}else{
						$this->session->set_flashdata('danger','Invalid Password');
				    	redirect('admin');
					}
				}else{
					$this->session->set_flashdata('danger','You do not have permission to login');
				    redirect('admin');
				}
			}
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
    		redirect('admin');
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin', 'refresh');
	}

	public function dashboard()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$this->load->view('admin/index');
	}

	public function categories()
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
		$this->load->view('admin/categories', $data);
	}
	public function add_category($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_categories');
		}
		$this->load->view('admin/add_category', $data);
	}
	public function upload_category($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$category_name = $this->input->post('category_name', TRUE);
		$main_category_id = $this->input->post('main_category_id');

		$vendor_data = array('category_name'=>$category_name, 'main_category_id'=>$main_category_id, 'created_at'=>date('Y-m-d H:i:s'));

		if(isset($_FILES['category_image']['name']) && !empty($_FILES['category_image']['name']))
		{
			if(!empty($id)){
				$delete_image = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_categories');
				if(!empty($delete_image['category_image'])){
					unlink("uploads/images/category_images/".$delete_image['category_image']);
				}
			}
        	$config['upload_path'] = 'uploads/images/category_images/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['file_name'] = strtolower($category_name).'_'.date("Y_m_d_H_i_s");
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('category_image'))
	        {
	        	$error =  $this->upload->display_errors();
	        	$this->session->set_flashdata('danger', $error);
	        	redirect('add_category');
	        }
	        else
	        {
	        	$imageData = $this->upload->data();
	            $category_image =  $imageData['file_name'];
	            $vendor_data = array_merge($vendor_data, ['category_image'=>$category_image]);
	        }
        }
        
		if(!empty($category_name) && !empty($main_category_id)){
			if(!empty($id)){
        		$data= $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_categories');
        		$this->session->set_flashdata('success','Category updated Successfully');
        	}else{
        		$data= $this->vendor_model->insert_data($vendor_data,'tbl_categories');
        		$this->session->set_flashdata('success','Category added Successfully');
        	}
        	redirect('categories');
		}else{
			$this->session->set_flashdata('danger', 'Please input all fields');
			redirect('add_category');
		}
	}
	public function delete_category($id='')
	{
		if($this->session->userdata("id")=="" || $this->session->userdata("role")!="Admin")
		{
			redirect('admin');
		}
		$delete_cat_image = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_categories');

		if(!empty($delete_cat_image['category_image'])){
			unlink("uploads/images/category_images/".$delete_cat_image['category_image']);
		}

		$this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_categories');

		$this->session->set_flashdata('danger','Category removed successfully');

		redirect('categories');
	}

	public function create_sale_item()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$this->load->view('admin/create_sale_item');
	}
	public function ajax_sale_items($value='')
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$records = $this->vendor_model->ajax_get_sale_items();

		$data = [];

		foreach($records->result() as $record){
			$edit = "<a href='".base_url('edit_create_sale_item')."/".base64_encode($record->id)."' class='btn btn-sm bg-success-light mr-2'> <i class='fe fe-pencil'></i> Edit</a>";

			$delete = "<a href='".base_url('delete_create_sale_item')."/".base64_encode($record->id)."' class='btn btn-sm bg-danger-light'> <i class='fe fe-trash'></i> Delete</a>";

			$data[] = array(
				"name"=>$record->name,
				"discount"=>$record->discount,
				"action"=>"".$edit."".$delete."",
			);
		}

		$response = array(
			"draw" => $draw,
			"recordsTotal" => $records->num_rows(),
			"recordsFiltered" => $records->num_rows(),
			"data" => $data
		);

		echo json_encode($response);
      	exit();
	}
	public function add_create_sale_item($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data['main_categories'] = $this->vendor_model->get_data(array(), 'tbl_main_categories');
		$data['categories'] = $this->vendor_model->get_data(array(), 'tbl_categories');
		if(!empty($id)){
			$data['q'] = $this->vendor_model->get_single_data(array('id'=>base64_decode($id)), 'tbl_create_sale_items');
		}
		$this->load->view('admin/add_create_sale_item', $data);
	}
	public function upload_create_sale_item($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$main_category_id = $this->input->post('main_category_id');
		$category_id = $this->input->post('category_id', TRUE);
		$discount = $this->input->post('discount');

		$vendor_data = array('name'=>$name, 'type'=>$type, 'main_category_id'=>$main_category_id, 'category_id'=>$category_id, 'discount'=>$discount, 'created_at'=>date('Y-m-d H:i:s'));

		if(!empty($name) && !empty($type) && !empty($discount)){
			if(empty($id)){
				$data = $this->vendor_model->insert_data($vendor_data, 'tbl_create_sale_items');
    			echo $data; exit();
			}else{
				$data = $this->vendor_model->update_data($vendor_data, array('id'=>$id), 'tbl_create_sale_items');
				echo $id; exit();
			}
		}
	}
	public function products_for_sale($value='')
	{
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$id = $this->input->get_post('id');
		$main_category_id = $this->input->get_post('main_category_id');
		$category_id = $this->input->get_post('category_id');

		$main_cat_filter = $this->input->get_post('main_cat_filter');
		$cat_filter = $this->input->get_post('cat_filter');

		$records = $this->vendor_model->ajax_get_products_for_sale($main_category_id, $category_id, $main_cat_filter, $cat_filter);

		$data = [];
		$seller_id = $this->session->userdata("id");
        
        if(!empty($id)){
        	$check = $this->vendor_model->get_single_data(array('id'=>$id), 'tbl_create_sale_items');
        	$check_array = explode(',', $check['products']);
        }

		foreach($records->result() as $record){
			$add = "<input type='checkbox' name='add_item' class='add_item' value='".$record->id."' ";
			if(isset($check_array) && !empty($check_array)) {
				if(in_array($record->id, $check_array)){
					$add .= "checked";
				}
			}
			$add .= ">";

			$data[] = array(
					"id"=>$add,
					"product_title"=>$record->product_title,
					"product_price"=>$record->product_price,
					"category_name"=>$record->category_name
				);
		}

		$response = array(
			"draw" => $draw,
			"recordsTotal" => $records->num_rows(),
			"recordsFiltered" => $records->num_rows(),
			"data" => $data
		);

		echo json_encode($response);
      	exit();
	}
	
	public function upload_create_sale_items_products()
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$id = $this->input->get_post('id');
		$add_products_id = $this->input->get_post('add_products_id');
		$new_products = implode(',', $this->input->get_post('add_products_id'));
		$remove_products_id = $this->input->get_post('remove_products_id');

		if(!empty($id)){
			$check = $this->vendor_model->get_data(array('id'=>$id), 'tbl_create_sale_items');
			if(!empty($check) && !empty($check['products'])){
				$products = explode(',', $check['products']);
				if(!empty($remove_products_id)){
					foreach ($remove_products_id as $value) {
						if (($key = array_search($value, $products)) !== false) {
							unset($products[$key]);
						}
					}
				}
				if(!empty($add_products_id)){
					foreach ($add_products_id as $value) {
						if (!(($key = array_search($value, $products)) !== false)) {
							$products .= ','.$value;
						}
					}
				}
				$insert_data = $this->db->where('id', $id)->set('products', $products)->update('tbl_create_sale_items');
			}else{
				$insert_data = $this->db->where('id', $id)->set('products', $new_products)->update('tbl_create_sale_items');
			}
			
			if($insert_data){
				echo "Added to Products";
			}else{
				echo "Failed";
			}
		}else{
			echo "Something went wrong. Please try again.";
		}
	}
	public function delete_create_sale_item($id='')
	{
		if($this->session->userdata("id")=="")
		{
			redirect('admin');
		}
		$data= $this->vendor_model->delete_data(array('id'=>base64_decode($id)),'tbl_create_sale_items');
		$this->session->set_flashdata('danger','Sale removed successfully');
		redirect('create_sale_item');
	}
}