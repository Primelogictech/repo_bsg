<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user_controller";

// ADMIN ROUTES
$route['admin'] = 'admin_controller/index';
$route['verify_login'] = 'admin_controller/verify_login';
$route['dashboard'] = 'admin_controller/dashboard';
$route['logout'] = 'admin_controller/logout';

$route['categories'] = 'admin_controller/categories';
$route['add_category'] = 'admin_controller/add_category';
$route['edit_category/(:any)'] = 'admin_controller/add_category/$1';
$route['delete_category/(:any)'] = 'admin_controller/delete_category/$1';

$route['all_products'] = 'product_controller/products';
$route['view_product/(:any)'] = 'product_controller/view_product/$1';
$route['add_product'] = 'product_controller/add_product';
$route['edit_product/(:any)'] = 'product_controller/add_product/$1';
$route['upload_product'] = 'product_controller/upload_product';
$route['delete_product/(:any)'] = 'product_controller/delete_product/$1';
$route['delete_product_images/(:any)/(:any)'] = 'product_controller/delete_product_images/$1/$2';
$route['delete_product_bank_offers/(:any)/(:any)'] = 'product_controller/delete_product_bank_offers/$1/$2';

$route['create_sale_item'] = 'admin_controller/create_sale_item';
$route['ajax_sale_items'] = 'admin_controller/ajax_sale_items';
$route['edit_create_sale_item/(:any)'] = 'admin_controller/add_create_sale_item/$1';
$route['add_create_sale_item'] = 'admin_controller/add_create_sale_item';
$route['upload_create_sale_item'] = 'admin_controller/upload_create_sale_item';
$route['delete_create_sale_item/(:any)'] = 'admin_controller/delete_create_sale_item/$1';

$route['home/top_banners'] = 'home_controller/top_banners';
$route['home/add_top_banners'] = 'home_controller/add_top_banners';
$route['home/top_banner_status/(:any)/(:any)'] = 'home_controller/top_banner_status/$1/$2';
$route['home/delete_top_banner/(:any)'] = 'home_controller/delete_top_banner/$1';
$route['view_sale_products/(:any)'] = 'home_controller/view_sale_products/$1';


// TAX CONTROLLER ROUTES
$route['tax'] = 'tax_controller/index';
$route['add_tax'] = 'tax_controller/add_tax';
$route['upload_tax'] = 'tax_controller/upload_tax';
$route['edit_tax/(:any)'] = 'tax_controller/add_tax/$1';
$route['delete_tax/(:any)'] = 'tax_controller/delete_tax/$1';


// ADMIN MASTERS CONTROLLER ROUTES
$route['masters/attributes'] = 'masters_controller/attributes';
$route['masters/upload_attribute'] = 'masters_controller/upload_attribute';

$route['masters/attribute_masters'] = 'masters_controller/attribute_masters';
$route['masters/add_attribute_masters'] = 'masters_controller/add_attribute_masters';
$route['masters/edit_attribute_masters/(:any)'] = 'masters_controller/add_attribute_masters/$1';
$route['masters/delete_attribute_masters/(:any)'] = 'masters_controller/delete_attribute_masters/$1';

$route['masters/our_advantages'] = 'masters_controller/our_advantages';
$route['masters/upload_our_advantages'] = 'masters_controller/upload_our_advantages';
$route['masters/delete_our_advantages_images/(:any)/(:any)'] = 'masters_controller/delete_our_advantages_images/$1/$2';

$route['masters/tags'] = 'masters_controller/tags';
$route['masters/add_tags'] = 'masters_controller/add_tags';
$route['masters/edit_tags/(:any)'] = 'masters_controller/add_tags/$1';
$route['masters/delete_tags/(:any)'] = 'masters_controller/delete_tags/$1';

$route['masters/scrolling_message'] = 'masters_controller/scrolling_message';


// ADMIN SALES CONTROLLER ROUTES
$route['sales/orders'] = 'sales_controller/orders';
$route['sales/order_details/(:any)'] = 'sales_controller/order_details/$1';
$route['sales/shipments'] = 'sales_controller/shipments';
$route['orderFilter/(:any)'] = 'sales_controller/orderFilter/$1';


// USER ROUTES
$route['login'] = 'user_controller/login';
$route['verify_user'] = 'user_controller/verify_user';
$route['user_logout'] = 'user_controller/user_logout';

$route['registration'] = 'user_controller/registration';
$route['register_user'] = 'user_controller/register_user';

$route['forgot_password'] = 'user_controller/forgot_password';
$route['send_otp'] = 'user_controller/send_otp';
$route['forgot_password_update'] = 'user_controller/forgot_password_update';

$route['home'] = 'user_controller/index';
$route['mens'] = 'user_controller/index';
$route['kids'] = 'user_controller/kids';

$route['products'] = 'user_controller/products';
$route['product_details'] = 'user_controller/product_details';

$route['add_wishlist'] = 'user_controller/add_wishlist';
$route['remove_wishlist'] = 'user_controller/remove_wishlist';

$route['add_cart'] = 'user_controller/add_cart';
$route['remove_from_cart/(:any)'] = 'user_controller/remove_from_cart/$1';
$route['add_quantity'] = 'user_controller/add_quantity';

$route['my_address'] = 'user_controller/my_address';
$route['add_new_address'] = 'user_controller/add_new_address';
$route['editAddress/(:any)'] = 'user_controller/editAddress/$1';
$route['deleteAddress/(:any)'] = 'user_controller/deleteAddress/$1';
$route['saveAddress'] = 'user_controller/saveAddress';

$route['preCheckout'] = 'user_controller/preCheckout';

$route['payment'] = 'user_controller/payment';
$route['verifyPayment'] = 'user_controller/verifyPayment';

$route['order_success'] = 'user_controller/order_success';

$route['my_orders'] = 'user_controller/my_orders';
$route['order_details/(:any)'] = 'user_controller/order_details/$1';

$route['trackOrder/(:any)'] = 'user_controller/trackOrder/$1';
$route['getCartItemsNum'] = 'user_controller/getCartItemsNum';

$route['checkout'] = 'user_controller/checkout';

$route['getColorImages'] = 'user_controller/getColorImages';
$route['getColorSizes'] = 'user_controller/getColorSizes';
$route['getColorPrice'] = 'user_controller/getColorPrice';
$route['listColors'] = 'product_controller/listColors';
$route['deleteColors'] = 'product_controller/deleteColors';
$route['editColors/(:any)'] = 'product_controller/editColors/$1';
$route['updateColors'] = 'product_controller/updateColors';

/* End of file routes.php */
/* Location: ./application/config/routes.php */